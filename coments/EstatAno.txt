/**
 * Write a description of class EstatAno here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.Serializable;
public class EstatAno implements Serializable
{
    // instance variables - replace the example below with your own
    private int ano;
    private HashMap<Integer,EstatMes> statics;

    /**
     * Constructor for objects of class EstatAno
     */
    public EstatAno()
    {
        // initialise instance variables
        GregorianCalendar aux = new GregorianCalendar();
        this.ano = aux.get(Calendar.YEAR);
        int i;
        this.statics= new HashMap<Integer,EstatMes>();
        for(i=0;i<12;i++){
            this.statics.put(i,new EstatMes(i));
        }
    }
    
    public EstatAno(int ano)
    {
        // initialise instance variables
        this.ano=ano;
        int i;
        this.statics= new HashMap<Integer,EstatMes>();
        for(i=0;i<12;i++){
            this.statics.put(i,new EstatMes(i));
        }
    }

    public EstatAno(EstatAno ea)
    {
        // initialise instance variables
        this.ano=ea.getAno();
        this.statics = ea.getStatics();
    }
    
    /*
     * GETTERS
     */
    public int getAno(){return this.ano;}
    public int getSize(){return this.statics.size();}
    public HashMap<Integer,EstatMes> getStatics(){
        HashMap<Integer,EstatMes> novo = new HashMap<Integer,EstatMes>();
        EstatMes aux;
        for(Iterator<EstatMes> it = this.statics.values().iterator(); it.hasNext();){
            aux = it.next().clone();
            novo.put(aux.getMes(),aux);
        }
        return novo;
    }
    public EstatMes getEstatMes(int i){
        if(i<1 || i>12) return null;
        if(this.statics.containsKey(i)==false) return null;
        return(this.statics.get(i).clone());
    }
    public int getPontos(int mes){return this.statics.get(mes).getPontuacao();}
    private void setAno(int ano){this.ano=ano;}
    
    private void setEstatMes(EstatMes m){
        this.statics.remove(m.getMes());
        this.statics.put(m.getMes(),m.clone());
    }
    //duvidas
    private void setStatics(HashMap<Integer,EstatMes> to){
        if(to.size()==12){
            int i;
            for(i=1;i<=12;i++){
                if(to.containsKey(i)== false) return;
            }
            for(Iterator<Integer> it = this.statics.keySet().iterator(); it.hasNext();){
                this.statics.remove(it.next());
            }
            EstatMes aux;
            for(Iterator<EstatMes> it2 = to.values().iterator(); it2.hasNext();){
                aux=it2.next().clone();
                this.statics.put(aux.getMes(),aux);
            }
        }
    }
    /*
     * Metodos essenciais
     */
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Ano: " + this.ano + "\n");
        for(int i=1;i<=12;i++){
            s.append("Mes " + i + ": ");
            s.append(this.statics.get(i).toString());
        }
        return s.toString();
    }
    
    public boolean equals(Object o){
        if (this==o) return true;
        if(this==null || o ==null || this.getClass()!=o.getClass()) return false;
        EstatAno an = (EstatAno)o;
        if(this.ano!=an.getAno()) return false;
        if(this.getSize()!=an.getSize()) return false;
        boolean ret=true;
        Iterator<EstatMes> it1 = this.statics.values().iterator();
        Iterator<EstatMes> it2 = an.getStatics().values().iterator();
        while(ret && it1.hasNext() && it2.hasNext() ){
            ret = it1.next().equals(it2.next()); 
        }
        if(it1.hasNext() || it2.hasNext()) return false;
        return ret;
    }
    public int hashCode(){return(this.ano);}
    public EstatAno clone(){return new EstatAno(this);}
    //interaçao
    public void addPontuacao(int mes,int add){this.statics.get(mes).addPontuacao(add);}
   
    public void addMicroF(int mes){this.statics.get(mes).addMicroF();}
    public void addMultF(int mes){this.statics.get(mes).addMultF();}
    public void addMisteF(int mes){this.statics.get(mes).addMisteF();}
    public void addVirtF(int mes){this.statics.get(mes).addVirtF();}
    public void addEventP(int mes){this.statics.get(mes).addEventP();}

    public void addMicroH(int mes){this.statics.get(mes).addMicroH();}
    public void addMultH(int mes){this.statics.get(mes).addMultH();}
    public void addMisteH(int mes){this.statics.get(mes).addMisteH();}
    public void addVirtH(int mes){this.statics.get(mes).addVirtH();}

    public int getMicroF(int mes){return(this.statics.get(mes).getMicroF());}
    public int getMultF(int mes){return(this.statics.get(mes).getMultF());}
    public int getMisteF(int mes){return(this.statics.get(mes).getMisteF());}
    public int getVirtF(int mes){return(this.statics.get(mes).getVirtF());}
    public int getEventP(int mes){return(this.statics.get(mes).getEventP());}
    public int getMicroH(int mes){return(this.statics.get(mes).getMicroH());}
    public int getMultH(int mes){return(this.statics.get(mes).getMultH());}
    public int getMisteH(int mes){return(this.statics.get(mes).getMisteH());}
    public int getVirtH(int mes){return(this.statics.get(mes).getVirtH());}
    
    public void setMicroF(int mes, int val){this.statics.get(mes).setMicroF(val);}
    public void setMultF(int  mes, int val){ this.statics.get(mes).setMultF(val);}
    public void setMisteF(int mes, int val){this.statics.get(mes).setMisteF(val);}
    public void setVirtF(int  mes, int val){ this.statics.get(mes).setVirtF(val);}
    public void setEventP(int mes, int val){this.statics.get(mes).setEventP(val);}
    public void setMicroH(int mes, int val){this.statics.get(mes).setMicroH(val);}
    public void setMultH(int  mes, int val){ this.statics.get(mes).setMultH(val);}
    public void setMisteH(int mes, int val){this.statics.get(mes).setMisteH(val);}
    public void setVirtH(int  mes, int val){ this.statics.get(mes).setVirtH(val);}

    public int getHiden(int mes){return (this.statics.get(mes).getHiden());}
    public int getFound(int mes){return (this.statics.get(mes).getFound());}



    public int getMicroF(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getMicroF(i);
        }
        return res;
    }
    public int getMultF(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getMultF(i);
        }
        return res;
    }
    public int getMisteF(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getMisteF(i);
        }
        return res;
    }
    public int getVirtF(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getVirtF(i);
        }
        return res;
    }
    public int getEventP(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getEventP(i);
        }
        return res;
    }
    public int getMicroH(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getMicroH(i);
        }
        return res;
    }
    public int getMultH(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getMultH(i);
        }
        return res;
    }
    public int getMisteH(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getMisteH(i);
        }
        return res;
    }
    public int getVirtH(int mesi, int mesf){
        int i,res=0;
        for(i=mesi;i<=mesf;i++){
            res+=this.getVirtH(i);
        }
        return res;
    }

    public int getHiden(int mesi, int mesf){
        int i,ret=0;
        for(i=mesi;i<=mesf;i++){
            ret+=this.getHiden(i);
        }
        return ret;
    }
    public int getFound(int mesi, int mesf){
        int i,ret=0;
        for(i=mesi;i<=mesf;i++){
            ret+=this.getFound(i);
        }
        return ret;
    }
    
    public int getPontos(int mesi,int mesf){
        int i,ret=0;
        for(i=mesi;i<=mesf;i++){
            ret+=this.getPontos(i);
        }
        return ret;
    }

    
    public float razHideFound(int mes){return(this.statics.get(mes).razHideFound());}
    public float razFoundHide(int mes){return(this.statics.get(mes).razFoundHide());}
    
    
    public float razMicroHF(int mes){return(this.statics.get(mes).razMicroHF());}
    public float razMicroFH(int mes){return(this.statics.get(mes).razMicroFH());}


    public float razMisteHF(int mes){return(this.statics.get(mes).razMisteHF());}
    public float razMisteFH(int mes){return(this.statics.get(mes).razMisteFH());}
    
    public float razVirtHF(int mes){return(this.statics.get(mes).razVirtHF());}
    public float razVirtFH(int mes){return(this.statics.get(mes).razVirtFH());}


    public float razMicroTotalH(int mes){return(this.statics.get(mes).razMicroTotalH());}
    public float razMultTotalH(int mes){return(this.statics.get(mes).razMultTotalH());}
    public float razMisteTotalH(int mes){return(this.statics.get(mes).razMisteTotalH());}
    public float razVirtTotalH(int mes){return(this.statics.get(mes).razVirtTotalH());}

    public float razMicroTotalF(int mes){return(this.statics.get(mes).razMicroTotalF());}
    public float razMultTotalF(int mes){return(this.statics.get(mes).razMultTotalF());}
    public float razMisteTotalF(int mes){return(this.statics.get(mes).razMisteTotalF());}
    public float razVirtTotalF(int mes){return(this.statics.get(mes).razVirtTotalF());}
    public float razEventTotalF(int mes){return(this.statics.get(mes).razEventTotalF());}
    

    
    public float razHideFound(int mesi, int mesf){
        int found=0,hide=0,i;
        for(i=mesi;i<=mesf;i++){
            found+=this.getFound(i);
            hide+=this.getHiden(i);
        }
        if(found==0) return 0;
        return(hide/found);
    }
    
    public float razFoundHide(int mesi, int mesf){
        float aux = this.razHideFound(mesi,mesf);
        if(aux==0) return 0;
        return (1 / aux);
    }


    public float razMicroHF(int mesi, int mesf){
        int found=0,hide=0,i;
        for(i=mesi;i<=mesf;i++){
            found+=this.getMicroF(i);
            hide+=this.getMicroH(i);
        }
        if(found==0) return 0;
        return(hide/found);
    }

    public float razMultHF(int mesi, int mesf){
        int found=0,hide=0,i;
        for(i=mesi;i<=mesf;i++){
            found+=this.getMultF(i);
            hide+=this.getMultH(i);
        }
        if(found==0) return 0;
        return(hide/found);
    }
    
    public float razMisteHF(int mesi, int mesf){
        int found=0,hide=0,i;
        for(i=mesi;i<=mesf;i++){
            found+=this.getMisteF(i);
            hide+=this.getMisteH(i);
        }
        if(found==0) return 0;
        return(hide/found);
    }

    public float razVirtHF(int mesi, int mesf){
        int found=0,hide=0,i;
        for(i=mesi;i<=mesf;i++){
            found+=this.getVirtF(i); 
            hide+=this.getVirtH(i); 
        }
        if(found==0) return 0;
        return(hide/found);
    }
    public float razVirtFH(int mesi, int mesf){
        float aux = this.razVirtHF(mesi,mesf);
        if(aux==0) return 0;
        return(1 / aux);
    }
    public float razMisteFH(int mesi, int mesf){
        float aux = this.razMisteHF(mesi,mesf);
        if(aux==0) return 0;
        return(1 / aux);
    }
    public float razMicroFH(int mesi, int mesf){ 
        float aux = this.razMicroHF(mesi,mesf);
        if(aux==0) return 0;
        return(1 / aux);
    }
    public float razMultFH(int mesi, int mesf){ 
         float aux = this.razMultHF(mesi,mesf);
        if(aux==0) return 0;
        return(1 / aux);
    }
        
    public float razMicroTotalH(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getHiden(i);
            parc+=this.getMicroH(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
     public float razMultTotalH(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getHiden(i);
            parc+=this.getMultH(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
    public float razMisteTotalH(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getHiden(i);
            parc+=this.getMisteH(i);
        }
        if(total==0) return 0;
        return(parc / total);
        }
     public float razVirtTotalH(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getHiden(i);
            parc+=this.getVirtH(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
    public float razMicroTotalF(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getFound(i);
            parc+=this.getMicroF(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
     public float razMultTotalF(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getFound(i);
            parc+=this.getMultF(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
    public float razMisteTotalF(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getFound(i);
            parc+=this.getMisteF(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
     public float razVirtTotalF(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getFound(i);
            parc+=this.getVirtF(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
    public float razEventTotalP(int mesi, int mesf){
        int total=0,parc=0,i;
        for(i=mesi;i<=mesf;i++){
            total+=this.getFound(i);
            parc+=this.getEventP(i);
        }
        if(total==0) return 0;
        return(parc / total);
    }
}
