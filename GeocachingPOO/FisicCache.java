
/**
 * Write a description of class VerifCache here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.GregorianCalendar;

public class FisicCache extends Cache
{
    // variaveis de instancia
    private String veriCode;
    private ArrayList<Item> trade; //lista de itens que estao na cache
    
    /**
     * Construtor vazio para objectos da class FisicCache
     */
    public FisicCache(){
        super();
        this.veriCode = new String();
        this.trade = new ArrayList<Item>();
    }
    /**
     * Construtor parametrizado para objetos da classe FisicCache com Email 
     *@param String utl, email
     */
    public FisicCache(String utl){
        super(utl);
        this.veriCode = new String();
        this.trade = new ArrayList<Item>();
    }
    
    /**
     * Construtor parametrizado para objetos da classe FisicCache
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * @param String code, codigo de verificação
     */
    public FisicCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code){
        super(utl,l,dt,n,desc,dM,dF,p);
        this.veriCode = code;
        this.trade = new ArrayList<Item>();;
    }
    
    /**
     * Construtor parametrizado para objetos da classe FisicCache
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuaçao
     * @param String code, codigo de verificação
     * @param ArrayList<Item> t), lista de itens 
     */
    public FisicCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t){
        super(utl,l,dt,n,desc,dM,dF,p);
        this.veriCode = code;
        ArrayList<Item> res = new ArrayList<Item>(t.size());
        for(Item i : t){res.add(i.clone());}
        this.trade =res;
    }
    
    /**
     * Construtor de copia para objetos da classe FisicCache
     * @param FisicCache fc
     */
    public FisicCache(FisicCache fc){
        super(fc);
        this.veriCode=fc.getVeriCode();
        this.trade=fc.getTrade();
    }
    
    /**
     * Método getVeriCode
     * @return String, código de verificação
     */
    public String getVeriCode(){return this.veriCode;}
    
    /**
     * Método getTrade
     * @return ArrayList<Item>, lista de itens que a cache possui
     */
    public ArrayList<Item> getTrade(){
        boolean b=true;
        ArrayList<Item> res = new ArrayList<Item>();
        for(Item i : this.trade ){
            res.add(i.clone());
        }
        return res;
    }
    
    /**
     * Método setVeriCode
     * @param String code, código de verificação
     */
    public void setVeriCode(String code){this.veriCode=code;}
    
    /**
     * Método setTrade
     * @param ArrayList<Item> td, lista de itens
     */
    public void setTrade(ArrayList<Item> td){
        ArrayList<Item> t = new ArrayList<Item>();
        for(Item i : td){
            t.add(i.clone());
        }
        this.trade = t;
    }
    
    /**
     * Método nItens
     * @return int, numero de itens que a cache tem
     */
    public int nItens(){return this.getTrade().size();}
    
    /**
     * Método tradeItem que troca itens de uma cache, para retirar um Item é obrigatório adicionar outro
     * @param Item add, item para ser adicionado
     * @param Item rm, item escolhido para ser removido
     * @return boolean, se foi possível remover e adicionar os itens
     */
    public boolean verificaCodigo(String code){
        return this.getVeriCode().equals(code);
    }
    
    /**
     * Metodo tradeItem que troca itens de uma cache, para retirar um Item é obrigatorio adicionar outro
     * @param       item para ser adicionado
     * @param       item escolhido para ser removido
     * @return      se foi possivel remover e adicionar os itens
     */
    public boolean tradeItem(Item add, Item rm){
        boolean res = false;
        if(!( this.getTrade().size() == 0 || !this.trade.contains(rm))) return false;
        if(this.trade.remove(rm.clone()) ){ this.trade.add(add.clone()); res = true;}
        return res;
    }
    
    /**
     * Método addItem que adiciona um Item a lista de itens para troca
     * @param  Item i, item para ser adicionado
     * @return boolean, se foi possível  adicionar o item 
     */
    public boolean addItem(Item i){return this.trade.add(i.clone());}
    
    /**
     * Método removeItem que remove um Item a lista de itens para troca
     * @param Item i, item para ser removido
     * @return boolean, se foi possível remover o item 
     */
    public boolean removeItem(Item i){return this.trade.remove(i.clone());}
    
    /**
     * Método clone
     * @return FisicCache, copia 
     */
    public FisicCache clone(){return new FisicCache(this);}
    
    /**
     * Método toString
     * @return String, String do objeto 
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Codigo de Verificação: " + this.getVeriCode()+ "\n");
        for(Item i : this.getTrade()){s.append(i.toString()+"\n");}
        return s.toString();
    }
    
    /**
     * Método equals
     * @return boolean, resultado da comparação da igualdade entre dois objetos 
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        FisicCache f = (FisicCache) obj;
        if (!super.equals(f)) return false;
        if(!(this.getVeriCode().equals(f.getVeriCode()) && this.nItens()==f.nItens())) return false;
        for(int i=0;i<this.nItens() && res;i++){res = this.getTrade().get(i).equals(f.getTrade().get(i));}
        return res;
    }
}
