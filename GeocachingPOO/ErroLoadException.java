/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class ErroLoadException extends Exception {
    public ErroLoadException(String message){super(message);}
    public ErroLoadException(){super();}
}