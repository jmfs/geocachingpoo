
/**
 * Class para compara Pelo total de pontos dois DefaulUser
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.Comparator;
public class DefaultUserComparator implements Comparator<DefaultUser>
{
    /**
     * novo comparador pelo total de poontos dos utilizadotes
     * @param  u1 DefaultUser1
     * @param  u2 DefaultUser1
     * @return    <0 se o DefaultUser1 tem menos pontos que DefaultUser2
     *            >0 se o DefaultUser1 tem mais pontos que DefaultUser2 
     *            se o DefaultUser1 tem o mesmo pontos que DefaultUser2 comparaçao normal de DefaultUser
     */
    public int compare(DefaultUser u1, DefaultUser u2){
        if(u1.getStats().getTotalPontos()-u1.getStats().getTotalPontos()!=0){
            return (u1.getStats().getTotalPontos()-u1.getStats().getTotalPontos());
        }else{
            return u1.compareTo(u2);
        }
    }

}
