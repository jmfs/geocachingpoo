/**
 * Write a description of class Foto here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */

public class Foto extends Item
{
    // variaveis de instancia
    private String url;
    private String desc;

    /**
     * Construtor vazio para objectos da classe Foto
     */
    public Foto(){
       super();
       this.url = new String();
       this.desc = new String();
    }
    
    /**
     * @param String nome
     * @param int valor
     * @param String url, url da imagem
     * @param String desc, descrição
     * Construtor parametrizado para objectos da classe Foto
     */
    public Foto(String nome, int valor, String url, String desc){
        super(nome,valor);
        this.url=url;
        this.desc = desc;
    }
    
    /**
     * @param Foto f
     * Construtor de copia para objectos da classe Foto
     */
    public Foto(Foto f){
        super(f);
        this.url = f.getUrl();
        this.desc = f.getDesc();
    }
    
    /**
     * Método getUrl
     * @return String, url da imagem
     */
    public String getUrl(){return this.url;}
    
    /**
     * Método getDesc
     * @return String, descrição da imagem
     */
    public String getDesc(){return this.desc;}
    
    /**
     * Método setUrl
     * @param String, url de uma imagem
     */
    public void setUrl(String url){this.url=url;}
    
    /**
     * Método setDesc
     * @param String, descrição de uma imagem
     */
    public void setDesc(String desc){ this.desc=desc;}

    /**
     * Método toString
     * @return  String   
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append( "URL: " + this.getUrl() + "\n");
        s.append( "Descrição: " + this.getDesc() + "\n");
        return s.toString();
    }
    
    /**
     * Metodo clone
     * @return Foto, uma cópia 
     */
    public Foto clone(){return(new Foto(this));}

    /**
     *  Método equals
     * @param Object obj, objecto que vai comprar 
     * @return boolean, resultado da comparação dos dois objectos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Foto v = (Foto)obj;
        return ( super.equals(v) && (this.getUrl().equals(v.getUrl()))
                    && (this.getDesc().equals(v.getDesc())));
    }
    
    /**
     * Método hashCode
     * @return int, valor de hash
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
}
