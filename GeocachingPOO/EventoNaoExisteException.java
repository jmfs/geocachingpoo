
/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class EventoNaoExisteException extends Exception {
    public EventoNaoExisteException(String message){super(message);}
    public EventoNaoExisteException(){super();}
}
