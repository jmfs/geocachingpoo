
/**
 * Write a description of class MisteryCache here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.GregorianCalendar;
import java.util.ArrayList;
public class MisteryCache extends FisicCache
{
    // instance variables
    private int size; //1..4
    private Puzzel puzz; 

    /**
     * Construtor vasio para objetos da classe MisteryCache 
     */
    public MisteryCache(){
        super();
        this.size=2;
        this.puzz=new Puzzel();
    }
    
    /**
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * @param String code, código de confirmação característico da cache
     * @paramint size, tamanho da cache
     * @param Puzzel pz, desafio(perguntas)
     * Construtor parametrizado para objetos da classe MisteryCache 
     */
    public MisteryCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code,int size,Puzzel pz){
        super(utl,l,dt,n,desc,dM,dF,p,code);
        this.size|=2;
        this.puzz = pz.clone();
    }
    
    /**
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * @param String code, código de confirmação característico da cache
     * @paramint size, tamanho da cache
     * @param Puzzel pz, desafio(perguntas)
     * Construtor parametrizado(total) para objetos da classe MisteryCache 
     */
    public MisteryCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t,int size,Puzzel pz){
        super(utl,l,dt,n,desc,dM,dF,p,code,t);
        this.size=size;
        this.puzz = pz.clone();
    }
    
    /**
     * @param MisteryCache m
     * Construtor de copia para objetos da classe MisteryCache
     */
    public MisteryCache(MisteryCache m){
        super(m);
        this.size=m.getSize();
        this.puzz=m.getPuzz();
    }
    
    /**
     * GETTERS
     * @return int s, tamanho da cache 
     */
    public int getSize(){return this.size;}
    
    /**
     * GETTERS
     * @return Puzzel
     */
    public Puzzel getPuzz(){return this.puzz.clone();}
    
    /**
     * @param int s, valor de size
     * SETTERS
     */
    public void setSize(int s){this.size=s;}
    
    /**
     * @param Puzzel p, valor
     * SETTERS
     */
    public void setPuzzel(Puzzel p){this.puzz=p.clone();}
    
    /**
     * @param int np, numero de pergunta
     * @param int opResp, opção de resposta
     * @return int, pontuação deste puzzel
     */
    public int verificaPuzzel(int np,int opResp){
        return this.getPuzz().verifaPerguntaN(np,opResp);
       
    }
    
    /**
     * Método toString
     * @return String do objeto     
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Tamanho: " + this.getSize() +"\n");
        s.append("Puzzel: \n");
        s.append(this.getPuzz().toString());
        return s.toString();
    }
    
    /**
     * Clone- Cria uma copia do ponto recetor
     *@return MisteryCache
     *
     */
    public MisteryCache clone(){return new MisteryCache(this);}
    
    /**
     * @param Object obj
     * @return boolean, resposta relativamente à igualdade
     * Testa a igualdade entre objetos
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        MisteryCache mc = (MisteryCache) obj;
        if (!super.equals(mc)) return false;
        if(!(this.getSize()==mc.getSize() && this.getPuzz().equals(mc.getPuzz()))) return false;
        return res;
    }
}
