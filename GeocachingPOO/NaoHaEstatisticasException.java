/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class NaoHaEstatisticasException extends Exception {
    public NaoHaEstatisticasException(String message){super(message);}
    public NaoHaEstatisticasException(){super();}
}