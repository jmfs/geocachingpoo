
/**
 * Write a description of class GocachingPOO here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.GregorianCalendar;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Calendar;
import java.util.Set;
import static java.lang.Math.abs;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collections;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.Map;
//simula
import java.io.PrintStream;
import java.util.Random;
public class GeocachingPOO implements Serializable 
{
    //Simulaçao variaveis
    private static Random randomGenerator = new Random();
    //estaçoes
    private static final int primav =1;
    private static final int verao =2;
    private static final int outon =3;
    private static final int inv =4;
    //clima
    private static final int bom =1;
    private static final int mau =2;
    private static PrintStream outM;
    //
    private static final int rp=5;

    private GregorianCalendar relogProg;
    private TreeMap<String,DefaultUser> users; // lista de todos os utlizadores da aplicaçao
    private TreeMap<Integer,Evento> events; // tem os eventos(atividades) que o utilizador cria
    private TreeMap<Integer,Integer> reportAbuse; //idcache -> numero de reportabuse
    private TreeMap<Integer,Cache> caches; //contem todas as caches exixtentes no momento
    private TreeMap<String,Admin> admins;
    private int modo; //0 user 1 admin
    private int usersRegist=0;
    private String onLogin;

    //Classe metodos
    private static String formataData(GregorianCalendar data){
        return data.get(Calendar.DAY_OF_MONTH)+ "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR);
    }
    
    private static double round(double value, int places) {
        if (places < 0) return value;

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    private static int calcPont(int dM,int dF){return dM+dF;}
    //
///Simulaçao
    private int getEstacao(Integer id){
        GregorianCalendar ini = this.events.get(id).getInEvent();
        int mes = (ini.get(Calendar.MONTH)+1); 
        if(mes<3 || mes >11) return inv;
        if(mes<6) return primav;
        if(mes<10) return verao;
        return outon;
    }
    private static int geraMeteo(int estac){
        int rd;
        if(estac == verao){
            rd = randomGenerator.nextInt(3);
            if(rd<2) return bom;
            else return mau;
        }
        if(estac == inv){
            rd = randomGenerator.nextInt(3);
            if(rd<2) return mau;
            else return bom;
        }
        if(estac == primav){
            rd = randomGenerator.nextInt(5);
            if(rd<3) return bom;
            else return mau;
        }
        if(estac == outon){
            rd = randomGenerator.nextInt(5);
            if(rd<3) return mau;
            else return bom;
        }
        return bom;
    }
    private ArrayList<String> ordPont(ArrayList<String> users){
        TreeMap <DefaultUser,String> aux = new TreeMap <DefaultUser,String>(new DefaultUserComparator());
        for(String user : users){
            aux.put(this.users.get(user).clone(),user);
        }
        ArrayList<String> ret = new ArrayList<String>();
        ret.addAll(aux.values());
        Collections.reverse(ret);
        return ret;
    }
    /*

    private void geraPasso(Evento emSim , String user)throws ErroSimulacaoException{
        try{



            int min = this.relogProg.get(Calendar.MINUTE);
            int hour = this.relogProg.get(Calendar.HOUR_OF_DAY);
            String minu;
            String hor;
            if(min<10) minu = "0" + min;
            else minu = ""+min;
            if(hour<10) hor = "0" + hour;
            else hor = ""+hour;
            outM.println("No dia: " + formataData(this.relogProg) + " as " + hor + ":" + minu + " o participante " + user + " encontrou a " +  this.dizTipoCache(faltaEncon.get(posEncon)) + " "+ faltaEncon.get(posEncon));
        }
        catch(CacheNaoExisteException e ){
            throw new ErroSimulacaoException("Nao Existe cache " +  e.getMessage() );
        }
    }
 
    */
    private void geraPasso(Evento emSim , String user) throws ErroSimulacaoException{
        try{
            ArrayList<Integer> faltaEncon = emSim.getPorEncon(user);
            int posEncon = randomGenerator.nextInt(faltaEncon.size());
            emSim.addCacheUtl(user,faltaEncon.get(posEncon));
            this.caches.get(faltaEncon.get(posEncon)).addViseted(user);//nova
            this.users.get(user).addFound(faltaEncon.get(posEncon),(this.relogProg.get(Calendar.YEAR)),(this.relogProg.get(Calendar.MONTH)+1),this.tipoCache(faltaEncon.get(posEncon)));
            int min = this.relogProg.get(Calendar.MINUTE);
            int hour = this.relogProg.get(Calendar.HOUR_OF_DAY);
            String minu;
            String hor;
            if(min<10) minu = "0" + min;
            else minu = ""+min;
            if(hour<10) hor = "0" + hour;
            else hor = ""+hour;
            
            outM.println("No dia: " + formataData(this.relogProg) + " as " + hor + ":" + minu + " o participante " + user + " encontrou a " +  this.dizTipoCache(faltaEncon.get(posEncon)) + " "+ faltaEncon.get(posEncon));
        }catch(CacheNaoExisteException e ){
            throw new ErroSimulacaoException("Nao Existe cache " +  e.getMessage() );
        }
    }
    private int horaEncon(){
        int hora = this.relogProg.get(Calendar.HOUR_OF_DAY);
        //se for de dia o tempo de tempo que demora a ocorrer um evento é menor do que de noite
        if(hora<8 || hora>21){ //noite
            return 15;
        }
        return 7;
        // de dia sao encontradas caheces em metade do tempo do que de noite
    }
    public  void simulaEveto(Integer id,PrintStream p) throws ErroSimulacaoException{
        outM=p;
        //preparar tudo para simular
        Evento emSim = this.events.get(id).clone();
        this.events.get(id).setSimul(true);
        int meteo = geraMeteo(getEstacao(id));
        this.relogProg = (GregorianCalendar)(emSim.getInEvent().clone());
        Double bonus;
        if(meteo==mau){
                outM.println("Está Mau Tempo, o encontro de caches é mais dificil.");
                bonus=2.6;
            }else{
                outM.println("Está Bom Tempo, o encontro de caches é mais Facil.");
                bonus=1.9;
        }
        //remove do envento as caches removidas do sistema
        ArrayList <Integer> oldEvento = emSim.getConj();
        ArrayList <Integer> novEvento = new ArrayList <Integer>();
        for(Integer test :oldEvento){
            if (this.existeCache(test)) novEvento.add(test);
        }
        if(oldEvento.size()!=novEvento.size()) emSim.setConj(novEvento);
        //
        
        //top dos tuilizadores com melhores pontuaçoes de sempre 
        ArrayList<String> totUsers = ordPont(emSim.getParticp());
        
        //poe o processo inicial em condiçoes
        try{
            for(String util : totUsers ){
                emSim.removeUser(util);
                emSim.addParticp(util,geComuns(util,id));
            }
        }catch(EventoNaoExisteException e){
            throw new ErroSimulacaoException("Nao existe o evento " + e.getMessage());
        }
        catch(UtilizadorNaoExisteException e){
            throw new ErroSimulacaoException("Nao Existe o user " + e.getMessage());
        }
        catch(Exception e){
            throw new ErroSimulacaoException(e.getMessage() );
        }
        //
        
        //Cria a lista do melhes 15% utilizadores e o resto
        Double auxTop = (totUsers.size()*0.15);
        int top15pCent = auxTop.intValue();
        ArrayList<String> topUsers = new ArrayList<String>();
        ArrayList<String> lastUsers = new ArrayList<String>();
        for(int i=0;i<top15pCent;i++){
            topUsers.add(totUsers.get(i));
        }
        for(int i=top15pCent; i<totUsers.size();i++){
            lastUsers.add(totUsers.get(i));
        }
        //
        int i =1;
        //Simular
        while(this.relogProg.compareTo(emSim.getEndEvent())<0 && !emSim.fimEvent()){ 
            //simula enquanto o relogio do programa nao passar o tempo do evento e algem tiver alguma cache para encontrar
            
            //gerar dois encontors no top 15
            if((i%3 !=0)){
              if(!emSim.jatodosEcon(topUsers)){
                    String top15a;
                    do{
                        top15a = topUsers.get(randomGenerator.nextInt(topUsers.size()));                        
                    }while(emSim.jaAcabou(top15a));
                    geraPasso(emSim,top15a);
                }  
            }
            else{
                if(!emSim.jatodosEcon(lastUsers)){
                    String last;
                    do{
                        last = lastUsers.get(randomGenerator.nextInt(lastUsers.size())); 
                    }while(emSim.jaAcabou(last));
                    geraPasso(emSim,last);
                }
            }
            int passo = randomGenerator.nextInt(10) + 1 + randomGenerator.nextInt( horaEncon());
            //1 gera o range entre 1 e 1o min para encontrar minimo que vai demorar
            // durante o dia o intevalo é menor e a noire o intevalo é maior horaEncon(relogProg)
            this.relogProg.add(Calendar.MINUTE,passo);// passo da simulaçao;
            i++;
        }
        emSim.setSimul(true);
        //adicinar aos users as pontuaçoes deste evento
        HashMap<String,Integer> update = emSim.getNumEncon();
        for(String utl : update.keySet()){
            Double ptUpdate= update.get(utl)*bonus;
            this.users.get(utl).addPontos((emSim.getEndEvent().get(Calendar.YEAR)),(emSim.getEndEvent().get(Calendar.MONTH)+1),ptUpdate.intValue());
        }
        this.events.remove(id);
        this.events.put(id, emSim);
        if(this.relogProg.compareTo(emSim.getEndEvent())>=0){
            outM.println("Fim Sumulação Evento " + id + " nem todos Enconraram todas as Caches.");
        }else{
            outM.println("Fim Sumulação Evento " + id + " todos enconraram todas as Caches.");
        }
    }
    
    //fim simulaão

    

    public boolean eventoEnd(Integer id){
        if(this.events.containsKey(id)){
            return this.events.get(id).getSimul();
        }
        return true;
    }

    public String detalheEvento(Integer id)throws EventoNaoExisteException { 
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        StringBuilder s = new StringBuilder();
        Evento ev = this.events.get(id).clone(); 
        s.append("ID: "+ ev.getId()+ " Nome: "+ ev.getNome() + " Dificuldade: "+ ev.getDific()+ "\n");
        if(ev.getSimul()) s.append("JÁ SIMULADO\n");
        else s.append("NÃO SIMULADO\n");
        Local l = ev.getLocal().clone();
        s.append("Pais: " + l.getPais() +" Cidade: " + l.getCidade() + " Rua: " +l.getRua() + " Codigo Postal: " + l.getCodP() +"\n");
        s.append("GPS: " + l.getGPSFormatado() + " Raio Proura: " + ev.getRaio()+ "KM\n");
        s.append("Incrições:\n");
        s.append("\tInicio: "+ formataData(ev.getDCreat()) +"\n");
        s.append("\tFim: "+ formataData(ev.getEndInsc()) +"\n");
        s.append("Duração evento:\n");
        s.append("\tInicio: "+ formataData(ev.getInEvent()) +"\n");
        s.append("\tFim: "+ formataData(ev.getEndEvent()) +"\n");
        s.append("Descrição: "+ ev.getDesc()+"\n" );
        ArrayList<Integer> conj = ev.getConj();
        s.append("Existem no evento "+conj.size() +" Caches:\n");
        for(Integer cache :conj){
            s.append("\t" +cache +  "("+ this.dizTipoCache(cache) +");\n");
        }
        s.append("Participantes: " + ev.getPartic() +" de " + ev.getMaxpartic()+" disponiveis:\n");
        HashMap<String,Integer> starProg = ev.getStartProg();
        HashMap<String,HashSet<Integer>> userProg = ev.getUserProg();
        Iterator<String> it = starProg.keySet().iterator();
        while(it.hasNext()){
            String n2 = it.next();
            s.append("\t"+n2 + " ProgInic: " +starProg.get(n2)+ ";\n");
            s.append("\tProgesso Atual encontou (" + userProg.get(n2).size()+ "):\n");
            for(Integer ap: userProg.get(n2)){
                s.append("\t\t" +ap +  "("+ this.dizTipoCache(ap) +");\n");
            }
        }
        ArrayList<String> pistas = ev.getPistas();
        s.append("Pistas: ("+pistas.size()+"):\n");
        for(String pista : pistas){
            s.append("\t" + pista+";\n");
        }
        

        return s.toString();
        
    }
    public String getNomeEvento(Integer id) throws EventoNaoExisteException{
        if(this.events.containsKey(id)){
            return this.events.get(id).getNome();
        }else{
            throw new EventoNaoExisteException(id.toString());
        }
    }
    public ArrayList<String> getPistasCache(Integer id)throws CacheNaoExisteException{
        if(!this.existeCache(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return new ArrayList<String>(this.caches.get(id).getPistas());
    }
    public void addEvent(String n, Local l, double r, ArrayList<Integer> caches, HashMap<String,Integer> sp, GregorianCalendar dCreat, GregorianCalendar endInsc, GregorianCalendar inEvent, GregorianCalendar endEvent, String desc, int dific, ArrayList<String> p,int maxpart){
        ArrayList<Integer> cachesN = new ArrayList<Integer>();
        HashMap<String,Integer> spN = new HashMap<String,Integer>();
        ArrayList<String> pistas = new ArrayList<String>();
        cachesN.addAll(caches);
        pistas.addAll(p);
        Iterator<String> it = sp.keySet().iterator();
        while(it.hasNext()){
            String n2 = it.next();
            spN.put(n2,sp.get(n2));
        }
        Evento novo = new Evento(n,l.clone(),r,cachesN,spN,(GregorianCalendar)dCreat.clone(),(GregorianCalendar)endInsc.clone(),(GregorianCalendar)inEvent.clone(),(GregorianCalendar)endEvent.clone(),desc,dific,pistas,maxpart);
        this.events.put(novo.getId(),novo.clone());
        this.admins.get(onLogin).addEvent(novo.getId());
    }

    public ArrayList <String> getPistasde (Integer id) throws EventoNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        ArrayList <String> ret = new ArrayList <String>();
        ret.addAll(this.events.get(id).getPistas());
        return ret;
    }
    
    public void addFoto(Integer id, Foto ft) throws CacheNaoExisteException,CacheTipoErradoException{ 
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof VirtualCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        
        ((VirtualCache)this.caches.get(id)).addFoto(ft.clone());
    }
    
    public int fotosDe(Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof VirtualCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        return ((VirtualCache)this.caches.get(id)).getAlbum().size(); 
    }
    
    public String listFotosof(Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof VirtualCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        
        StringBuilder s = new StringBuilder();
        ArrayList<Foto> album= ((VirtualCache)this.caches.get(id)).getAlbum();
        s.append("Existem " +album.size() + " Fotos\n");
        for(Foto ft : album){
            s.append("\tNome: " + ft.getNome() + " Valor:" + ft.getValor() +";\n");
            s.append("\t Descrição:" +ft.getDesc() +";\n");
            s.append("\t URL: " + ft.getUrl() + "\n");
        }
        return s.toString();
    }
    
    public int itensDe( Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        return ((FisicCache)this.caches.get(id)).getTrade().size();
    }
    
    public String listItensof(Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        StringBuilder s = new StringBuilder();
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        
        ArrayList<Item> trade= ((FisicCache)this.caches.get(id)).getTrade();
        s.append("Existem para troca " +trade.size() + " itens\n");
        for(Item it : trade){
            s.append("\tNome: " + it.getNome() + " Valor:" + it.getValor());
            if(it instanceof CoinItem){
                s.append(" (TRAVELBUG)");
            }
            s.append(";\n");
        }
        return s.toString();
    }
    public Item tradeItems(Integer id, Item it , String nome)throws CacheNaoExisteException,CacheTipoErradoException,NaoExisteItemException{
        Item itCopy = it.clone();
        Item ant=null;
        
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        
        FisicCache td  =(FisicCache)this.caches.get(id).clone();
        ArrayList<Item> trade = td.getTrade();
        for(Item test : trade){
            if(! (test instanceof CoinItem )){
                if(test.getNome().equals(nome)){
                    ant= test.clone();
                }
            }
        }
        if(ant==null){
            throw new NaoExisteItemException(nome + " " + id.toString());
        }
        
        ((FisicCache)this.caches.get(id)).tradeItem(it,ant);
        ((FisicCache)this.caches.get(id)).removeItem(ant);
        return ant;
    }
    
    public boolean meHasCoin(){
        return this.users.get(onLogin).hasCoin();
    }
    
    public void addLisItem(Integer id , ArrayList<Item> lista)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
            
        for(Item it : lista){
                ((FisicCache)this.caches.get(id)).addItem(it.clone());
        }  
    }
    
    public CoinItem retiraCoin(Integer id) throws CacheNaoExisteException,CacheTipoErradoException,NaoExisteCoinException,NaoExisteItemException{
        CoinItem ant=null;
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
    
        FisicCache td  =((FisicCache)this.caches.get(id)).clone();
        ArrayList<Item> trade = td.getTrade();
        if(trade.size()==0){
            throw new NaoExisteItemException(id.toString()+" nao tem itens");
        }
        for(Item test : trade){
            if(test instanceof CoinItem ){
                ant= (CoinItem)test.clone();
            }
        } 

        if(ant==null){
           throw new NaoExisteCoinException(id.toString()); 
        }
        if(ant!=null){
            ((FisicCache)this.caches.get(id)).removeItem(ant);
            this.users.get(onLogin).setCoin(ant.clone());
            return ant.clone();
        }
        return null;
    }
    
    private CoinItem getMyCoin()throws NaoExisteCoinException{
        if(!this.users.get(onLogin).hasCoin()){
            throw new NaoExisteCoinException(onLogin + " nao possui Coin");
        }
        return this.users.get(onLogin).retirarCoin().clone();
    }
    
    public void addLeavCoinItem(Integer id)throws NaoExisteCoinException,CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        ((FisicCache)this.caches.get(id)).addItem(getMyCoin().clone());
    }
    public void addCoinItem(Integer id , CoinItem co)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        ((FisicCache)this.caches.get(id)).addItem(co.clone());
    }
    
    public void save(String path) throws ErroSaveException
    {
        FileOutputStream fos;
        ObjectOutputStream oos;
        try {
            fos = new FileOutputStream(path);
            oos = new ObjectOutputStream(fos);
         
            oos.writeObject(this);
            oos.writeObject(Cache.IDCacheG());
            oos.writeObject(Evento.IDEvento());
            oos.close();
             
        } catch (FileNotFoundException ex) {
            throw new ErroSaveException("ERRO ficheiro nao encontrado");
        } catch (IOException ex) {
            throw new ErroSaveException("Erro no IO");
        }
    }
    
    private void setIDs(int cache, int event){
        Evento.setIDEvento(event);
        Cache.setIDCache(cache);
    }
    
    public GeocachingPOO load(String path) throws ErroLoadException
    {
           FileInputStream fis;
           ObjectInputStream ois;
           GeocachingPOO geo = new GeocachingPOO();
        try {
           
              fis = new FileInputStream(path);
              ois = new ObjectInputStream(fis);
              
              geo=((GeocachingPOO)ois.readObject());
              geo.setIDs((Integer)ois.readObject(),(Integer)ois.readObject());
              ois.close();
             return geo;
            }
            
            
        catch(StreamCorruptedException ex){
            throw new ErroLoadException("Load Stream Corrupto");
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(GeocachingPOO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ErroLoadException("Load " + GeocachingPOO.class.getName());
        } catch (IOException ex) {
            Logger.getLogger(GeocachingPOO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ErroLoadException("Load " + GeocachingPOO.class.getName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GeocachingPOO.class.getName()).log(Level.SEVERE, null, ex);
            throw new ErroLoadException("Load " + GeocachingPOO.class.getName());
        }catch (Exception e){
             throw new ErroLoadException("Load Erro desconhecido");
        }
        
    }
    
    private GregorianCalendar getRelogProg(){return (GregorianCalendar)this.relogProg.clone();}

    private TreeMap<Integer,Integer> getReportAbuse(){
        TreeMap<Integer,Integer> novo = new TreeMap<Integer,Integer>();
        novo.putAll(this.reportAbuse);
        return novo; 
    }

    private TreeMap<String,Admin> getAdmins() throws PermissionsException{
        if(!this.adminMode()){
           throw new PermissionsException(onLogin + " não admin");
        }
        TreeMap<String,Admin> novo =  new TreeMap<String,Admin>();
        Iterator<String> it = this.admins.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            novo.put(n,this.admins.get(n).clone());
        }
        return novo;
    }

    private int getModo(){return this.modo;}
    
    private String getOnLogin(){return this.onLogin;}
    
    
    /**
     * Atençao a estes set isto esta a ser quase sempre copiar colar pode haver algum erro
     * se ouver bosta ver primeiro aqui
     */
    private void setRelogProg(GregorianCalendar dt){this.relogProg=(GregorianCalendar)dt.clone();}

    private void setUsers(TreeMap<String,DefaultUser> utls){
        TreeMap<String,DefaultUser> novo = new TreeMap<String,DefaultUser>();
        Iterator<String> it = utls.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            novo.put(n, utls.get(n).clone());
        }
        this.users=novo;
    }
    private void setEvents(TreeMap<Integer,Evento> evt){
        TreeMap<Integer,Evento> novo = new TreeMap<Integer,Evento>();
        Iterator<Integer> it = evt.keySet().iterator();
        while(it.hasNext()){
            Integer n = it.next();
            novo.put(n, evt.get(n).clone());
        }
        this.events=novo;
    }
    private void setReportAbuse(TreeMap<Integer,Integer> rb){
        TreeMap<Integer,Integer> novo = new TreeMap<Integer,Integer>();
        novo.putAll(rb);
        this.reportAbuse=novo;
    }
    private void setCaches(TreeMap<Integer,Cache> c){
        TreeMap<Integer,Cache> novo = new TreeMap<Integer,Cache>();
        Iterator<Integer> it = c.keySet().iterator();
        while(it.hasNext()){
            Integer n = it.next();
            novo.put(n, c.get(n).clone());
        }
        this.caches=novo;
    }
    private void setAdmins(TreeMap<String,Admin> ads){
        TreeMap<String,Admin> novo = new TreeMap<String,Admin>();
        Iterator<String> it = ads.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            novo.put(n, ads.get(n).clone());
        }
        this.admins=novo;
    }
    private void setUsersRegist(int i){this.usersRegist=i;}
    
    public Foto criaFoto(String nFoto,int vFoto,String fotoUrl,String descFoto){return new Foto(nFoto,vFoto,fotoUrl,descFoto);}
    
    public void alterarNomeCache(Integer id,String nome)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.caches.get(id).setNome(nome);
    }
    public void alterarCordenadasCache(Integer id, Cordenada c)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.caches.get(id).setLoc(c);
    }
    public void alterarDescCache(Integer id,String desc)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.caches.get(id).setDescri(desc);
    }
    public void alterarDMentalCache(Integer id,int dM)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.caches.get(id).setDifMent(dM);
    }
    public void alterarDFisicCache(Integer id,int dF)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.caches.get(id).setDifFisic(dF);
    }
    public boolean adicionarPistaCache(Integer id, String p)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.caches.get(id).addPistas(p);
    }
    public boolean adicionarPontIntCache(Integer id, Cordenada c)throws CacheNaoExisteException{
       if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.caches.get(id).addPontInt(c);
    }
    public boolean removerPistaCache(Integer id, String p)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.caches.get(id).removePistas(p);
    }
    public boolean removerPontIntCache(Integer id, Cordenada c)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.caches.get(id).removePontInt(c);
    } 

    public boolean adicionarItemCache(Integer id,Item t)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        return ((FisicCache)this.caches.get(id)).addItem(t);
    }
    public boolean removerItemCache(Integer id,Item t)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        return ((FisicCache)this.caches.get(id)).removeItem(t);
    }
    public boolean adicionarFotoCache(Integer id,String nFoto,int vFoto,String fotoUrl,String descFoto)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof VirtualCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        Foto t = criaFoto(nFoto,vFoto,fotoUrl,descFoto);
        return ((VirtualCache)this.caches.get(id)).addFoto(t);
    }
    public boolean removerFotoCache(Integer id,String nFoto,int vFoto,String fotoUrl,String descFoto)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof VirtualCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        Foto t = criaFoto(nFoto,vFoto,fotoUrl,descFoto);
        return ((VirtualCache)this.caches.get(id)).removeFoto(t);

    }
    
    
    
    public ArrayList<String> particpEventORD(Integer id) throws EventoNaoExisteException{
        if(!this.events.containsKey(id)){
           throw new EventoNaoExisteException(id.toString());
        }
        ArrayList<String> novo = new ArrayList<String>();
        TreeSet<String> tree = new TreeSet<String>();
        tree.addAll(this.events.get(id).getParticp());
        novo.addAll(tree);
        return novo;
    }
    
    public boolean hidenByMe(Integer id) throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.caches.get(id).getOwner().equals(onLogin);
    }

    public Puzzel getPuzzelOff(Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof MisteryCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        return ((MisteryCache)this.caches.get(id)).getPuzz().clone();
    }
    
    public boolean isFisic(Integer id)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return (this.caches.get(id).clone() instanceof FisicCache);
    }
    
    public int tipoCache(Integer id)throws CacheNaoExisteException{
        //1-Virtual
        //2multi
        //3micro
        //4misterio
        
       if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        Cache ca = this.caches.get(id).clone();
        int ret=1;
        if(ca instanceof FisicCache){
          if(ca instanceof MultiCache) ret =2;
          else{
              if(ca instanceof MicroCache) ret =3;
              else ret = 4;
            }      
        }
        return ret;
    }


    public GregorianCalendar getdayEvent(Integer id) throws EventoNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        return (GregorianCalendar)this.events.get(id).getInEvent().clone();
    }
    private HashSet<Integer> geComuns(String mail, Integer id)throws EventoNaoExisteException,UtilizadorNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> conjEvet = this.events.get(id).getConj();
        ArrayList<Integer> conjUser = this.users.get(mail).getFounded();
        HashSet<Integer> intcep = new HashSet<Integer>();
        for(Integer ca:conjEvet ){
            if(conjUser.contains(ca)){
                intcep.add(ca);
            }
        }
        return intcep;
    }

    public void addMeEvent(Integer id,GregorianCalendar dt )throws EventoNaoExisteException,UtilizadorNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        if(!this.users.containsKey(onLogin)){
            throw new UtilizadorNaoExisteException(onLogin);
        }
        this.users.get(onLogin).addPratEvents(id,dt.get(Calendar.YEAR), dt.get(Calendar.MONTH));
        this.events.get(id).addParticp(onLogin,new HashSet<Integer>());
    }

    public boolean jameIcreviEvent(Integer id)throws EventoNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        return this.users.get(onLogin).getPratEvents().contains(id);
    }
    public boolean possiIncreEvent(Integer id)throws EventoNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        return this.events.get(id).possiIncre();
    }
    public GregorianCalendar endIncriEvent(Integer id)throws EventoNaoExisteException{
        if(!this.events.containsKey(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        return (GregorianCalendar)this.events.get(id).getEndInsc().clone();
    }
    
    public String cacheDeatils(Integer id)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        StringBuilder s = new StringBuilder();
        Cache anal = this.caches.get(id).clone();
        s.append("ID: " + id + ", Nome: " + anal.getNome()+"\n");
        Cordenada co =anal.getLoc();
        s.append("Data: " + anal.getDataFormatada() + " Criador: "+  anal.getOwner() +"\n");
        s.append("Coordenadas: " + co.latitForm() + " , "+ co.longitForm()+ " Altitude: "+ co.getAlt()+"\n");
        s.append("Descrição:\n\t" + anal.getDescri()+"\n");
        s.append("Dificuldade: mental " + anal.getDifMent() + ", fisica " +anal.getDifFisic() + ", Pont:" + anal.getPontuation()+ "\n");
        ArrayList<String> visited = new ArrayList<String>();
        visited.addAll(anal.getVisited());
        s.append("Visitada por (" + visited.size() + ") :\n");
        for(String f: visited){
            s.append("\t"+ f.toString() +";\n");
        }
        
        ArrayList<String> pistas =anal.getPistas();
        s.append("Pistas (" + pistas.size() + ") :\n");
        for(String p: pistas){
            s.append("\t"+ p.toString() +";\n");
        }
        
        ArrayList<Cordenada> pInt =anal.getPontInt();
        s.append("Pontos de interesse (" + pInt.size() + ") :\n");
        for(Cordenada cord: pInt){
            s.append("\tCoordenadas: " + cord.latitForm() + " , "+ cord.longitForm()+";\n");
        }
        
        if(anal instanceof VirtualCache){
            VirtualCache vC =(VirtualCache)anal;
            ArrayList<Foto> fotos = vC. getAlbum();
            s.append("Fotos (" + fotos.size() + ") :\n");
            for(Foto ft : fotos){
                s.append("\tNome: " + ft.getNome() +";Valor: " + ft.getValor() +";Descriçao: " + ft.getDesc() +";URL: " + ft.getUrl()+"\n");
            }
        
        }else{
           FisicCache fC = (FisicCache)anal;     
           if(adminMode() || fC.getOwner().equals(onLogin)){
                s.append("Codigo: "+ fC.getVeriCode()+"\n");
           }  
           ArrayList<Item> trade = fC. getTrade();
           s.append("Itens (" + trade.size() + ") :\n");
           for(Item td : trade){
               if(td instanceof CoinItem ){
                   CoinItem coin = (CoinItem)td;
                   s.append("\t--COINITEM--\n");
                   s.append("\t\tData criação: "+ coin.getDataFormatada() +"\n");
                   s.append("\t\tCaches Visidadas:\n");
                   for(Integer hca : coin.getHistorico()){
                       s.append("\t\t\t" + hca +";\n");
                    }
                   s.append("\t");
                }
                s.append("\tNome: " + td.getNome() +";Valor: " + td.getValor() +"\n");
           }
           if(fC instanceof MultiCache){
               MultiCache mC = (MultiCache)fC;
               ArrayList<Integer> conjut = mC.getConj();
               s.append("MULTI CACHE\n");
               s.append("Exietem nesta cache " + conjut.size() + " sub-Caches:\n" );
               for(Integer conj : conjut){
                   s.append("\t"+ conj +"("+ this.tipoCache(conj)+");\n");
                }
               if(adminMode() || mC.getOwner().equals(onLogin)){
                    int tam =  conjut.size();
                    s.append("Progressos: \n");
                    HashMap<String,HashSet<Integer>> map = mC.getUserProg();
                    for(String user : map.keySet()){
                        s.append("\tUser" + user + "Progresso:" + map.get(user).size()+ "/"+ tam+";\n");
                    }
               } 
           }else{
               if(fC instanceof MicroCache){
                   s.append("MICRO CACHE\n");
                   MicroCache mC = (MicroCache)fC;
                   s.append("\tTamanho:" + mC.getSize()+"\n");
                }
                else{//misterio
                   s.append("CACHE MISTERIO\n");
                   MisteryCache mC = (MisteryCache)fC; 
                   s.append("\tTamanho:" + mC.getSize()+"\n");
                   if(adminMode() || mC.getOwner().equals(onLogin)){
                     Puzzel pz = mC.getPuzz();
                     s.append("\tPUZZEL Tamanho: "+ pz.nPerguntas()+"\n");
                     s.append("\tPerguntas:\n");
                     for(Pergunta perg: pz.getPerguntas()){
                         s.append("\t\tPregunta: " + perg.getPergunta() +" Pont:" +perg.getPont()+"\n");
                         s.append("\t\tRespostas:\n");
                         int i=1;
                         for(String resp: perg.getOpResp()){
                             s.append("\t\t\tOP "+i+"->"+resp+"\n");
                             i++;
                         }
                         s.append("\t\tOpção Correta: " + perg.getCorrecta()+"\n");
                     }
                   }
                }
            }
        }
        return s.toString(); 
    }
    

    //ver com a exceçao mas acho que esta melhor assim
    public String nomedeCache(Integer id){
        try{
            if(this.caches.containsKey(id)){
                return this.caches.get(id).getNome();
            }
            return "Removida";
        }catch(Exception e){
            return "Removida";
        }
    }
    
    //nao sei se esta bem ver melhor
    public ArrayList<Integer> listaParaEvento(Cordenada c, double distKM){
        ArrayList<Integer> novo = new  ArrayList<Integer>();
        ArrayList<Integer> ret = new  ArrayList<Integer>();
        novo.addAll(this.caches.keySet());
        for (Integer ca :novo){
            if(this.caches.get(ca).maisPertoKM(c,distKM)){
                ret.add(ca);
            }
        }
        return ret;
    }
    
    public String listaMaisPerto(Cordenada c, double distKM){
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> novo = new  ArrayList<Integer>();
        TreeMap<Double,Integer> toret = new TreeMap<Double,Integer>();
        novo.addAll(this.caches.keySet());
        for (Integer ca :novo){
            if(this.caches.get(ca).maisPertoKM(c,distKM)){
                toret.put(this.caches.get(ca).distKm(c), ca);
            }
        }
        s.append("Encontramos " + toret.size() +" Caches no raio de " + round(distKM,3) + "KM:\n");
        while(!toret.isEmpty()){
            Double distana = round(toret.firstKey(),3);
            Integer idana = toret.remove(distana);
            String nomeana = this.nomedeCache(idana);
            s.append("\tDistancia: " + distana + " KM Cache: " + idana + "(" + nomeana + ")\n");
        }
        return s.toString();
    }
    
    public int lastCacheID(){return Cache.IDCacheG();}
    public int lastEventID(){return Evento.IDEvento();}
    public int totalRegistedUsers(){return this.usersRegist;}
    public int totalCacheActive(){return this.caches.size();}
    public int totalEventActive(){return this.events.size();}
    public int totalUsersActive(){return this.users.size();}
    
     public String myStatsAnoMes(int ano,int mes) throws NaoHaEstatisticasException{
        StringBuilder s = new StringBuilder();
        DefaultUser log = this.users.get(onLogin);
        if(!log.hasYear(ano)){
           throw new NaoHaEstatisticasException(onLogin + " nao tem estatisticas em " +ano + "/"+mes);
        }

        s.append("Estatisticas de "+ onLogin +" do ano "+ ano +" ,mes " + (mes+1) +":\n");
        s.append("\tPontuação: "+ log.quantPontos(ano,mes)  +" ;\n");
        s.append("\tEcontradas:\n");
        s.append("\t\tCache Misterio: "+ log.quantMistF(ano,mes) +";\n");
        s.append("\t\tMicro Cache: "+ log.quantMicroF(ano,mes) +";\n");
        s.append("\t\tCache Virtual: "+ log.quantVirtF(ano,mes) +";\n");
        s.append("\t\tMulti Cache:"+ log.quantMultF(ano,mes) +";\n");
        s.append("\tEventos Participados: " +log.quantEventP(ano,mes) +";\n");
        s.append("\tEscondidas:\n");
        s.append("\t\tCache Misterio: "+ log.quantMistH(ano,mes) +";\n");
        s.append("\t\tMicro Cache: "+ log.quantMicroH(ano,mes) +";\n");
        s.append("\t\tCache Virtual: "+ log.quantVirtH(ano,mes) +";\n");
        s.append("\t\tMulti Cache:"+ log.quantMultH(ano,mes) +";\n");
        s.append("\tRazoes com Total:\n");
        s.append("\tEcontradas:\n");
        s.append("\t\tCache Misterio: "+ round(log.razMistFoundTotl(ano,mes),2) +";\n");
        s.append("\t\tMicro Cache: "+ round(log.razMicroFoundTotl(ano,mes),2) +";\n");
        s.append("\t\tCache Virtual: "+ round(log.razVirtFoundTotl(ano,mes),2) +";\n");
        s.append("\t\tMulti Cache:"+ round(log.razMultFoundTotl(ano,mes),2) +";\n");
        s.append("\tEscondidas:\n");
        s.append("\t\tCache Misterio: "+ round(log.razMistHidenTotl(ano,mes),2) +";\n");
        s.append("\t\tMicro Cache: "+ round(log.razMicroHidenTotl(ano,mes),2) +";\n");
        s.append("\t\tCache Virtual: "+ round(log.razVirtHidenTotl(ano,mes),2) +";\n");
        s.append("\t\tMulti Cache:"+ round(log.razMultHidenTotl(ano,mes),2) +";\n");
        s.append("\tEncontadas/Escondidas: "+ round(log.razHidenFound(ano,mes),2) +";\n");  
        s.append("\tEscondidas/Encontadas: "+ round(log.razFoundHiden(ano,mes),2) +"\n");
        return s.toString();
    }
    
    public String myStatsAno(int ano) throws NaoHaEstatisticasException{
        StringBuilder s = new StringBuilder();
        DefaultUser log = this.users.get(onLogin);
        if(!log.hasYear(ano)){
           throw new NaoHaEstatisticasException(onLogin + " nao tem estatisticas em " +ano);
        }

        s.append("Estatisticas de "+ onLogin +" do ano " + ano +":\n");
        s.append("\tPontuação: "+ log.quantPontos(ano)  +" ;\n");
        s.append("\tEcontradas:\n");
        s.append("\t\tCache Misterio: "+ log.quantMistF(ano) +";\n");
        s.append("\t\tMicro Cache: "+ log.quantMicroF(ano) +";\n");
        s.append("\t\tCache Virtual: "+ log.quantVirtF(ano) +";\n");
        s.append("\t\tMulti Cache:"+ log.quantMultF(ano) +";\n");
        s.append("\tEventos Participados: " +log.quantEventP(ano) +";\n");
        s.append("\tEscondidas:\n");
        s.append("\t\tCache Misterio: "+ log.quantMistH(ano) +";\n");
        s.append("\t\tMicro Cache: "+ log.quantMicroH(ano) +";\n");
        s.append("\t\tCache Virtual: "+ log.quantVirtH(ano) +";\n");
        s.append("\t\tMulti Cache:"+ log.quantMultH(ano) +";\n");
        s.append("\tRazoes com Total:\n");
        s.append("\tEcontradas:\n");
        s.append("\t\tCache Misterio: "+ round(log.razMistFoundTotl(ano),2) +";\n");
        s.append("\t\tMicro Cache: "+ round(log.razMicroFoundTotl(ano),2) +";\n");
        s.append("\t\tCache Virtual: "+ round(log.razVirtFoundTotl(ano),2) +";\n");
        s.append("\t\tMulti Cache:"+ round(log.razMultFoundTotl(ano),2) +";\n");
        s.append("\tEscondidas:\n");
        s.append("\t\tCache Misterio: "+ round(log.razMistHidenTotl(ano),2) +";\n");
        s.append("\t\tMicro Cache: "+ round(log.razMicroHidenTotl(ano),2) +";\n");
        s.append("\t\tCache Virtual: "+ round(log.razVirtHidenTotl(ano),2) +";\n");
        s.append("\t\tMulti Cache:"+ round(log.razMultHidenTotl(ano),2) +";\n");
        s.append("\tEncontadas/Escondidas: "+ round(log.razHidenFound(ano),2) +";\n");  
        s.append("\tEscondidas/Encontadas: "+ round(log.razFoundHiden(ano),2) +";\n");     
        return s.toString();
    }
    public void alteraMyMorada(String pais,String cidade,String rua,String cp){
        Local novo = new Local(pais,cidade,rua,cp,new Cordenada());
        this.users.get(onLogin).setMorada(novo.clone());
    }
    public Integer addCacheVirt(Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, Foto f){
        //1-Virtual
        VirtualCache creat = new VirtualCache(onLogin,l,dt,n,desc,dM,dF,calcPont(dM,dF));
        Integer id = creat.getId();
        creat.addFoto(f);
        this.caches.put(id,creat.clone());
        if(modo==1){
           this.admins.get(onLogin).addHidden(id); 
        }
        else{
            this.users.get(onLogin).addHidden(id,dt.get(Calendar.YEAR), dt.get(Calendar.MONTH),1);
        }
        return id;
    }
    
    public Integer addCacheMicro(Cordenada cord, GregorianCalendar nc, String nome,String desc,int dF, int dM, String veriF,ArrayList<Item> itens, int size){
        //3micro
        MicroCache creat = new MicroCache(onLogin,cord,nc,nome,desc,dM,dF,calcPont(dM,dF),veriF,itens,size);
        Integer id = creat.getId();
        this.caches.put(id,creat.clone());
        if(modo==1){
           this.admins.get(onLogin).addHidden(id); 
        }
        else{
            this.users.get(onLogin).addHidden(id,nc.get(Calendar.YEAR), nc.get(Calendar.MONTH),3);
        }
        return id;
    }
    
    
    public Integer addCacheMulti(Cordenada cord, GregorianCalendar nc, String nome,String desc,int dF, int dM, String veriF,ArrayList<Item> itens, ArrayList<Integer> caches){
        //2multi
        MultiCache creat = new MultiCache(onLogin,cord,nc,nome,desc,dM,dF,calcPont(dM,dF),veriF,itens,caches);
        Integer id = creat.getId();
        this.caches.put(id,creat.clone());
        if(modo==1){
           this.admins.get(onLogin).addHidden(id); 
        }
        else{
            this.users.get(onLogin).addHidden(id,nc.get(Calendar.YEAR), nc.get(Calendar.MONTH),2);
        }
        return id;
    }
    
    public Integer addCacheMist(Cordenada cord, GregorianCalendar nc, String nome,String desc,int dF, int dM, String veriF,ArrayList<Item> itens, int size,Puzzel pz){
        MisteryCache creat = new MisteryCache(onLogin,cord,nc,nome,desc,dM,dF,calcPont(dM,dF),veriF,itens,size,pz);
        Integer id = creat.getId();
        this.caches.put(id,creat.clone());
        if(modo==1){
           this.admins.get(onLogin).addHidden(id); 
        }
        else{
            this.users.get(onLogin).addHidden(id,nc.get(Calendar.YEAR), nc.get(Calendar.MONTH),4);
        }
        return id;
    }

    public void addFriend(String mail) throws UtilizadorNaoExisteException {
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        this.users.get(onLogin).addFriends(mail);
        this.users.get(mail).addFriends(onLogin);
    }
    
    public void rmoveFriend(String mail) throws NaoAmigoException,UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
           throw new UtilizadorNaoExisteException(mail); 
        }
        if(!this.users.containsKey(onLogin)){
           throw new UtilizadorNaoExisteException(onLogin); 
        }
        if(!this.users.get(onLogin).getFriends().contains(mail)){
            throw new NaoAmigoException(onLogin + " nao amigo de " + mail);
        }
        if(!this.users.get(mail).getFriends().contains(onLogin)){
            throw new NaoAmigoException(mail + " nao amigo de " + onLogin);
        }
        this.users.get(onLogin).removeFriends(mail);
        this.users.get(mail).removeFriends(onLogin);
    }
    
    public boolean isMyFrind(String mail){return this.users.get(onLogin).getFriends().contains(mail);}

    
    
    public boolean userJaEnconCache( Integer id){
        return this.users.get(onLogin).getFounded().contains(id);
    }
    public int getPontFisicCache(Integer id) throws CacheNaoExisteException,CacheTipoErradoException{
        Cache ca;
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!((ca = this.caches.get(id).clone()) instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }

        FisicCache f = (FisicCache)ca;
        return f.getPontuation();
    }
    
    //vermelhor
    public boolean multiTodasVisitadas(Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!(this.caches.get(id) instanceof MultiCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        ArrayList<Integer> conjMult = ((MultiCache)this.caches.get(id)).getConj();
        ArrayList<Integer> conjUser = this.users.get(onLogin).getFounded();
        ArrayList<Integer> intcep = new ArrayList<Integer>();
        for(Integer ca:conjMult ){
            if(conjUser.contains(ca)){
                ((MultiCache)this.caches.get(id)).atualizaProgUser(onLogin,ca);
            }
        }
        //return intcep
        return this.users.get(onLogin).getFounded().containsAll(((MultiCache)this.caches.get(id)).getConj());
        //ir as visitadas do onlogin e ver se tem todas as da multi
        //adicionar a multi as comums
    }

    public int pontodCacheID(Integer id)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.caches.get(id).getPontuation();
    }

    public void registaEncontroCache(Integer id,int pontos,int bonus,int tipo,GregorianCalendar data)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.caches.get(id).addViseted(onLogin);
        this.users.get(onLogin).addFound(id,data.get(Calendar.YEAR),data.get(Calendar.MONTH),tipo);
        this.users.get(onLogin).addPontos(data.get(Calendar.YEAR),data.get(Calendar.MONTH),pontos+(this.pontodCacheID(id))*bonus);
        
    }
    
    private void updateReport(Integer id)throws CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        int n=1;
        if(this.reportAbuse.containsKey(id)){
            n = this.reportAbuse.get(id)+1;
            this.reportAbuse.remove(id);
            n++;
        }
        this.reportAbuse.put(id,n);
    }
    public void registReportAbuse(Integer id) throws CacheNaoExisteException{
        if(!this.repoedByMe(id)){
            this.updateReport(id);
            this.users.get(onLogin).addMyReportAbuse(id);
            
        }
    }
    public boolean repoedByMe(Integer id){
         return this.users.get(onLogin).getMyReportAbuse().contains(id);
    }
    
    public int getUsersRegist(){return this.usersRegist;}
    
    
    public String last10encCacheBy(String mail) throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> anlfoung = getEncontradas(mail);
        Collections.reverse(anlfoung);
        StringBuilder s = new StringBuilder();
        int i=0;
          s.append("Caches encontradas ("+ Math.min(10,anlfoung.size())  + " de " +  Math.max(10,anlfoung.size()) + "):\n");
          for(i=0;i<Math.min(10,(anlfoung.size()));i++){
               Integer c = anlfoung.get(i);
               s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }
        return s.toString();
    }
    
    public String allOrdencCacheBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> anlfoung = new ArrayList<Integer>();
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<String> ret = new ArrayList<String>();
        StringBuilder s = new StringBuilder();
        novo.addAll(this.getEncontradas(mail));
        anlfoung.addAll(novo);
        s.append("Caches encontadas (" + anlfoung.size() + "):\n");
        for(Integer c: anlfoung){
            s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
        }
        return s.toString();
    }
    
    public String last10HideCacheBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> anlhide = getHiddenPor(mail);
        Collections.reverse(anlhide);
        StringBuilder s = new StringBuilder();
        int i=0;
        s.append("Caches escondidas ("+ Math.min(10,anlhide.size())  + " de " +  Math.max(10,anlhide.size()) + "):\n");
          for(i=0;i<Math.min(10,anlhide.size()) ;i++){
               Integer c = anlhide.get(i);
               s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
        }
        return s.toString();
    }
    
    public String allOrdHideCacheBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> anlhide = new ArrayList<Integer>();
        TreeSet<Integer> novo = new TreeSet<Integer>();
        novo.addAll(this.getHiddenPor(mail));
        anlhide.addAll(novo);
        s.append("Caches escondidas (" + anlhide.size() + "):\n");
            for(Integer c: anlhide){
                s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }
        return s.toString();
    }
    
    public String last10EventsBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> analEvents = getEventos(mail);
        StringBuilder s = new StringBuilder();
        int i=0;
        Collections.reverse(analEvents);
        s.append("Eventos Participados ("+ Math.min(10,analEvents.size())  + " de " +  Math.max(10,analEvents.size()) + "):\n");
            for(i=0;i<Math.min(10,analEvents.size());i++){
               Integer ev = analEvents.get(i);
               s.append("\t"+ ev.toString() +"\n");
            }
        return s.toString();
    }
    
    public String allOrdEventsBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> analEvents = new ArrayList<Integer>();
        TreeSet<Integer> novo = new TreeSet<Integer>();
        novo.addAll(this.getEventos(mail));
        analEvents.addAll(novo);
        s.append("Eventos Participados (" + analEvents.size() + "):\n");
            for(Integer ev: analEvents){
                s.append("\t"+ ev.toString() +"\n");
            }
        return s.toString();
    }
    
    
    public String last10RAbuseBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> anaRP = getRepotAbuse(mail);
        StringBuilder s = new StringBuilder();
        int i=0;
        Collections.reverse(anaRP);
        s.append("ReportAbuse ("+ Math.min(10,anaRP.size())  + " de " +  Math.max(10,anaRP.size()) + "):\n");
            for(i=1;i<Math.min(10,anaRP.size());i++){
               Integer ev = anaRP.get(i);
               s.append("\t"+ ev.toString() +"\n");
            }
        return s.toString();
    }
    
    public String allOrdRAbuseBy(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> anaRp = new ArrayList<Integer>();
        TreeSet<Integer> novo = new TreeSet<Integer>();
        novo.addAll(this.getRepotAbuse(mail));
        anaRp.addAll(novo);
        
        s.append("Report Abuse (" + anaRp.size() + "):\n");
        
            for(Integer ev: anaRp){
                s.append("\t"+ ev.toString() +"\n");
            }
        return s.toString();
    }
    
    public String userDeatils(String mail)throws UtilizadorNaoExisteException{
        StringBuilder s = new StringBuilder();
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        
        DefaultUser anal = this.users.get(mail).clone();
        s.append("ID: " + mail + ", Nome: " + anal.getNome());
        if(adminMode()){
            s.append(" ,Pass: "+ anal.getPassword());
        }
        
        s.append("\nNac: " + anal.getDataNacFotmatada() + " Genero: "+  anal.getGenero() +"\n");
        Local analoc = anal.getMorada();
        s.append("Pais: " + analoc.getPais() +" Cidade: " +  analoc.getCidade() +" Rua: " + analoc.getRua() +" Codigo Postal: " + analoc.getCodP() +"\n");
        ArrayList<String> anlamigos = getAmigos(mail);
        s.append("Amigos (" + anlamigos.size() + "):\n");
        for(String f: anlamigos){
            s.append("\t"+ f.toString() +"\n");
        }
        if(this.onLogin.equals(mail) || this.adminMode()){
            s.append(this.allOrdencCacheBy(mail));
            s.append(this.allOrdHideCacheBy(mail));
            s.append(this.allOrdEventsBy(mail));
            s.append(this.allOrdRAbuseBy(mail));
        }else{//amigo so ve 10 cache por ordem
            s.append(this.last10encCacheBy(mail));
            s.append(this.last10HideCacheBy(mail));
            s.append(this.last10EventsBy(mail));
        }
        return s.toString(); 
    }
    
    public boolean adminMode(){return this.modo==1;}
    public void setLogin(String mail){this.onLogin=mail;}
    
    private ArrayList<Integer> getEventos(String mail){
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getPratEvents());
        return novo;
    }
    
    private ArrayList<Integer> getRepotAbuse(String mail){
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getMyReportAbuse());
        return novo;
    }
    
    public String dizTipoCache(Integer id){
        if(!this.caches.containsKey(id)) return "Removida";
        return this.caches.get(id).tipoCache();
    }
    
    public String getCacheCode(Integer id)throws CacheTipoErradoException, CacheNaoExisteException{
        if(!this.caches.containsKey(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        Cache ca = this.caches.get(id).clone();
        
        if(!(ca instanceof FisicCache)){
            throw new CacheTipoErradoException(id.toString()+" é "+ this.dizTipoCache(id));
        }
        return ((FisicCache)ca).getVeriCode();
    }
    
    public ArrayList<Integer> getEncontradasOrd(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getFounded());
        ret.addAll(novo);
        return ret;
    }
    
    public ArrayList<Integer> getEncontradas(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getFounded());
        return novo;
    }
    
    
    private ArrayList<Integer> getHiddenPor(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getHidden());
        return novo;
    }
    
    private String nomeDe(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        return this.users.get(mail).getNome();
    }
    
    public ArrayList<String> getAmigos(String mail)throws UtilizadorNaoExisteException{
        if(!this.users.containsKey(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        TreeSet<String> novo = new TreeSet<String>();
        ArrayList<String> ret = new ArrayList<String>();
        ArrayList<String> retL = new ArrayList<String>();
        novo.addAll(this.users.get(mail).getFriends());
        ret.addAll(novo);
        for(String a : ret){
            if(jaExisteUser(a)){
                retL.add(a + " ("+ nomeDe(a) +")");
            }
        }
        return retL;
    }
    
    public boolean existeEvento(Integer id){return this.events.containsKey(id);}
    
    public void removeEvento(Integer id) throws EventoNaoExisteException{
        if(!this.existeEvento(id)){
            throw new EventoNaoExisteException(id.toString());
        }
        this.events.remove(id);
    }
    
    public ArrayList<Integer> getEventos(){
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        novo.addAll(this.events.keySet());
        ret.addAll(novo);
        return ret;
    }
    
    
    public void forceRemoveCache(Integer id)throws CacheNaoExisteException,PermissionsException{
        
        if(!this.existeCache(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        if(!this.adminMode() || !this.users.get(onLogin).getHidden().contains(id)){
            throw new PermissionsException();
        }
        
        this.caches.remove(id);
        if(this.estanoReport(id)){
            resetReport(id);
        }
    }
    public boolean existeCache(Integer id){return this.caches.containsKey(id);}
    public int limpaReportALL(){
        ArrayList<Integer> prec = new ArrayList<Integer>();
        prec.addAll(this.reportAbuse.keySet());
        int ret=0;
        for(Integer analise : prec){
            if(this.reportAbuse.get(analise) >= rp){
                this.reportAbuse.remove(analise);
                this.caches.remove(analise);
                ret++;
            }
        }
        return ret;
    }
    
    public boolean existeALimpar(){
        ArrayList<Integer> prec = new ArrayList<Integer>();
        prec.addAll(this.reportAbuse.keySet());
        int ret=0;
        for(Integer analise : prec){
            if(this.reportAbuse.get(analise) >= rp){
                ret++;
            }
        }
        return ret>0;
    
    }
    
    public boolean semReport(){return this.reportAbuse.isEmpty();}
    
    public boolean estanoReport(Integer id) throws CacheNaoExisteException{
        if(!this.existeCache(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        return this.reportAbuse.containsKey(id);
    }
    public void limpacacheRp(Integer id) throws CacheNaoExisteException{
        if(!this.existeCache(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.reportAbuse.remove(id); this.caches.remove(id);
    }
    public void resetReport(Integer id) throws CacheNaoExisteException{
        if(!this.existeCache(id)){
            throw new CacheNaoExisteException(id.toString());
        }
        this.reportAbuse.remove(id);
    }
    

    public boolean hasAdmin(){return !(this.admins.isEmpty());}
    
    public void registaAdmin(String mail, String pass, String nome)throws JaExisteMailException{
        if(this.jaExisteAdmin(mail) || this.jaExisteUser(mail)){
            throw new JaExisteMailException(mail);
        }
        Admin novo = new Admin(mail,pass,nome);
        this.admins.put(novo.getEmail(),novo);
    }
    public void registaUser(String mail, String pass, String nome,char genero,GregorianCalendar dtn,String pais,String cidade,String rua,String cp) throws JaExisteMailException{
        if(this.jaExisteAdmin(mail) || this.jaExisteUser(mail)){
            throw new JaExisteMailException(mail);
        }
        DefaultUser novo = new DefaultUser(mail,pass,nome,dtn.get(Calendar.YEAR),dtn.get(Calendar.MONTH),dtn.get(Calendar.DAY_OF_MONTH),genero,new Local(pais,cidade,rua,cp,new Cordenada()));
        this.users.put(novo.getEmail(),novo);
        this.usersRegist++;
    }
    public boolean jaExisteAdmin(String mail){return this.admins.containsKey(mail);}
    public boolean jaExisteUser(String mail){return this.users.containsKey(mail);}
    public boolean jaExisteIDuseradmin(String mail){return (this.jaExisteAdmin(mail) || this.jaExisteUser(mail));}
    
    public String getPassDe(String mail)throws UtilizadorNaoExisteException, AdminNaoExisteException{
        if(!this.jaExisteAdmin(mail) && !this.jaExisteUser(mail)){
            if(!this.jaExisteAdmin(mail) ){
                throw new AdminNaoExisteException(mail);
            }else{
                throw new UtilizadorNaoExisteException(mail);
            }
        }
        String ret;
        if(jaExisteAdmin(mail)){
            ret = this.admins.get(mail).getPassword();
        }else{
           ret = this.users.get(mail).getPassword(); 
        }
        return ret;
    }
    
    public ArrayList<String> getUsers(){
        TreeSet<String> novo = new TreeSet<String>();
        ArrayList<String> ret = new ArrayList<String>();
        novo.addAll(this.users.keySet());
        ret.addAll(novo);
        return ret;
    }
    
    
    public ArrayList<Integer> getCaches(){
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        novo.addAll(this.caches.keySet());
        ret.addAll(novo);
        return ret;
    }
    
    
    
    
    public void removeUser(String mail)throws UtilizadorNaoExisteException{
        if (!this.jaExisteUser(mail)){
            throw new UtilizadorNaoExisteException(mail);
        }
        ArrayList<Integer> eventIncrit = this.users.get(mail).getPratEvents();
        for(Integer evRM : eventIncrit){
            if(!this.events.get(evRM).getSimul()){
                this.events.get(evRM).removeUser(mail);                 
            }
        }
        this.users.remove(mail);
    }
    
    public void removeAdmin(String mail)throws AdminNaoExisteException{
        if (!this.jaExisteAdmin(mail)){
            throw new AdminNaoExisteException(mail);
        }
        this.admins.remove(mail);
    }
    public void setUserPASS(String id ,String pass)throws UtilizadorNaoExisteException{
        if(!this.jaExisteUser(id)){
            throw new UtilizadorNaoExisteException(id);
        }
        this.users.get(id).setPassword(pass);
    }
    
    public void setadminPASS(String id ,String pass)throws AdminNaoExisteException{
        if(!this.jaExisteAdmin(id)){
            throw new AdminNaoExisteException(id);
        }
        this.admins.get(id).setPassword(pass);
    }
    public void setModo(int m){this.modo=m;}
    /**
     * Constructor for objects of class GocachingPOO
     */
    public GeocachingPOO()
    {
        // initialise instance variables
        this.relogProg = new GregorianCalendar();
        this.users = new TreeMap<String,DefaultUser>();
        this.events = new TreeMap<Integer,Evento>();
        this.reportAbuse = new TreeMap<Integer,Integer>();
        this.caches = new  TreeMap<Integer,Cache>();
        this.admins = new TreeMap<String,Admin>();
        this.modo=0;
        this.onLogin="";
        //Para criar admin default descomentar linhas abaixo
        //mail: admin@geo.poo
        //PASS : geocachingPOOAdmin
        Admin novo= new Admin();
        this.admins.put(novo.getEmail(),novo);
    }
    public GeocachingPOO(GeocachingPOO geo) 
    {
        // initialise instance variables
        this.relogProg = geo.getRelogProg();
        this.users = geo.getUserALL();
        this.events = geo.getEventosALL();
        this.reportAbuse = geo.getReportAbuse();
        this.caches = geo.getCachesALL();
        this.admins = geo.getAdminsALL();
        this.modo=geo.getModo();
        this.onLogin=geo.getOnLogin();
    }
    
    public int hashCode(){return this.toString().hashCode();}
    
    public GeocachingPOO clone(){return new GeocachingPOO(this);}
    
    public TreeMap<String,DefaultUser> getUserALL(){
        ArrayList<String> novo = new ArrayList<String>(this.getUsers());
        TreeMap<String,DefaultUser> ret = new TreeMap<String,DefaultUser>();
        for(String user: novo){
            ret.put(user,this.users.get(user).clone());
        }
        return ret;
    }
    
    private TreeMap<String,Admin> getAdminsALL(){
        ArrayList<String> novo = new ArrayList<String>(this.admins.keySet());
        TreeMap<String,Admin> ret = new TreeMap<String,Admin>();
        for(String user: novo){
            ret.put(user,this.admins.get(user).clone());
        }
        return ret;
    }
    
    public TreeMap<Integer,Evento> getEventosALL(){
        ArrayList<Integer> novo = new ArrayList<Integer>(this.getEventos());
        TreeMap<Integer,Evento> ret = new TreeMap<Integer,Evento>();
        for(Integer ev: novo){
            ret.put(ev,this.events.get(ev).clone());
        }
        return ret;
    }
    
    public TreeMap<Integer,Cache> getCachesALL(){
        ArrayList<Integer> novo = new ArrayList<Integer>(this.getCaches());
        TreeMap<Integer,Cache> ret = new TreeMap<Integer,Cache>();
        for(Integer ca: novo){
            ret.put(ca,this.caches.get(ca).clone());
        }
        return ret;
    }
    public boolean equals(Object o){
        try{
            if(this==o) return true;
            if(o==null || this==null || this.getClass()!=o.getClass()) return false;
            GeocachingPOO geo = (GeocachingPOO)o;
            if((!this.getRelogProg().equals(geo.getRelogProg()))) return false;
            if(!(this.getModo()==geo.getModo())) return false;
            if(!(this.getOnLogin().equals(geo.getOnLogin()))) return false;
        
            TreeMap<String,DefaultUser> usersThis = this.getUserALL();
            TreeMap<String,DefaultUser> usersGeo = geo.getUserALL();
            if(usersThis.size()!= usersGeo.size()) return false;
            TreeMap<Integer,Evento> eventThis = this.getEventosALL();
            TreeMap<Integer,Evento> eventGeo = geo.getEventosALL();
            if(eventThis.size()!= eventGeo.size()) return false;
            TreeMap<Integer,Integer> raThis = this.getReportAbuse();
            TreeMap<Integer,Integer> raGeo = geo.getReportAbuse();
            if(raThis.size()!= raGeo.size()) return false;
            TreeMap<Integer,Cache> caThis = this.getCachesALL();
            TreeMap<Integer,Cache> caGeo = geo.getCachesALL();
            if(caThis.size()!= caGeo.size()) return false;
            TreeMap<String,Admin> admThis = this.getAdminsALL();
            TreeMap<String,Admin> admGeo = geo.getAdminsALL();
            if(admThis.size()!= admGeo.size()) return false;
        
            for(String user : usersThis.keySet()){
                if(!(usersThis.get(user).equals(usersGeo.get(user)))) return false;
            }

            for(Integer ev : eventThis.keySet()){
                if(!(eventThis.get(ev).equals(eventGeo.get(ev)))) return false;
            }
        
            for(Integer id : raThis.keySet()){
                if(!(raThis.get(id).equals(raGeo.get(id)))) return false;
            }
        
            for(Integer id : caThis.keySet()){
                if(!(caThis.get(id).equals(caGeo.get(id)))) return false;
            }
        
            for(String admin : admThis.keySet()){
                if(!(admThis.get(admin).equals(admGeo.get(admin)))) return false;
            }
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(this.getRelogProg().toString());
        s.append(this.getModo());
        s.append(this.getOnLogin());
        TreeMap<String,DefaultUser> usersThis = this.getUserALL();
        TreeMap<Integer,Evento> eventThis = this.getEventosALL();
        TreeMap<Integer,Integer> raThis = this.getReportAbuse();
        TreeMap<Integer,Cache> caThis = this.getCachesALL();
        TreeMap<String,Admin> admThis = this.getAdminsALL();
        
        for(String admin : admThis.keySet()){
            s.append(admThis.get(admin).toString());
        }
            
        for(Integer id : caThis.keySet()){
            s.append(caThis.get(id).toString());
        }
            
        for(Integer id : raThis.keySet()){
            s.append(id.toString()+"->"+raThis.get(id).toString());
        }
            
        for(Integer ev : eventThis.keySet()){
            s.append(eventThis.get(ev).toString());
        }
            
        for(String user : usersThis.keySet()){
            s.append(usersThis.get(user).toString());
        }

        return s.toString();
    }
    
}
