
/**
 * Write a description of class MicroCache here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.GregorianCalendar;
import java.util.ArrayList;
public class MicroCache extends FisicCache
{
    // instance variables 
    private int size; //1..4

    /**
     * Construtor vasio para objetos da classe MicroCache
     */
    public MicroCache(){
        super();
        this.size=2;
    }
    
    /**
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * @param String code, código de confirmação característico da cache
     * @param ArrayList<Item> t, itens dentro dessa cache
     * @paramint size, tamanho da cache
     * Construtor parametrizado para objetos da classe MicroCache
     */
    public MicroCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t,int size){
        super(utl,l,(GregorianCalendar)dt.clone(),n,desc,dM,dF,p,code,t);
        this.size=size;
    }
    
    /**
     * @param MicroCache mc
     * Construtor de copia para objetos da classe MicroCache
     */
    public MicroCache(MicroCache mc){
        super(mc);
        this.size=mc.getSize();
    }
    
    /**
     * Metodo getSize
     * @return      tamanho da cache 
     */
    public int getSize(){return this.size;}
    
    /**
     * Método setSize
     * @param int s, tamanho da cache 
     */
    private void setSize(int s){this.size=s;}
    
    /**
     * Método toString
     * @return String do objeto     
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Tamanho: " + this.getSize() +"\n");
        return s.toString();
    }
    
    /**
     * Método clone
     * @return MicroCache, uma copia de um objeto
     */
    public MicroCache clone(){return new MicroCache(this);}
    
    /**
     * Método equals
     * @param     Object  objeto que vai comprar 
     * @return    bollean  resultado da comparação dos dois objetos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        MicroCache mc = (MicroCache) obj;
        return (super.equals(mc) && this.getSize()==mc.getSize());
    }
}
