/**
 * Write a description of class Item here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.io.Serializable;
public class Item implements Serializable
{
     // variaveis de instancia
    private String nome;
    private int valor; // 1..5
    
    /**
     * Construtor vazio para objectos da classe Item
     */
    public Item(){
        this.nome= new String();
        this.valor=0;
    }
    
    /**
     * Construtor parameterizado para objectos da classe Item com Nome e Valor
     */
    public Item(String nome, int valor){
        this.nome= nome;
        this.valor=valor;
    }
    
    /**
     * Construtor de copia para objectos da classe Item
     */
    public Item(Item t){
        this.nome=t.getNome();
        this.valor=t.getValor();
    }
    
    /**
     * Metodo getNome
     * @return      nome do item
     */
    public String getNome(){return this.nome;}
    
    /**
     * Metodo getValor
     * @return      valor do item
     */
    public int getValor(){return this.valor;}
    
    /**
     * Metodo setNome
     * @param       nome do item
     */
    public void setNome(String nome){this.nome=nome;}
    
    /**
     * Metodo setValor
     * @param       valor do item
     */
    public void setValor(int valor){this.valor=valor;}
    
    /**
     * Metodo toString
     * @return  String     
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append( "Item-> Nome: " + this.getNome() + "; Valor: " + this.getValor() + "\n");
        return s.toString();
    }
    
    /**
     * Metodo clone
     * @return     uma copia 
     */
    public Item clone(){return(new Item(this));}
    
    /**
     * Metodo hashCode
     * @return     valor de hash
     */
    public int hashCode(){return this.nome.hashCode();}
    
    /**
     *  Metodo equals
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Item v = (Item)obj;
        return ( (this.getNome().equals(v.getNome()))  &&  (this.getValor()==v.getValor() ));
    }
}
