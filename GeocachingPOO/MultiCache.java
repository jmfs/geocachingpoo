/**
 * Write a description of class MultCache here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.GregorianCalendar;
import java.util.Iterator;
public class MultiCache extends FisicCache
{
    // variaveis de instancia
    private ArrayList<Integer> conj;// Id da cahes da multi nao podem estar neste conjunto EventCache nem MultiCache
    private HashMap<String,HashSet<Integer>> userProg;//progresso do user(mail) e conjinto dos id de cachesk

    /**
     * Construtor vazio para objectos da classe MultCache
     */
    public MultiCache(){
        super();
        this.conj = new ArrayList<Integer>();
        this.userProg = new HashMap<String,HashSet<Integer>>();
    }
    
    /**
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * @param String code, código de confirmação característico da cache
     * @param ArrayList<Item> t, lista com os itens
     * Construtor parametrizado para objetos da classe MultCache
     */
    public MultiCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t){
        super(utl,l,dt,n,desc,dM,dF,p,code,t);
        this.conj = new ArrayList<Integer>();
        this.userProg = new HashMap<String,HashSet<Integer>>();
    }

    /**
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * @param String code, código de confirmação característico da cache
     * @param ArrayList<Item> t, lista com os itens
     * @param ArrayList<Integer> conjt, código de verificação e lista de Itens
     * Construtor parametrizado para objectos da classe MultCache 
     */
    public MultiCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t, ArrayList<Integer> conjt){
        super(utl,l,dt,n,desc,dM,dF,p,code,t);
        ArrayList<Integer> res = new ArrayList<Integer>();
        for(Integer cache : conjt){
            res.add(cache);
        }
        this.conj = res;
        this.userProg = new HashMap<String,HashSet<Integer>>();
    }
    
    /**
     * @param MultiCache mc
     * Construtor de copia para objetos da classe MultCache
     */
    public MultiCache(MultiCache mc){
        super(mc);
        this.conj = mc.getConj();
        this.userProg = mc.getUserProg();
    }

    /**
     * Método getConj
     * @return ArrayList<Integer> - lista do conjunto de caches que pertencem a multi-cache
     */
    public ArrayList<Integer> getConj(){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(this.conj);
        return res;
    }
    
    /**
     * Método getUserProg
     * @return HashMap<String,HashSet<Integer>> - progresso de cada utilizador
     */
    public HashMap<String,HashSet<Integer>> getUserProg(){
        HashMap<String,HashSet<Integer>> res = new HashMap<String,HashSet<Integer>>();
        Iterator<String> itKey = this.userProg.keySet().iterator();
        while(itKey.hasNext()){
            String n = itKey.next();
            HashSet<Integer> resSet = new HashSet<Integer>();
            resSet.addAll(this.userProg.get(n));
            res.put(n,resSet);
        }
        return res;
    }
    
    /**
     * Método setConj
     * @param ArrayList<Integer> c - lista do conjunto de caches que pertencem a multi-cache
     */
    public void setConj(ArrayList<Integer> c){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(c);
        this.conj = res;
    }
    
    /**
     * Método setUserProg
     * @param HashMap<String,HashSet<Integer>> users, progresso de cada utilizador
     */
    private void setUserProg(HashMap<String,HashSet<Integer>> users){
        HashMap<String,HashSet<Integer>> res = new HashMap<String,HashSet<Integer>>();
        Iterator<String> itKey = users.keySet().iterator();
        while(itKey.hasNext()){
            String n = itKey.next();
            HashSet<Integer> resSet = new HashSet<Integer>();
            resSet.addAll(users.get(n));
            res.put(n,resSet);
        }
        this.userProg = res;
    }
    
    /**
     * Método nConj
     * @return int, o numero de caches que fazem parte desta multicache 
     */
    public int  nConj(){return this.getConj().size();}
    
    /**
     * Método nUtliProg
     * @return int, o numero de utilizadores que já encontraram pelo menos uma cache do conjunto 
     */
    public int nUtliProg(){return this.getUserProg().size();}
    
    /**
     * Método addCache
     * @param Integer c, id da cache a adicionar
     * @return boolean, se foi possível adicionar a cache
     */
    public boolean addCache(Integer c){return this.conj.add(c);}
    
    /**
     * Método removeCache
     * @param Integer c, id da cache a remover
     * @return boolean, se foi possível remover a cache
     */
    public boolean removeCache(Integer c){return this.conj.remove(c);}
    
    /**
     * Método inicializaProgUser
     * @param String user, email utilizador
     * @param ArrayList<Integer> listCache, lista de caches que um utilizador visitou
     * @return boolean se foi possível adicionar ao progresso as caches que esse user já tinha visitado
     */
    public boolean inicializaProgUser(String user, ArrayList<Integer> listCache){
        HashSet<Integer> res = this.getUserProg().get(user);
        for(Integer c : this.getConj()){
            if(listCache.contains(c)){
                if(!res.add(c)) return false;

            } 
        }
        return true;
    }
    
    /**
     * Método atualizaProgUser
     * @param Sring user, email do utilizador
     * @param Integer c, id da cache que o utilizador visitou
     * @return boolean, se foi possível adicionar a cache ao progresso do utilizador
     */
    public boolean atualizaProgUser(String user, Integer c){
        boolean res = false;
        //so se ele nao contiver é que adivionas
        if(this.conj.contains(c)) {res = this.userProg.get(user).add(c);}
        return res;
    }
    
    /**
     * Método visitouQuant
     * @param String utl, email do utilizador
     * @return int, quantas caches do conjunto da multi-cache já visitou
     */
    public int visitouQuant(String utl){return this.getUserProg().get(utl).size();}
    
    /**
     * Método visitouTodas
     * @param  String utl, email do utilizador
     * @return boolean, se já visitou todas as caches 
     */
    public boolean visitouTodas(String utl){return this.nConj()==visitouQuant(utl);}
    
    /**
     * Método toString,
     * @return String, forma "human readble"
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Conjunto de Caches: n: " + this.getConj() +"\n");
        for(Integer c : this.getConj()){s.append(c.toString());}
        Iterator<String> itKey = this.getUserProg().keySet().iterator();
        while(itKey.hasNext()){
            String u = itKey.next();
            s.append("User: " + u);
            for(Integer c : this.getUserProg().get(u)){s.append("-> " + c.toString()+ "\n");}
        }
        return s.toString();
    }
    
    /**
     * Método Clone
     * @return MultiCache copia
     */
    public MultiCache clone(){return new MultiCache(this);}
    
    /**
     * Método equals
     * @param Object obj, objeto a comparar
     * @return boolean,  resultado da igualdade
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        MultiCache mc = (MultiCache) obj;
        if (!super.equals(mc)) return false;
        if(!( this.nUtliProg()==mc.nUtliProg() && this.nConj() == mc.nConj() )) return false;
        for(int i=0; res && i<nConj();i++){
            this.getConj().get(i).equals(mc.getConj().get(i));
        }
        Iterator<String> itKeyT = this.getUserProg().keySet().iterator();
        Iterator<String> itKeyO = mc.getUserProg().keySet().iterator();
        while( res && itKeyT.hasNext() && itKeyO.hasNext()){
            String uT = itKeyT.next();
            String uO = itKeyO.next();
            if(!(uT.equals(uO))) return false;
            Iterator<Integer> itValeusT = this.getUserProg().get(uT).iterator();
            Iterator<Integer> itValeusO = mc.getUserProg().get(uO).iterator();
            while( res && itValeusT.hasNext() && itValeusO.hasNext()){
                itValeusT.next().equals(itValeusO);
            }
        }
        return res;
    }
}
