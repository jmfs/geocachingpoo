
/**
 * Write a description of class Pergunta here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.ArrayList;
import java.io.Serializable;
public class Pergunta implements Serializable
{
    // instance variables
    private String pergunta;
    private ArrayList<String> opResp;//No maximo 2 opçoes
    private String correcta;
    private int pont;
    
    /**
     * Constructor for objects of class Pergunta
     */
    public Pergunta(){
        this.pergunta = new String();
        this.opResp = new ArrayList<String>(2);
        this.correcta = new String();
        this.pont = 1;
    }
    
    /**
     * @param String p, Pergunta
     * @param ArrayList<String> r, ,Lista de opções 
     * @param String c, Resposta
     * @param int pont, Pontuação
     * Construtor parametrizado para objectos da classe Pergunta 
     */
    public Pergunta(String p, ArrayList<String> r, String c, int pont){
        this.pergunta = p;
        ArrayList<String> resp = new ArrayList<String>();
        resp.addAll(r);
        this.opResp = resp;
        this.correcta = c;
        this.pont = pont;
    } 
    
    /**
     * @param String p, Opção 1
     * @param String op1, Opção 2 
     * @param String op2, Opção 2
     * @param String c, Resposta
     * @param int pont, Pontuação
     * Construtor parametrizado para objectos da classe Pergunta
     */
    public Pergunta(String p,String op1, String op2, String c, int pont){
        this.pergunta = p;
        ArrayList<String> resp = new ArrayList<String>();
        resp.add(op1);
        resp.add(op2);
        this.opResp = resp;
        this.correcta = c;
        this.pont = pont;
    }
    
    /**
     * @param String p, Pergunta
     * @param String c, Resposta
     * @param int pont, pontuação
     * Construtor parametrizado para objectos da classe Pergunta
     */
    public Pergunta(String p, String c, int pont){
        this.pergunta = p;
        this.opResp = new ArrayList<String>(2);
        this.correcta = c;
        this.pont = pont;
    }
    
     /**
      * @param Pergunta p
      * Construtor que copia para objectos da classe Pergunta
      */
    public Pergunta(Pergunta p){
        this.pergunta = p.getPergunta();
        this.opResp = p.getOpResp();
        this.correcta = p.getCorrecta();
        this.pont = p.getPont();
    }
    
    /**
     * Método getPergunta
     * @return String, string pergunta
     */
    public String getPergunta(){return this.pergunta;}
    
    /**
     * Método getCorrecta
     * @return String, opção correcta
     */
    public String getCorrecta(){return this.correcta;}
    
    /**
     * Método getPont
     * @return int, pontuação para a resposta certa
     */
    public int getPont(){return this.pont;}
    
    /**
     * Método getOpResp
     * @return ArrayList<String>, lista de todas as opções de resposta
     */
    public ArrayList<String> getOpResp(){
        ArrayList<String> resp = new ArrayList<String>();
        resp.addAll(this.opResp);
        return resp;
    }
    
    /**
     * Método setPergunta
     * @param String p, pergunta
     */
    public void setPergunta(String p){this.pergunta=p;}
    
    /**
     * Método setCorrecta
     * @param String c, resposta correcta.
     */
    public void setCorrecta(String c){this.correcta=c;}
    
    /**
     * Método setPont
     * @param int pont, pontuação da pergunta
     */
    public void setPont(int pont){this.pont=pont;}
    
    /**
     * Método setOpResp
     * @param ArrayList<String> r, lista de opções de resposta
     */
    public void setOpResp(ArrayList<String> r){
        ArrayList<String> resp = new ArrayList<String>();
        resp.addAll(r);
        this.opResp = resp;
    }
    
    /**
     * Método verificaResp que verifica se uma resposta é a correcta
     * @param int pos, numero da resposta
     * @return int, pontuação obtida de acordo com a resposta
     */
    public int verificaResp(int pos){
        int res = 0;
        if(this.correcta.equals(this.opResp.get(pos-1))){
            res = this.getPont();
        }
        return res;
    }
    
    /**
     * Método addResposta que adiciona uma opção de resposta a pergunta
     * @param String op, resposta
     * @return boolean, se foi possível adicionar essa resposta(nº resposta máximo = 2)  
     */
    public boolean addResposta(String op){
        boolean res = false;
        if(this.opResp.size()<2){
            this.opResp.add(op);
            res = true;
        }
        return res;
    }
    
    /**
     * Método toString
     * @return String do objecto     
     */
    public String toString(){
        int i=1;
        StringBuilder s = new StringBuilder();
        s.append( "Pergunta: " + this.getPergunta() +"?\n");
        for(String a : this.getOpResp()){
            s.append("\t(Opção-"+i+"): " + a + ";\n");
            i++;
        }
        return s.toString();
    }
    
    /**
     * Método clone
     * @return Pergunta, uma copia de um objecto
     */
    public Pergunta clone(){return(new Pergunta(this));}

    /**
     * Método equals
     * @param Object obj, objecto que vai comprar 
     * @return boolean, resultado da comparação dos dois objectos  
     */
    public boolean equals(Object obj){
        boolean res;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Pergunta v = (Pergunta)obj;
        res = ( (this.getPergunta().equals(v.getPergunta()))  &&  (this.getCorrecta()==v.getCorrecta()) && (this.getPont()==v.getPont()) && (this.getOpResp().size()==v.getOpResp().size()));
        for (int i=0;i<this.opResp.size() && res ; i++){
            res = ( this.opResp.get(i).equals(v.getOpResp().get(i)) );
        }
        return res;
    }
    
    /**
     * Metodo hashCode
     * @return      resultado do calculo do hashCode o objecto
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
}
