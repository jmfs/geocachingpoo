
/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class CacheNaoExisteException extends Exception {
    public CacheNaoExisteException(String message){super(message);}
    public CacheNaoExisteException(){super();}
}