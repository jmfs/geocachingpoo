/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class ErroSaveException extends Exception {
    public ErroSaveException(String message){super(message);}
    public ErroSaveException(){super();}
}