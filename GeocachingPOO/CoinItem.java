/**
 * Write a description of class CoinItem here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Iterator;
public class CoinItem extends Item
{
    // variaveis de instância
    private GregorianCalendar fstDat;
    private ArrayList<Integer> historico; 
     
    /**
     * Construtor vazio para objectos da classe CoinItem
     */
    public String getDataFormatada(){
        return this.fstDat.get(Calendar.DAY_OF_MONTH)+ "/" + this.fstDat.get(Calendar.MONTH) + "/" + this.fstDat.get(Calendar.YEAR);
    }
    public CoinItem(){
        super();
        this.fstDat = new GregorianCalendar();
        this.historico = new ArrayList<Integer>();
    }
    
    /**
     * @param String nome 
     * @param int valor
     * @param GregorianCalendar fstDat
     * @param ArrayList<Integer> h
     * Construtor parametrizado para objectos da classe CoinItem 
     */
    public CoinItem(String nome , int valor, GregorianCalendar fstDat, ArrayList<Integer> h){
        super(nome,valor);
        this.fstDat = fstDat;
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(h); 
        this.historico = res;
    }
    
    /**
     * @param String nome 
     * @param int valor
     * @param GregorianCalendar fstDat, Data de criação
     * Construtor parametrizado para objectos da classe
     */
    public CoinItem(String nome, int valor, GregorianCalendar fstDat){
        super(nome,valor);
        this.fstDat = (GregorianCalendar)fstDat.clone(); //atençao a isto penso que temos de fazer clone
        this.historico = new ArrayList<Integer>();
    }
    
    /**
     * @param CoinItem c
     * Construtor de copia para objectos da classe CoinItem
     */
    public CoinItem(CoinItem c){
        super(c);
        this.fstDat = c.getFstDat();
        this.historico = c.getHistorico();
    }
    
    /**
     * Método getFstDat
     * @return GregorianCalendar, da de criação do item
     */
    public GregorianCalendar getFstDat(){return (GregorianCalendar)fstDat.clone();}
    
    /**
     * Método getHistorico
     * @return ArrayList<Integer>, lista de caches por onde a CoinItem passou
     */
    public ArrayList<Integer> getHistorico(){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(this.historico);
        return res;
    }
    
    /**
     * Método setFstDat
     * @param GregorianCalendar d, data de criação do objecto 
     */
    private void setFstDat(GregorianCalendar d){this.fstDat = (GregorianCalendar)d.clone();}
    
    /**
     * Método setHistorico
     * @param ArrayList<Integer> h, lista de caches
     */
    public void setHistorico(ArrayList<Integer> h){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(h);
        this.historico = res;
    }
    
    /**
     * Método que adiciona local a CoinItem por onde passou
     * @param Integer id, recebe a cache por onde passou
     * @return boolean, se foi possível adicionar esse local a CoinItem
     */
    public boolean addLocal(Integer id ){ return this.historico.add(id);}
    
    /**
     * Método conta quantos locais este CoinItem já visitou
     * @return int, numero de caches por onde passou
     */
    public int nVisitou(){
        return this.historico.size();
    }
    
    /**
     * Método diz qual foi o ultimo local visitado por uma CoinItem
     * @return Integer, o ultimo local por onde passou
     */
    public Integer lastLocal(){
        return this.historico.get(this.nVisitou()-1);
    }
    
    /**
     * Método hashCode
     * @return int, resultado do calculo do hashCode o objecto
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    /**
     * Método que passa para String o objecto
     * @return String, a informação do objecto em forma de string
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append( super.toString());
        s.append("Data primeiro local: " );
        s.append("Ano: " +  this.fstDat.get(Calendar.YEAR) + "Mes: " + (this.fstDat.get(Calendar.MONTH) + 1 )+ "Dia: " + this.fstDat.get(Calendar.DAY_OF_MONTH) +"\n");
        s.append( "Locais:\n");
        for(Integer p : this.historico){
            s.append(p.toString());
        }
        return s.toString();
    }
    
    /**
     * Método que clona um determinado objecto
     * @return CoinItem, uma copia do objecto
     */
    public CoinItem clone(){return(new CoinItem(this));}
    
    /**
     * Método equals
     * @param Object obj, objecto que vai comprar
     * @return boolean, resultado da comparação dos dois objectos
     */
    public boolean equals(Object obj){
        boolean res;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        CoinItem v = (CoinItem)obj;
        res = ( super.equals(v) 
                        && (this.historico.size()==v.historico.size())
                            && this.fstDat.equals(v.getFstDat()));
        for (int i=0;i<this.historico.size() && res ; i++){
            res = ( this.historico.get(i).equals(v.historico.get(i)) );
        }
        return res;
    }
}
