/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class JaExisteAdminException extends Exception {
    public JaExisteAdminException(String message){super(message);}
    public JaExisteAdminException(){super();}
}
