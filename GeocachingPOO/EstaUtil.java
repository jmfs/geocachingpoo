
/**
 * Write a description of class EstaUtil here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
//FAlta as Estatisticas (razoes)
import java.util.HashSet;
import java.util.HashMap;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Iterator;
import java.io.Serializable;
public class EstaUtil implements Serializable
{
    // instance variables - replace the example below with your own
    private int reportabused;
    //hashmao dos reports abuse
    private HashMap<Integer,EstatAno> statics;
    
    //vatiaveis de classe de modo a obtermos o nivel de geocavher o tuilizador
    private static final int mfraco = 20;
    private static final int fraco = 50;
    private static final int bom = 100;
    private static final int mbom = 150;
    private static final int exelente = 200;
    /**
     * Constructor for objects of class EstaUtil
     */
    public EstaUtil()
    {
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        // initialise instance variables
        this.statics = new HashMap<Integer,EstatAno>();
        this.reportabused = 0;
        EstatAno ae = new EstatAno(ano);
        this.statics.put(ae.hashCode(),ae);
    }
    public EstaUtil(int ano)
    {
        // initialise instance variables;
        this.statics = new HashMap<Integer,EstatAno>();
        this.reportabused = 0;
        EstatAno ae = new EstatAno(ano);
        this.statics.put(ae.hashCode(),ae);
    }
    private EstaUtil(EstaUtil e)
    {
        // initialise instance variables;
        this.reportabused = e.getReportabused();
        this.statics = e.getStatics();
    }
    
    public int getReportabused(){return this.reportabused;}
    
    private HashMap<Integer,EstatAno> getStatics(){
        HashMap<Integer,EstatAno> novo = new HashMap<Integer,EstatAno>();
        EstatAno aux;
        for(Iterator<EstatAno> it = this.statics.values().iterator(); it.hasNext();){
            aux = it.next().clone();
            novo.put(aux.getAno(),aux);
        }
        return novo;
    }
    
    public EstaUtil clone(){return new EstaUtil(this);}
    
    public boolean equals(Object o){
        if(this == o) return true;
        if(this==null || o==null || o.getClass()!=this.getClass()) return false;
        EstaUtil es = (EstaUtil)o;
        //return (this.toString().equals(es.toString()));
        if(this.getReportabused()!=es.getReportabused()) return false;
        if(this.quantosAnos()!=es.quantosAnos()) return false;
        boolean ret=true;
        Iterator<EstatAno> it1 = this.statics.values().iterator();
        Iterator<EstatAno> it2 = es.getStatics().values().iterator();
        while(ret && it1.hasNext() && it2.hasNext() ){
            ret = it1.next().equals(it2.next()); 
        }
        if(it1.hasNext() || it2.hasNext()) return false;
        return ret;
    }
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Reported: " + this.getReportabused() + "\n");
        Iterator<EstatAno> it = this.statics.values().iterator();
        while(it.hasNext()){
            s.append("Ano: ");
            s.append(it.next().toString() + "\n"); 
        }
        return s.toString();
    }
   
    //rafado so para ter
    public int hashCode(){return this.toString().hashCode();}
    
    // metodos de interaçao
     public int quantosAnos(){return this.statics.size();}
    //abusos
    public boolean algumRepotaduse(){return(this.getReportabused()!=0);}
    public boolean nenhumRepotaduse(){return(this.getReportabused()==0);} 
    public void addRepotaduse(){this.reportabused++;}

    //Estatisticas
    
    public int getTotalPontos(){
        int tot=0;
        for(Integer ano : this.statics.keySet()){
            tot= this.getPontAno(ano);
        }
        return tot;
    }
    public boolean containsAno(int ano){return this.statics.containsKey(ano);}
    
    public void addAno(int ano){
        if(this.containsAno(ano)==false){
            EstatAno ae = new EstatAno(ano);
            this.statics.put(ae.hashCode(),ae);
        }
    }
    
    public boolean containsAtualAno(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.containsAno(ano);
    }
    
    public void addAtualAno(){
        if(this.containsAtualAno()==false){
          GregorianCalendar aux = new GregorianCalendar();
          int ano = aux.get(Calendar.YEAR); 
          this.addAno(ano);
        }
    }
   //GETERS
    /*Metodos para decodir as pontuaçoes com varios parametros*/
    //pontuaçao
    public int getPontAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getPontos(mes);
    }
    
    public int getPontAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getPontos(mesi,mesf);
    }
    
    public int getPontAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getPontAnoMes(ano,mes);
    }
    
    public int getPontAno(int ano){
        return this.getPontAnoMeses(ano,0,11);
    }
    
    public int getPontAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getPontAno(ano);
    }
    //found
    public int getFoundAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getFound(mes);
    }
    
    public int getFoundAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getFound(mesi,mesf);
    }
    
    public int getFoundAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getFoundAnoMes(ano,mes);
    }
    
    public int getFoundAno(int ano){
        return this.getFoundAnoMeses(ano,0,11);
    }
    
    public int getFoundAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getFoundAno(ano);
    }
    //Hiden
    public int getHidenAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getHiden(mes);
    }
    
    public int getHidenAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getHiden(mesi,mesf);
    }
    
    public int getHidenAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getHidenAnoMes(ano,mes);
    }
    
    public int getHidenAno(int ano){
        return this.getHidenAnoMeses(ano,0,11);
    }
    
    public int getHidenAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getHidenAno(ano);
    }
    //microF
    public int getMicroFAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMicroF(mes);
    }
    
    public int getMicroFAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMicroF(mesi,mesf);
    }
    
    public int getMicroFAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getMicroFAnoMes(ano,mes);
    }
    
    public int getMicroFAno(int ano){
        return this.getMicroFAnoMeses(ano,0,11);
    }
    
    public int geticroFAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getMicroFAno(ano);
    }    
   
    //MultF
    public int getMultFAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMultF(mes);
    }
    
    public int getMultFAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMultF(mesi,mesf);
    }
    
    public int getMultFAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getMultFAnoMes(ano,mes);
    }
    
    public int getMultFAno(int ano){
        return this.getMultFAnoMeses(ano,0,11);
    }
    
    public int getMultFAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getMultFAno(ano);
    }
    
    //MisteF
    public int getMisteFAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMisteF(mes);
    }
    
    public int getMisteFAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMisteF(mesi,mesf);
    }
    
    public int getMisteFAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getMisteFAnoMes(ano,mes);
    }
    
    public int getMisteFAno(int ano){
        return this.getMisteFAnoMeses(ano,0,11);
    }
    
    public int getMisteFAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getMisteFAno(ano);
    }
    //VirtF
    public int getVirtFAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getVirtF(mes);
    }
    
    public int getVirtFAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getVirtF(mesi,mesf);
    }
    
    public int getVirtFAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getVirtFAnoMes(ano,mes);
    }
    
    public int getVirtFAno(int ano){
        return this.getVirtFAnoMeses(ano,0,11);
    }
    
    public int getVirtFAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getVirtFAno(ano);
    }
    
    //EventP
    public int getEventPAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getEventP(mes);
    }
    
    public int getEventPAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getEventP(mesi,mesf);
    }
    
    public int getEventPAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getEventPAnoMes(ano,mes);
    }
    
    public int getEventPAno(int ano){
        return this.getEventPAnoMeses(ano,0,11);
    }
    
    public int getEventPAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getEventPAno(ano);
    }
    
    //MicroH
    public int getMicroHAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMicroH(mes);
    }
    
    public int getMicroHAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMicroH(mesi,mesf);
    }
    
    public int getMicroHAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getMicroHAnoMes(ano,mes);
    }
    
    public int getMicroHAno(int ano){
        return this.getMicroHAnoMeses(ano,0,11);
    }
    
    public int getMicroHAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getMicroHAno(ano);
    }
    
    //MultH
    public int getMultHAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMultH(mes);
    }
    
    public int getMultHAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMultH(mesi,mesf);
    }
    
    public int getMultHAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getMultHAnoMes(ano,mes);
    }
    
    public int getMultHAno(int ano){
        return this.getMultHAnoMeses(ano,0,11);
    }
    
    public int getMultHAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getMultHAno(ano);
    }
    
    //MisteH
    public int getMisteHAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMisteH(mes);
    }
    
    public int getMisteHAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getMisteH(mesi,mesf);
    }
    
    public int getMisteHAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getMisteHAnoMes(ano,mes);
    }
    
    public int getMisteHAno(int ano){
        return this.getMisteHAnoMeses(ano,0,11);
    }
    
    public int getMisteHAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getMisteHAno(ano);
    }
    
    //VirtH
    public int getVirtHAnoMes(int ano, int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getVirtH(mes);
    }
    
    public int getVirtHAnoMeses(int ano, int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).getVirtH(mesi,mesf);
    }
    
    public int getVirtHAnoMesAtual(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return this.getVirtHAnoMes(ano,mes);
    }
    
    public int getVirtHAno(int ano){
        return this.getVirtHAnoMeses(ano,0,11);
    }
    
    public int getVirtHAnoAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return this.getVirtHAno(ano);
    }
    
    //adicionar pontos
    public void addPontosAnoMes(int ano, int mes,int p){
        this.addAno(ano);
        this.statics.get(ano).addPontuacao(mes,p);
    }
    
    public void addPontosAnoMesAtual(int p){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addPontosAnoMes(ano,mes,p);
    }
    //Found
    //MicroF
    public void addMicroFAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addMicroF(mes);
    }
    
    public void addMicroFAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addMicroFAnoMes(ano,mes);
    }
    //MultF;
    public void addMultFAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addMultF(mes);
    }
    
    public void addMultFAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addMultFAnoMes(ano,mes);
    }
    //MisteF;
    public void addMisteFAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addMisteF(mes);
    }
    
    public void addMisteFAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addMisteFAnoMes(ano,mes);
    }
    //VirtF;
    public void addVirtFAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addVirtF(mes);
    }
    
    public void addVirtFAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addVirtFAnoMes(ano,mes);
    }    
    //EventP;
    public void addEventPAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addEventP(mes);
    }
    
    public void addEventPAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addEventPAnoMes(ano,mes);
    } 
        //Hiden
    //microH
    public void addMicroHAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addMicroH(mes);
    }
    
    public void addMicroHAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addMicroHAnoMes(ano,mes);
    }
    //MultH;
    public void addMultHAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addMultH(mes);
    }
    
    public void addMultHAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addMultHAnoMes(ano,mes);
    }
    //MisteH;
    public void addMisteHAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addMisteH(mes);
    }
    
    public void addMisteHAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addMisteHAnoMes(ano,mes);
    }
    //VirtH;
    public void addVirtHAnoMes(int ano, int mes){
        this.addAno(ano);
        this.statics.get(ano).addVirtH(mes);
    }
    
    public void addVirtHAnoMesAtual(){
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        this.addVirtHAnoMes(ano,mes);
    }
    ///RAZOES
    public float razFoundHide(int ano, int mes){
       if(this.containsAno(ano)==false) return 0;
       return this.statics.get(ano).razFoundHide(mes);
    }
    public float razHideFound(int ano, int mes){
       if(this.containsAno(ano)==false) return 0;
       return this.statics.get(ano).razHideFound(mes);
    }
    
    public float razFoundHide(int ano, int mesi,int mesf){
       if(this.containsAno(ano)==false) return 0;
       return this.statics.get(ano).razFoundHide(mesi,mesf);
    }
    public float razHideFound(int ano, int mesi, int mesf){
       if(this.containsAno(ano)==false) return 0;
       return this.statics.get(ano).razHideFound(mesi,mesf);
    }
    public float razFoundHideAtualAno(){
       GregorianCalendar aux = new GregorianCalendar();
       int ano = aux.get(Calendar.YEAR);
       return this.statics.get(ano).razFoundHide(1,12);
    }
    public float razHideFoundAtualAno(){
       GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
       return this.statics.get(ano).razHideFound(1,12);
    }
    public float razFoundHideAtualAnoMes(){
       GregorianCalendar aux = new GregorianCalendar();
       int ano = aux.get(Calendar.YEAR);
       int mes = (aux.get(Calendar.MONTH))+1;
       return this.statics.get(ano).razFoundHide(mes);
    }
    public float razHideFoundAtualAnoMes(){
       GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
       return this.statics.get(ano).razHideFound(mes);
    }
    
    //Razoes MicroHF
    public float razMicroHFAnoMes(int ano ,int mes){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMicroHF(mes));
    }
    public float razMicroHFAnoMeses(int ano ,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMicroHF(mesi,mesf));
    }
    public float razMicroHFAno(int ano){
        if(this.containsAno(ano)==false) return 0;
        return(this.razMicroHFAnoMeses(ano,0,11));
    }
    public float razMicroHFAtualAnoMes(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return(this.razMicroHFAnoMes(ano,mes));
    }
    public float razMicroHFAtualAno(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return(this.razMicroHFAno(ano));
    }
    
    //Razoes MicroFH
    public float razMicroFHAnoMes(int ano ,int mes){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMicroFH(mes));
    }
    public float razMicroFHAnoMeses(int ano ,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMicroFH(mesi,mesf));
    }
    public float razMicroFHAno(int ano){
        if(this.containsAno(ano)==false) return 0;
        return(this.razMicroFHAnoMeses(ano,0,11));
    }
    public float razMicroFHAtualAnoMes(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return(this.razMicroFHAnoMes(ano,mes));
    }
    public float razMicroFHAtualAno(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return(this.razMicroFHAno(ano));
    }  
    
    
    //Razoes MisteHF
    public float razMisteHFAnoMes(int ano ,int mes){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMisteHF(mes));
    }
    public float razMisteHFAnoMeses(int ano ,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMisteHF(mesi,mesf));
    }
    public float razMisteHFAno(int ano){
        if(this.containsAno(ano)==false) return 0;
        return(this.razMisteHFAnoMeses(ano,0,11));
    }
    public float razMisteHFAtualAnoMes(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return(this.razMisteHFAnoMes(ano,mes));
    }
    public float razMisteHFAtualAno(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return(this.razMisteHFAno(ano));
    }
    
    //Razoes MisteFH
    public float razMisteFHAnoMes(int ano ,int mes){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMisteFH(mes));
    }
    public float razMisteFHAnoMeses(int ano ,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razMisteFH(mesi,mesf));
    }
    public float razMisteFHAno(int ano){
        if(this.containsAno(ano)==false) return 0;
        return(this.razMisteFHAnoMeses(ano,0,11));
    }
    public float razMisteFHAtualAnoMes(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return(this.razMisteFHAnoMes(ano,mes));
    }
    public float razMisteFHAtualAno(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return(this.razMisteFHAno(ano));
    }  
    //Razoes VirtHF
    public float razVirtHFAnoMes(int ano ,int mes){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razVirtHF(mes));
    }
    public float razVirtHFAnoMeses(int ano ,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razVirtHF(mesi,mesf));
    }
    public float razVirtHFAno(int ano){
        if(this.containsAno(ano)==false) return 0;
        return(this.razVirtHFAnoMeses(ano,0,11));
    }
    public float razVirtHFAtualAnoMes(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return(this.razVirtHFAnoMes(ano,mes));
    }
    public float razVirtHFAtualAno(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return(this.razVirtHFAno(ano));
    }
    
    //Razoes VirtFH
    public float razVirtFHAnoMes(int ano ,int mes){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razVirtFH(mes));
    }
    public float razVirtFHAnoMeses(int ano ,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return(this.statics.get(ano).razVirtFH(mesi,mesf));
    }
    public float razVirtFHAno(int ano){
        if(this.containsAno(ano)==false) return 0;
        return(this.razVirtFHAnoMeses(ano,0,11));
    }
    public float razVirtFHAtualAnoMes(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        int mes = (aux.get(Calendar.MONTH))+1;
        return(this.razVirtFHAnoMes(ano,mes));
    }
    public float razVirtFHAtualAno(){
        if(this.containsAtualAno()==false) return 0;
        GregorianCalendar aux = new GregorianCalendar();
        int ano = aux.get(Calendar.YEAR);
        return(this.razVirtFHAno(ano));
    }  
    //Razoes como total
    //MicroH
    public float razMicroTotalHAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMicroTotalH(mes);
    }
    public float razMicroTotalHAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMicroTotalH(mesi,mesf);
    }
    public float razMicroTotalHAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razMicroTotalHAnoMeses(ano,0,11);
    }
    public float razMicroTotalHAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razMicroTotalHAnoMes(ano,mes);
    }
        public float razMicroTotalHAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razMicroTotalHAno(ano);
    }
     //MicroF
    public float razMicroTotalFAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMicroTotalF(mes);
    }
    public float razMicroTotalFAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMicroTotalF(mesi,mesf);
    }
    public float razMicroTotalFAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razMicroTotalFAnoMeses(ano,0,11);
    }
    public float razMicroTotalFAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razMicroTotalFAnoMes(ano,mes);
    }
        public float razMicroTotalFAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razMicroTotalFAno(ano);
    }
    //MultH
    public float razMultTotalHAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMultTotalH(mes);
    }
    public float razMultTotalHAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMultTotalH(mesi,mesf);
    }
    public float razMultTotalHAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razMultTotalHAnoMeses(ano,0,11);
    }
    public float razMultTotalHAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razMultTotalHAnoMes(ano,mes);
    }
        public float razMultTotalHAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razMultTotalHAno(ano);
    }
     //MultF
    public float razMultTotalFAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMultTotalF(mes);
    }
    public float razMultTotalFAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMultTotalF(mesi,mesf);
    }
    public float razMultTotalFAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razMultTotalFAnoMeses(ano,0,11);
    }
    public float razMultTotalFAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razMultTotalFAnoMes(ano,mes);
    }
        public float razMultTotalFAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razMultTotalFAno(ano);
    }
    //MisteH
    public float razMisteTotalHAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMisteTotalH(mes);
    }
    public float razMisteTotalHAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMisteTotalH(mesi,mesf);
    }
    public float razMisteTotalHAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razMisteTotalHAnoMeses(ano,0,11);
    }
    public float razMisteTotalHAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razMisteTotalHAnoMes(ano,mes);
    }
        public float razMisteTotalHAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razMisteTotalHAno(ano);
    }
     //MisteF
    public float razMisteTotalFAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMisteTotalF(mes);
    }
    public float razMisteTotalFAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razMisteTotalF(mesi,mesf);
    }
    public float razMisteTotalFAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razMisteTotalFAnoMeses(ano,0,11);
    }
    public float razMisteTotalFAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razMisteTotalFAnoMes(ano,mes);
    }
        public float razMisteTotalFAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razMisteTotalFAno(ano);
    }
    //VirtH
    public float razVirtTotalHAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razVirtTotalH(mes);
    }
    public float razVirtTotalHAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razVirtTotalH(mesi,mesf);
    }
    public float razVirtTotalHAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razVirtTotalHAnoMeses(ano,0,11);
    }
    public float razVirtTotalHAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razVirtTotalHAnoMes(ano,mes);
    }
        public float razVirtTotalHAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razVirtTotalHAno(ano);
    }
     //VirtF
    public float razVirtTotalFAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razVirtTotalF(mes);
    }
    public float razVirtTotalFAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razVirtTotalF(mesi,mesf);
    }
    public float razVirtTotalFAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razVirtTotalFAnoMeses(ano,0,11);
    }
    public float razVirtTotalFAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razVirtTotalFAnoMes(ano,mes);
    }
        public float razVirtTotalFAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razVirtTotalFAno(ano);
    }
    
     //EventF
    public float razEventTotalFAnoMes(int ano,int mes){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razEventTotalF(mes);
    }
    public float razEventTotalFAnoMeses(int ano,int mesi,int mesf){
        if(this.containsAno(ano)==false) return 0;
        return this.statics.get(ano).razEventTotalP(mesi,mesf);
    }
    public float razEventTotalFAno(int ano){
         if(this.containsAno(ano)==false) return 0;
         return this.razEventTotalFAnoMeses(ano,0,11);
    }
    public float razEventTotalFAtualAnoMes(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         int mes = (aux.get(Calendar.MONTH))+1;
         return this.razEventTotalFAnoMes(ano,mes);
    }
        public float razEventTotalFAtualAno(){
         if(this.containsAtualAno()==false) return 0;
         GregorianCalendar aux = new GregorianCalendar();
         int ano = aux.get(Calendar.YEAR);
         return this.razEventTotalFAno(ano);
    }
}
