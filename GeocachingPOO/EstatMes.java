
/**
 * Write a description of class EstatMes here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.io.Serializable;
public class EstatMes implements Serializable
{
    // instance variables - replace the example below with your own
    private int pontuacao;
    private int found;
    private int hiden;
    
    //found
    private int microF;
    private int multF;
    private int misteF;
    private int virtF;
    private int eventP;
    //hiden
    private int microH;
    private int multH;
    private int misteH;
    private int virtH;
    
    private int mes;

    /*
    *Constuctores
    */
    /**
     * Empty Constructor for objects of class EstatMes
     */
    public EstatMes()
    {
        // initialise instance variables
        this.pontuacao=0;
        this.found=0;
        this.hiden=0;
        this.microF=0;
        this.multF=0;
        this.misteF=0;
        this.virtF=0;
        this.eventP=0;
        this.microH=0;
        this.multH=0;
        this.misteH=0;
        this.virtH=0;
        this.mes=0;
    }
    /**
     * Copy Constructor for objects of class EstatMes
     * 
     * @param  Object os class EstatMes to replicate
     * 
     */
    public EstatMes(EstatMes st)
    {
        // initialise instance variables
        this.pontuacao=st.getPontuacao();
        this.found=st.getFound();
        this.hiden=st.getHiden();
        this.multF=st.getMultF();
        this.virtF=st.getVirtF();
        this.multH=st.getMultH();
        this.virtH=st.getVirtH();
        this.microF=st.getMicroF();
        this.misteF=st.getMisteF();
        this.eventP=st.getEventP();
        this.microH=st.getMicroH();
        this.misteH=st.getMisteH();
        this.mes=st.getMes();
    }
    /**
     * Paramt Constructor for objects of class EstatMes
     *
     * @param  pontuacao  inicial do mes
     * @param  found   caches encontradas iniciais do mes     
     * @param  hiden  caches econdidas iniciais do mes 
     * @param "others" volores iniciais das estatisticas do mes
     *
     */
    public EstatMes(int pontuacao, int found, int hiden, int multF, int virtF, int multH, int virtH,int microF, int misteF, int eventP, int microH, int misteH, int mes)
    {
        // initialise instance variables
        this.pontuacao=pontuacao;
        this.found=found;
        this.hiden=hiden;
        this.multF=multF;
        this.virtF=virtF;
        this.multH=multH;
        this.virtH=virtH;
        this.microF=microF;
        this.misteF=misteF;
        this.eventP=eventP;
        this.microH=microH;
        this.misteH=misteH;
        this.mes=mes;
    }
    //construtor parcial
    public EstatMes(int mes)
    {
        // initialise instance variables
        this.pontuacao=0;
        this.found=0;
        this.hiden=0;
        this.microF=0;
        this.multF=0;
        this.misteF=0;
        this.virtF=0;
        this.eventP=0;
        this.microH=0;
        this.multH=0;
        this.misteH=0;
        this.virtH=0;
        this.mes=mes;
    }

    /*
    *GETES
    */
    public int getPontuacao(){return this.pontuacao;}
    public int getFound(){return this.found;}
    public int getHiden(){return this.hiden;}
    public int getMultF(){return this.multF;}
    public int getVirtF(){return this.virtF;}
    public int getMultH(){return this.multH;}
    public int getVirtH(){return this.virtH;}
    public int getMicroF(){return this.microF;}
    public int getMisteF(){return this.misteF;}
    public int getEventP(){return this.eventP;}
    public int getMicroH(){return this.microH;}
    public int getMisteH(){return this.misteH;}
    public int getMes(){return this.mes;}

    /*
    *SETERS
    */

    public void setPontuacao(int x){this.pontuacao=x;}
    public void setFound(int x){this.found=x;}
    public void setHiden(int x){this.hiden=x;}
    public void setMultF(int x){this.multF=x;}
    public void setVirtF(int x){this.virtF=x;}
    public void setMultH(int x){this.multH=x;}
    public void setVirtH(int x){this.virtH=x;}
    public void setMicroF(int x){this.microF=x;}
    public void setMisteF(int x){this.misteF=x;}
    public void setEventP(int x){this.eventP=x;}
    public void setMicroH(int x){this.microH=x;}
    public void setMisteH(int x){this.misteH=x;}
    public void setMes(int x){this.mes=x;}

    /*
     * Clone
     */
    public EstatMes clone(){return new EstatMes(this);}

    /*
     * toString
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("mes: " + this.mes + "\n");
        s.append("pontuaçao: " + this.pontuacao + "\n");
        s.append("encontradas: " + this.found + "\n");
        s.append("escondidas: " + this.hiden + "\n");
        s.append("microF: " + this.microF + "\n");
        s.append("multF: " + this.multF + "\n");
        s.append("misteF: " + this.misteF + "\n");
        s.append("virtF: " + this.virtF + "\n");
        s.append("eventP: " + this.eventP + "\n");
        s.append("microH: " + this.microH + "\n");
        s.append("multH: " + this.multH + "\n");
        s.append("misteH: " + this.misteH + "\n");
        s.append("virtH: " + this.virtH + "\n");
        return s.toString();       
    }
    /*
     * equals
     */
    public boolean equals(Object o){
        if (this==o) return true;
        if(this==null || o ==null || this.getClass()!=o.getClass()) return false;
        EstatMes e = (EstatMes)o;
        boolean ret = true;
        ret = ret && this.mes == e.getMes() && this.pontuacao==e.getPontuacao() && this.found==e.getFound();
        ret = ret && this.hiden==e.getHiden() && this.microF == e.getMicroF() && this.multF == e.getMultF();
        ret = ret && this.misteF==e.getMisteF() && this.virtF == e.getVirtF() &&  this.eventP == e.getEventP();
        ret = ret && this.microH==e.getMicroH() && this.multH==e.getMultH() && this.misteH==e.getMisteH();
        ret=  ret && this.virtH==e.getVirtH();
        return ret;
    }
    /*
     * hashCode
     */
    public int hashCode(){return(this.mes);}
    
    private void addFound(){this.found++;}
    private void addHiden(){this.hiden++;}


    /**
     * Metodos que adiciona uma determinada pontuaçao a um utilizador
     * 
     * @param pontuaçao a adicionar 
     */

    public void addPontuacao(int add){this.pontuacao+=add;}

    /**
     * Metodos de adicionar unitariamente aos "contadores" do historial do utilizador  
     */

    public void addMicroF(){this.addFound();this.microF++;}
    public void addMultF(){this.addFound();this.multF++;}
    public void addMisteF(){this.addFound();this.misteF++;}
    public void addVirtF(){this.addFound();this.virtF++;}
    public void addEventP(){this.addFound();this.eventP++;}

    public void addMicroH(){this.addHiden();this.microH++;}
    public void addMultH(){this.addHiden();this.multH++;}
    public void addMisteH(){this.addHiden();this.misteH++;}
    public void addVirtH(){this.addHiden();this.virtH++;}

    /**
     * Metodos de razoes estatisticas de 
     *relaçoes de todos os tipos de pontos do utilizador
     * 
     * @return     razao entre o primeiro no nome da funçao e o segundo 
     * (H) escondidas (F) encontradads 
     */

    public float razHideFound(){
        try{
            return(this.hiden / (float)this.found);
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razFoundHide(){
        try{
            return (this.found/this.hiden);
        }
        catch(ArithmeticException e){
            return 0;
        }
    }

    public float razMicroHF(){
        try{
            return(this.microH / (float)this.microF);
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razMicroFH(){
        try{
            return(this.microF / (float)this.microH);
        }
        catch(ArithmeticException e){
            return 0;
        }
    }

    public float razMisteHF(){
        try{
            return(this.misteH / (float)this.misteF);
            }
        catch(ArithmeticException e){
            return 0;
        }
      
    }
    
    public float razMisteFH(){
        try{
            return(this.misteF / (float)this.misteH);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }

    public float razVirtHF(){
        try{
            return(this.virtH / (float)this.virtF);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razVirtFH(){
        try{
            return(this.virtF / (float)this.virtH);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }

    public float razMicroTotalH(){
        try{
            return(this.microH/ (float)this.hiden);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razMultTotalH(){
        try{
            return(this.multH/ (float)this.hiden);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razMisteTotalH(){
        try{
            return(this.misteH/ (float)this.hiden);  
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razVirtTotalH(){
        try{
            return(this.virtH/ (float)this.hiden);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }

    public float razMicroTotalF(){
        try{
            return(this.microF/ (float)this.found);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razMultTotalF(){
        try{
            return(this.multF/ (float)this.found);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razMisteTotalF(){
        try{
            return(this.misteF/ (float)this.found);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razVirtTotalF(){
        try{
            return(this.virtF/ (float)this.found);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }
    public float razEventTotalF(){
        try{
            return(this.eventP/ (float)this.found);   
        }
        catch(ArithmeticException e){
            return 0;
        }
    }

}
