/**
 * Write a description of class UtilizadorNaoExisteException here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
public class JaExisteMailException extends Exception {
    public JaExisteMailException(String message){super(message);}
    public JaExisteMailException(){super();}
}