/**
 * Write a description of class VirtualCache here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.ArrayList;
import java.util.GregorianCalendar;
public class VirtualCache extends Cache
{
    // variaveis de instancia
    private ArrayList<Foto> album;
    
    /**
     * Construtor vazio para objectos da classe VirtualCache
     */
    public VirtualCache(){
        super();
        this.album=new ArrayList<Foto>();
    }
    
    /**
     * @param String utl, Email
     * @param Cordenada l, localização de cache
     * @param GregorianCalendar dt, data de criação de cache
     * @param String n, Nome da cache
     * @param String desc, descrição da cache
     * @param int dM, dificuldade mental
     * @param int dF, dificuldade física
     * @param int p, pontuação
     * Construtor parametrizado para objectos da classe VirtualCache 
     */
    public VirtualCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p){
        super(utl,l,dt,n,desc,dM,dF,p);
        this.album=new ArrayList<Foto>();
    }
    
    /**
     * @param VirtualCache vc
     * Construtor de copia para objectos da classe VirtualCache
     */
    public VirtualCache (VirtualCache vc){
        super(vc);
        this.album=vc.getAlbum();
    }

    /**
     * Método getAlbum
     * @return ArrayList<Foto> - lista de fotos
     */
    public ArrayList<Foto> getAlbum(){
        ArrayList<Foto> af = new ArrayList<Foto>();
        for(Foto f : this.album){af.add(f.clone());}
        return af;
    }
    
    /**
     * Método setAlbum
     * @param ArrayList<Foto> lista de fotos
     */
    private void setAlbum(ArrayList<Foto> al){
        ArrayList<Foto> af = new ArrayList<Foto>();
        for(Foto f : al){af.add(f.clone());}
        this.album= af;
    }
    
    /**
     * Método nFotos
     * @return int- numero de fotos do album
     */
    public int nFotos(){return this.getAlbum().size();}
    
    /**
     * Método addFoto
     * @param Foto f- foto para ser adicionada a lista de fotos da cache
     * @return boolean, se foi possivel adicionar
     */
    public boolean addFoto(Foto f){return this.album.add(f.clone());}
    
    /**
     * Método removeFoto
     * @param Foto f, foto para ser removida da lista de fotos da cache
     * @return boolean, se foi possivel remover
     */
    public boolean removeFoto(Foto f){return this.album.remove(f.clone());}
    
    /**
     * Método toString
     * @return String
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Fotos:\n");
        for(Foto f : this.getAlbum()){s.append(f.toString());}
        return s.toString();
    }
    
    /**
     * Método Clone
     * @return VirtualCache, copia do objecto
     */
    public VirtualCache clone(){return new VirtualCache(this);}
    
    /**
     * Método equals
     * @param Object obj, objeto a comparar
     * @return boolean, resultado da igualdade
     */
    public boolean equals(Object obj){
       boolean res = true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        VirtualCache vc = (VirtualCache) obj;
        if(!(super.equals(vc) && this.nFotos()==vc.nFotos())) return false;
        for(int i=0; res && i<this.nFotos(); i++){res = this.getAlbum().get(i).equals(vc.getAlbum().get(i));}
        return res;
    }
}
