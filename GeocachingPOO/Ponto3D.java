/**
 * Escreva a descrição da classe Ponto3D aqui.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import static java.lang.Math.abs;
class Ponto3D extends Ponto2D
{
    // Variáveis de instância 
    private double z;
    
    /**
     * @param double x
     * @param double y
     * @param double z
     * 
     * Construtor da Classe Ponto3D, definição do Ponto pelos valores recebidos como parâmetro
     */
     public Ponto3D (double x, double y, double z) {
      super(x, y); this.z = z ;
    }
    
    /**
     * 
     * Construtor vazio da Classe Ponto3D, inicializa a 0's
     */
    public Ponto3D () {super () ; z = 0.0; }
    
     /**
     * @param Ponto3D
     * 
     * Construtor da Classe Ponto3D
     */
    public Ponto3D(Ponto3D p){
        super(p);
        this.z = p.getZ();
        
    }
     
     /**
     * @return double
     * 
     * Geter - Função que nos da o valor de z  
     */
    public double getZ () {return z; }
    
    /**
     * @param double
     * 
     * Seter - Função que define o valor de z  
     */
    public void setZ(double z){this.z=z;}
   
    /**
     * @return String, para ser "Human Readble"
     * 
     * Converte para uma representação textual - toString()  
     */
    public String toString () {
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append (","); 
        s.append (this.getZ());
        return s.toString();
    }
    
    /**
     * @return Ponto3D
     * 
     * Cria uma copia do ponto recetor
     */
    public Ponto3D clone(){return new Ponto3D(this);}
    
    /**
     * 
     * @return    int HashCode do Ponto3D 
     * 
     */
    public int hashCode(){return this.toString().hashCode();}
    
    /**
     * @param Object
     * @return boolean, caso seja ou não igual
     * 
     * Confirma se ambos os objetos são iguais
     */
    public boolean equals(Object o){
        if (o==this) return true;
        if (o==null || this==null || o.getClass()!=this.getClass()) return false;
        Ponto3D p=(Ponto3D)o;
        boolean ret = super.equals(p);
        
        return (ret && this.getZ()==p.getZ());
    
    }
    
    //metodos
    
    /**
     * @param double dx
     * @param double dy
     * @param double dz
     *  
     * desloca um Ponto dependendo dos valores recebidos como parâmetros
     */
    public void desloca (double dx, double dy, double dz) {super.desloca (dx, dy); z += dz;}
    
    /**
     * @param Ponto3D 
     * @return double distancia entre 2 pontos
     * 
     * Calcula distancia entre dois pontos
     */
    public double distanciaPlano(Ponto3D p) { return super.distancia(p);}
    
    /**
     * @param Ponto3D 
     * @return double distancia entre 2 pontos
     * 
     * Calcula distancia entre dois pontos
     */
    public double distanciaEspaco(Ponto3D p){
        double d2 = this.distanciaPlano(p);
        double dz = this.varAlt(p);
        return Math.sqrt((d2*d2) + (dz*dz));
    }
    
    /**
     * @param Ponto3D 
     * @return double, valor variância de altura
     * 
     * Função que vê a diferença da altura entre 2 pontos
     */
    public double varAlt(Ponto3D p){return this.z - p.getZ();}
    
    /**
     * @param Ponto3D 
     * @return boolean, caso seja ou não o mais "alto"
     * 
     * Vê se o valor é superior ou não
     */ 
    public boolean maisAlto(Ponto3D p){return this.z>p.getZ();}
    
    /**
     * @param Ponto3D 
     * @return boolean, caso seja ou não o mais "baixo"
     * 
     * Vê se o valor é inferior ou não
     */
    public boolean maisBaixo(Ponto3D p){return this.z<p.getZ();}
    
    /**
     * 
     * @param int X
     * @param int Y
     * @param int Z
     * 
     * Altera o valor do ponto
     */   
    public void somaponto (double x, double y, double z) {
        super.somaPonto (x, y); this.z += z;
    }

    /**
     * @param Ponto3D 
     * 
     * soma os valores parâmetro e devolve um novo ponto
     */
    public void somaPonto (Ponto3D p) {
        super.somaPonto (p); z += p.getZ();
    }
    
    /**
     * @param Ponto3D 
     * @return Ponto3D a soma dos 2 pontos
     * 
     * soma os valores parâmetro e devolve um novo ponto
     */
    public Ponto3D somaPonto (double dx, double dy, double dz){
        return new Ponto3D (this.getX()+dx,this.getY()+dy,this.getZ()+dz);
    }
    

}
