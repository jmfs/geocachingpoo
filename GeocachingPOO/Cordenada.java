
/**
 * Write a description of class Cordenada here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import static java.lang.Math.abs;
import java.io.Serializable;
public class Cordenada implements Serializable
{
    /**
     * Vatiavel de classe com o raio da tera em KM
     */
    private static final double RTERRA = 6378.137;
    // instance variables
    private Ponto3D pt;
    
    //metodos de class
    
    /**
     * arredonda em double a um dado numero de casas decimais.
     * @param  value  valor a arredondar 
     * @param  places casas decimais para arredondar
     * @return        valor arredondado
     */
    private static double round(double value, int places) {
        if (places < 0) return value;

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    /**
     * converte uma coordenada em DDºMM.MM e a orientação no seu valor  2d correspondente
     * @param  co  DD da coordenada
     * @param  min MM.MM da cordeada
     * @param  or  orientnçao da cordenada  (N,S,E,W,O)
     * @return     retorna o ponto da cordenada no plano
     */
    private static double converte(int co, double min, char or){
        double pt = co + min/60;
        if(or=='s' || or=='S' || or=='O' || or =='o'|| or=='W' || or =='w'){
            pt = -1*pt;
        }
        return pt;
    }
    /**
     * Converte um grau em radianos
     * @param  x  grau a converter
     * @return   radiano 
     */
    private static double rad(double x){return x*Math.PI/180;}

    //metodos instancia
    /**
     * Contrutor vazio de Cordenada
     * @return nova Cordenada
     */
    public Cordenada(){
        this.pt= new Ponto3D();
    }
    /**
     * Contrutor de copia de Cordenada
     * @return nova Cordenada
     */
    private Cordenada(Cordenada c){
        this.pt = c.getPonto();
    }
    /**
     * Contrutor parametrizado de Cordenada
     * @return nova Cordenada
     */
    public Cordenada(int laco, double lamin, char laor, int lco, double lmin, char lor, double alt){
        this.pt = new Ponto3D(converte(lco,lmin,lor),converte(laco,lamin,laor),alt);
    }
    
    /**
     *  redefiniçao do toSring de Object
     * @return      string com a info da dordenada
     */
    public String toString(){return this.pt.toString();}
    
    /**
     * [clone description]
     * @param  Cordenada(this) cordenada a clonar
     * @return                copia do objeto que recebeu a mensagem
     */
    public Cordenada clone(){return new Cordenada(this);}
    
    /**
     * Redefiniçao do equals de object
     * @param  object a comparar com a cordenada que recebe a mensagem
     * @return   igualdade dos dois objetos
     */
    public boolean equals(Object o){
        if (o==this) return true;
        if (o==null || this==null || o.getClass()!=this.getClass()) return false;
        Cordenada c=(Cordenada)o;
        return (this.pt.equals(c.getPonto()));
    
    }
   /**
    * redefiniçao do metodo hashCode de object
    * @param  this
    * @return      o hashCode da cordenda
    */
    public int hashCode(){return this.toString().hashCode();}
    
    /**
     * Aterra todos os parametros a uma cordenada existente
     * @param laco      DD da Latitude
     * @param lamin     MM.MM da latitude
     * @param laor      Orientaçao da latitude (N,S)
     * @param lco       DD da longitude
     * @param lmin      MM.MM da longitude
     * @param lor       Orientaçao da longitude (E,O,W)
     * @param alt       altitude
     */
    public void setCoordenada(int laco, double lamin, char laor, int lco, double lmin, char lor, double alt){this.pt = new Ponto3D(converte(lco,lmin,lor),converte(laco,lamin,laor),alt);}
    
    /**
     * GETTER da Altitude
     * @param  this
     * @return      Altitude da cordenada
     */
    public double getAlt(){return this.pt.getZ();}
    /**
     * GETTER do Ponto3D
     * @param  this
     * @return     retorna copia do ponto 3D da cordenada
     */
    public Ponto3D getPonto(){return this.pt.clone();}


    /**
     * Calcula a distancia em KM entre duas cordenadas
     * @param  co cordanada a medir a distancia relativamente ao this
     * @return    distancia em KM das duas cordenadas
     */
    public double distKM(Cordenada co){
        double dLat  = rad( co.getPonto().getY() - this.getPonto().getY());
        double dLong = rad( co.getPonto().getX() - this.getPonto().getX());
        
        double a = (Math.sin(dLat/2) * Math.sin(dLat/2)) + Math.cos(rad(this.getPonto().getY())) * Math.cos(rad(co.getPonto().getY())) * Math.sin(dLong/2) * Math.sin(dLong/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        
        return c*RTERRA;
    }
 
    
    /**
     * Calcula a distancia em Metros entre duas cordenadas
     * @param  co cordanada a medir a distancia relativamente ao this
     * @return    distancia em Metros das duas cordenadas
     */
    public double distM(Cordenada co){return this.distKM(co)*1000;}
    
    /**
     * Verifica se duas cordenadas estao mais perto uma da outra do que uma dadta distancia em metros
     * @param  co  Cordenada em comparaçao
     * @param  dm Distancia de referncia metros 
     * @return    se codenada esta mais perto do this do que dm
     */
    
    public boolean maisPertoM(Cordenada co, double dm){return (this.distM(co)<dm);}

    /**
     * Verifica se duas cordenas estao mais perto uma da outra do que uma dadta distancia em KM
     * @param  co  Cordenada em comparaçao
     * @param  dKm Distancia de referncia KM 
     * @return    se codenada esta mais perto do this do que dm
     */
    public boolean maisPertoKM(Cordenada co, double dKm){return (this.distKM(co)<dKm);}
    
    /**
     * Cria uma string la latitude como é comim ver nas cordenadas gps (DDºMM.MM O)
     * @return String no formato comum (DDºMM.MM O)
     */
    public String latitForm(){
        double num = this.getPonto().getY();
        long iPart = (long) Math.abs(num);
        double fPart = (double)Math.round((Math.abs(num) -iPart)*1000)/1000;
        char or='N';
        if(num<0) or='S';
        return ( iPart +"º" + round((fPart *60),3) + "\" " + or);
    } 
    
    /**
     * Cria uma string la longitude como é comim ver nas cordenadas gps (DDºMM.MM O)
     * @return String no formato comum (DDºMM.MM O)
     */
    public String longitForm(){
        double num = this.getPonto().getX();
        long iPart = (long) Math.abs(num);
        double fPart = (double)Math.round((Math.abs(num) -iPart)*1000)/1000;
        char or='E';
        if(num<0) or='O';
        return ( iPart +"º" + round((fPart *60),3) + "\" " + or );
    }
    
    /**
     * Verifica se this esta mais a norte que c
     * @param  c  Cordenada em comparaçao
     * @return    se this esta mais a norte do this do que c
     */
    public boolean maisNorte(Cordenada c){return this.getPonto().acima(c.getPonto());}
    /**
     * Verifica se this esta mais a alta que c
     * @param  c  Cordenada em comparaçao
     * @return    se this esta mais a alta do this do que c
     */
    public boolean maisAlto(Cordenada c){return this.getPonto().maisAlto(c.getPonto());}
    /**
     * Verifica se this esta mais a este que c
     * @param  c  Cordenada em comparaçao
     * @return    se this esta mais a este do this do que c
     */
    public boolean maisEste(Cordenada c){return this.getPonto().direita(c.getPonto());}
    
    /**
     * Verifica se this esta mais a sul que c
     * @param  c  Cordenada em comparaçao
     * @return    se this esta mais a sul do this do que c
     */
    public boolean maisSul(Cordenada c){return (!this.maisNorte(c));}
    /**
     * Verifica se this esta mais  baixa que c
     * @param  c  Cordenada em comparaçao
     * @return    se this esta mais baixa do this do que c
     */
    public boolean maisBaixo(Cordenada c){return (!this.maisAlto(c));}
    /**
     * Verifica se this esta mais a oeste que c
     * @param  c  Cordenada em comparaçao
     * @return    se this esta mais a oeste do this do que c
     */
    public boolean maisOeste(Cordenada c){return (!this.maisEste(c));}
    
    
    
}
