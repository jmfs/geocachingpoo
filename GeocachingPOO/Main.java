
/**
 * Write a description of class Main here.
 * 
 * @author Grupo55 POO 2014/2015
 * @version 28-05-2015
 */
import java.util.Scanner;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.HashMap;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.NumberFormatException;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Calendar;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Main
{
    // instance variables - replace the example below with your own
    private static GeocachingPOO app=new GeocachingPOO() ;
    private static Scanner input = new Scanner(System.in);
    private static String lixo;
    private static String onLogin;
    private static int premi;
    private static int v=0;
    /**
     * MAIN
     */
      
    private static void simulaEvent(){
        try{
            int id;
            System.out.println("Diga ID do Evento a simular:");
            System.out.print(">>");
            id = lerInt();
            if(app.existeEvento(id)){
                if(!app.eventoEnd(id)){
                    System.out.println("----SIMULAÇÃO EVENTO "+ id +"----");
                    app.simulaEveto(id,System.out);
                }
                else System.out.println("O Evento "+ id+ " já foi simulado.");
            }else{
                System.out.println("O Evento " + id + " não existe na APP.");
            }
        }
        catch(ErroSimulacaoException e){
            System.out.println("ERRO " + e.getMessage());
        }
    }
    private static void detalheEvento(){
        int id;
        System.out.println("Diga ID do Evento a consultar:");
        System.out.print(">>");
        id = lerInt();
        try{
            System.out.println(app.detalheEvento(id));
        }catch (EventoNaoExisteException e){
            System.out.println("O Evento " + id + " não existe na APP");
        }
    }
    private static void criarEvento(){
         Local loc;
         String nome,desc;
         Double raio;
         int maxpart,npist,i,dif;
         ArrayList<String> pistas = new ArrayList<String>();
         GregorianCalendar dCreat,endInsc,inEvent,endEvent;
         ArrayList<Integer> conj;
         System.out.println("----Menu Criação Evento----");
         System.out.println("Diga o nome do evento:");
         System.out.print(">>");
         nome=lerTexto();
         
         loc= criarLocal();
         
         System.out.println("Diga o raio para criação do evento em KM (1 a 5):");
         System.out.print(">>");
         raio=lerDouble();
         if(raio<1.0) raio=1.0;
         if(raio>5.0) raio=5.0;
         
         conj = app.listaParaEvento(loc.getGPS(),raio);
         System.out.println("No raio de " + raio + "KM foram adicionadas " + conj.size()+ " Caches:");
         for(Integer caI : conj){
             System.out.println("\t"+caI + "(" + app.dizTipoCache(caI)+ ");");
          }
         System.out.println("Data de criação(Inicio das inscrições):");
         dCreat=lerData();
         System.out.println("Data de final de inscrições:");
         endInsc=lerData();
         while(endInsc.compareTo(dCreat) <=0){
             System.out.println("A data final de Inscrições deve ser \"Maior\" do que a de Criação(Inicio das inscrições)");
             endInsc=lerData();
         }
         System.out.println("Data inicio evento:");
         inEvent=lerData();
         while(inEvent.compareTo(endInsc) <=0){
             System.out.println("A data de inicio do evento deve \"Maior\" do que a final de Inscrições");
             inEvent=lerData();
         }
         System.out.println("Data final evento:");
         endEvent=lerData();
          while(endEvent.compareTo(inEvent) <=0){
             System.out.println("A data de fim do evento deve \"Maior\" do que a de inicio");
             endEvent=lerData();
         }
         System.out.println("Diga a descrição do evento:");
         System.out.print(">>");
         desc=lerTexto();
         
         System.out.println("Diga o máximo de participantes:");
         System.out.print(">>");
         maxpart=lerInt();
         
         dif = raio.intValue();
         System.out.println("Quantas pistas deseja adicionar ? (0..10)");
         System.out.print(">>");
         npist=lerInt();
         if(npist<0) npist=0;
         if(npist>10) npist=10;
         i=0;
         String pista;
         while(i<npist){
             System.out.println("Criação da pista " + i + " de " + npist );
             System.out.println("Diga a pista que deseja adicionar:");
             System.out.print(">>");
             pista=lerTexto();
             pistas.add(pista);
             i++; 
        }
        app.addEvent(nome,loc,raio,conj,new HashMap<String,Integer>(),dCreat,endInsc,inEvent,endEvent,desc,dif,pistas,maxpart);
        System.out.println("Criou com sucesso o novo evento");
    }
   
    private static Local criarLocal(){
        String pais,cidade,rua,cp;
       
        System.out.println("Criação do Local");
        System.out.println("Diga as Coordenadas:");
        Cordenada c = criarCoordenada();
        
        System.out.println("Diga o Pais:");
        System.out.print(">>");
        pais = lerTexto();
        
        System.out.println("Diga a Cidade:");
        System.out.print(">>");
        cidade = lerTexto();
        
        System.out.println("Diga a Rua:");
        System.out.print(">>");
        rua = lerTexto();
        
        System.out.println("Diga o Código Postal:");
        System.out.print(">>");
        cp = lerTexto();
        return new Local(pais,cidade,rua,cp,c);
    }
    private static void atualizarInfoCaches(){
        int op,id,dif;
        String nome;
        Cordenada cord;

        System.out.println("-------Menu Atualizar Cache--------");
        System.out.println("Diga o id da Cache que deseja atualizar:");
        System.out.print(">>");
        id=lerInt();
        try{
            if(!(app.adminMode() || !app.hidenByMe(id))) {
                System.out.println("Não possui permissões para atualizar esta Cache");
            }else{
                System.out.println("-------Atualizar Cache--------");
                System.out.println("1-Alterar Nome;");
                System.out.println("2-Alterar Coordenadas;");
                System.out.println("3-Alterar Descrição;");
                System.out.println("4-Alterar Dificuldade Mental;");
                System.out.println("5-Alterar Dificuldade Física;");
                System.out.println("6-Adicionar Pista;");
                System.out.println("7-Remover Pista;");
                System.out.println("8-Adicionar Ponto de Interesse;");
                if(app.isFisic(id)){
                    System.out.println("9-Adicionar Itens para troca;");
                    System.out.println("10-Adicionar CoinItem;");   
                }
                else{
                    System.out.println("9-Adicionar Foto;");
                }

                System.out.println("Outro-Sair;");
                System.out.print(">>");
                op=lerInt();
                if(app.isFisic(id) && op >10){
                    op=20;    
                }
                if(!app.isFisic(id) && op >8){
                    if(op==9) op=11;
                    else op=20;
                }
        
                switch (op){
                    case 1 : 
                    {    
                        System.out.println("Diga o novo nome:");
                        nome=lerTexto();
                        app.alterarNomeCache(id,nome);
                        break;
                    }
                    case 2 : 
                    {
                        cord=criarCoordenada();
                        app.alterarCordenadasCache(id,cord);
                        break;
                    }
                    case 3 : 
                    {
                        System.out.println("Escreva a nova descrição:");
                        nome=lerTexto();
                        app.alterarDescCache(id,nome);
                        break;
                    }
                    case 4 : 
                    {
                        dif=lerDif("mental");
                        app.alterarDMentalCache(id,dif);
                        break;
                    }
                    case 5 : 
                    {
                        dif=lerDif("física");
                        app.alterarDMentalCache(id,dif);
                        break;
                    }
                    case 6 : 
                    {
                        System.out.println("Diga a; pista que deseja adicionar:");
                        System.out.print(">>");
                        nome=lerTexto();
                        app.adicionarPistaCache(id,nome);
                        break;
                    }
                    case 7 : 
                    {
                        ArrayList <String> pistas = app.getPistasCache(id);
                        int ct = 0;
                        nome="";
                        if(pistas==null){
                            System.out.println("ERRO ao listar as pistas");
                            return;
                        }
                        System.out.println("A Cache " + id +" tem " + pistas.size()+ " pistas:");
                        for(String ps : pistas){
                            System.out.println("\n" + ps + ";\n");
                        }
                        do{
                            if(ct>0) System.out.println("A pista " + nome +" não se encontra nas pistas de " + id);
                            System.out.println("Diga a pista que deseja remover:");
                            System.out.print(">>");
                            nome=lerTexto();
                            ct++;
                        }while(!pistas.contains(nome) && ct<2);
                        if(ct==2){
                            System.out.println("Atingiu o limite de tentativas para altear a pista");
                            return;
                        }
                        app.removerPistaCache(id,nome);
                        break;
                    }
                    case 8 : 
                    {
                        cord=criarCoordenada();
                        app.adicionarPontIntCache(id,cord);
                        break;
                    }
                    case 9 : 
                    {
                        int nItens;
                        System.out.println("Diga o numero de Itens que deseja criar");
                        System.out.print(">>");
                        nItens=lerInt();
                        ArrayList<Item> lista = new ArrayList<Item>(nItens);
                        for(int i=0; i<nItens;i++){
                            lista.add(criarItemnormal().clone()); break;
                        }
                        app.addLisItem(id,lista);
                        break;
                    }
                    case 10:
                    {
                        CoinItem novo = crairCoinItem(null);
                        app.addCoinItem(id,novo.clone());
                        break;
                    }
                    case 11:
                    {
                        leaveFoto(id);
                        break;
                    }
                }
            }
        }catch(CacheNaoExisteException e){
            System.out.println("A Cache " + e.getMessage() + " não existe");
        }catch(CacheTipoErradoException e){
            System.out.println(e.getMessage());
        }
    
    }
    
    
    
    
     //       System.out.println("16-Ver participantes do Evento;");
    private static void lsitPartcipORD(){
        int id;
        System.out.println("Diga ID do Evento a pesquisar:");
        System.out.print(">>");
        id = lerInt();
        ArrayList<String> particp = new ArrayList<String>();
        try{
           particp.addAll(app.particpEventORD(id));
           System.out.println("Existem " + particp.size()+ " participantes inscritos no evento " + id);
           for(String pt: particp){
               System.out.println("\n"+ pt +";");
            }
        }catch(EventoNaoExisteException e){
           System.out.println("Não existe na APP o Evento " + e.getMessage());
        }
        
    }
    
     
    private static int jogarPregunta(Pergunta pg){
        String pergunta =pg.getPergunta();
        ArrayList<String> opResp = new ArrayList<String>();
        opResp.addAll(pg.getOpResp());
        String correcta=pg.getCorrecta();
        int opI;
        int pont=0;
        int i=1;
        System.out.println("Pergunta: " + pergunta);
        for(String op : opResp){
            System.out.println("\tOp " + i+ "-> "+ op);
            i++;
        }
        System.out.println("Diga a opção que considera Correta:");
        System.out.print(">>");
        opI = lerInt();
        while(opI<1 || opI> opResp.size()){
            System.out.println("Apenas Existem " + opResp.size() + " opçoes de resposta");
            System.out.println("Diga a opção que considera Correta:");
            System.out.print(">>");
            opI = lerInt();    
        }
        if(opResp.get(opI-1).equals(correcta)){
            System.out.println("Parabéns acertou na resposta");
            pont=pg.getPont();
        }
        else{
            System.out.println("Resposta Errada");
        }
        return pont;
    }
    private static int jogarMisterio(Integer id){
        int pont=0;
        int i=1,size;
        Puzzel p;
        try{
            p= app.getPuzzelOff(id);
            System.out.println("Esta a jogar o Puzzle " + p.getNome() + " da Cache :" + id);
            size= p.getPerguntas().size();
            for(Pergunta pg : p.getPerguntas()){
                System.out.println("Está na pergunta nº " + i+" de " + size +";");
                pont+=jogarPregunta(pg.clone()); 
            }
            return pont;
        }catch(CacheNaoExisteException e){
            System.out.println("Não Existe a Cache " + e.getMessage());
            return 0;
        }catch(CacheTipoErradoException e){
            System.out.println(e.getMessage());
            return 0;
        }
    }
     
    private static String formataData(GregorianCalendar data){
        return data.get(Calendar.DAY_OF_MONTH)+ "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR);
    }
    private static void eventRegist(){
        int id;
        GregorianCalendar dt;
        System.out.println("Diga ID do Evento a registar:");
        System.out.print(">>");
        id = lerInt();
        try{
            if(app.jameIcreviEvent(id)){
                System.out.println("Já esta inscrito no evento: " + id);    
            }
            else{
                dt = lerData();
                if(app.eventoEnd(id) || dt.compareTo(app.endIncriEvent(id))>0){
                    System.out.println("Data de inscrição já terminada.");
                }
                else{
                    if(!app.possiIncreEvent(id)){
                        System.out.println("Numero máximo de inscritos já atingido");
                    }
                    else{
                        app.addMeEvent(id,dt);
                        System.out.println("Inscreveu-se com sucesso no evento: " +id + " realiza-se no dia " + formataData(app.getdayEvent(id)) );
                    }
                }
            }
        }catch(EventoNaoExisteException e){
            System.out.println("O Evento " + e.getMessage()+ " não existe.");
        }catch(UtilizadorNaoExisteException e){
            System.out.println("Utilizador " +  e.getMessage()+ " não existe.");
        }
    }
   
    
    private static void cacheDetalhe(){
        int id;
        System.out.println("Diga ID da Cache a consultar:");
        System.out.print(">>");
        id = lerInt();
        try{
            System.out.println(app.cacheDeatils(id));
        }
        catch(CacheNaoExisteException e){
            
            System.out.println("ERRO a Cache: " + e.getMessage() +" não se encontra na APP");
        }
    }
    
    private static void myStatsAno(){
        int ano;
        System.out.println("Diga o ano:");
        System.out.print(">>");
        ano = lerInt();
        while(ano<1900 || ano>9999){
            System.out.println("Ano invalido diga de novo:");
            System.out.print(">>");
            ano = lerInt();
            
        }
        try{
            System.out.println(app.myStatsAno(ano));
        }catch(NaoHaEstatisticasException e){
            System.out.println(e.getMessage());
            
        }
    }
    
    private static void listaMaisPerto(){
        double dist;
        System.out.println("Diga o distancia (KM):");
        System.out.print(">>");
        //ver para ler double
        dist = lerDouble();
        while(dist<0.1 || dist>20.0){
            System.out.println("Distancia deve ser entre 0.1KM e 20.0KM:");
            System.out.print(">>");
            //ver para ler double
            dist = input.nextDouble();
            
        }
        Cordenada c = criarCoordenada();
        System.out.println(app.listaMaisPerto(c,dist));
    }
    private static void myStatsAnoMes(){
        int ano,mes;
        System.out.println("Diga o ano:");
        System.out.print(">>");
        ano = lerInt();
        while(ano<1900 || ano>9999){
            System.out.println("Ano invalido diga de novo:");
            System.out.print(">>");
            ano = lerInt();
            
        }
        System.out.println("Diga o mês:");
        System.out.print(">>");
        mes = lerInt();
        while(mes<1 || mes>12){
            System.out.println("Mês invalido diga de novo:");
            System.out.print(">>");
            mes = lerInt();
            
        }
        try{
            System.out.println(app.myStatsAnoMes(ano,mes-1));
        }catch(NaoHaEstatisticasException e){
            System.out.println(e.getMessage());
            
        }
    }
    
    private static void userChangeMorada(){
        String pais;
        String cidade;
        String rua;
        String cp;
        if(comparPASS(onLogin)){
            System.out.println("Diga o novo Pais:");
            System.out.print(">>");
            pais = lerTexto();
        
            System.out.println("Diga a nova Cidade:");
            System.out.print(">>");
            cidade = lerTexto();
        
            System.out.println("Diga a nova Rua:");
            System.out.print(">>");
            rua = lerTexto();
        
            System.out.println("Diga o novo Código Postal:");
            System.out.print(">>");
            cp = lerTexto();
            app.alteraMyMorada(pais,cidade,rua,cp);
            System.out.println("A sua morada foi alterada com sucesso");
        }
    }

    
    //Comuns
    private static void limparEcra(){
        for (int i = 0; i < 30; i++) System.out.println("\n"); 
    }

    private static String lerEmail(){
        String email=lerPalavra();
        while ( !email.contains("@") || (email.indexOf("@")!=email.lastIndexOf("@")) || !email.substring(email.length()-email.indexOf("@"),email.length()).contains(".") ) {
            System.out.println("Formato de Email invalido!(Repita)");
            System.out.print(">>");
            email=lerPalavra();
        }
        return email;
    }
    
    private static Cordenada criarCoordenada(){
        int latn;
        int longn;
        double latm;
        double longm;
        char eOo;
        char nOs;
        double alt;
        int i=0;
        String cord;
        try{
            do{
                if(i!=0) System.out.println("Formato invalido:");
                System.out.println("Formato: IIIºMM.MMO_IIIºMM.MMO_AA");
                System.out.print(">>");
                cord=lerPalavra();
        
                String[] array1 = cord.split("_");
                String[] array11 = array1[0].split("º");
                String[] array12 = array1[1].split("º");
                alt= Double.parseDouble(array1[2]);
                latn = Integer.parseInt(array11[0]);
                longn =Integer.parseInt(array12[0]);
        
                latm = Double.parseDouble(array11[1].substring(0, array11[1].length()-1));
                longm= Double.parseDouble(array12[1].substring(0, array12[1].length()-1));
        
                eOo = array12[1].charAt(array12[1].length()-1);
                nOs = array11[1].charAt(array11[1].length()-1);
                i++;
            }while(latn<0 || latn>180 || latm<0.0 || latm>60.0 || longn<0 || longn>180 || longm<0.0 || longm>60.0  || !(eOo == 'E' || eOo == 'e' || eOo == 'O' || eOo == 'o') ||
            !(nOs == 'N' || nOs == 'n' || nOs == 'S' || nOs == 's') || alt<-60.0 || alt>3000.0);
            return new Cordenada(latn,latm,nOs,longn,longm,eOo,alt);
        }
        catch(NumberFormatException ex){
            System.out.println("Formato invalido:");
            return criarCoordenada();
        }
        catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("Formato invalido:");
            return criarCoordenada();
        }
    }

    private static String lerPalavra() {
       // int i=0;
        String s = input.nextLine();
        String[] array = s.split(" ");
        if(array[0].equals("")){
            s = input.nextLine();
            array = s.split(" ");
        }
        while(array.length>1 || array[0].equals("")){
            System.out.println("Repita(ERRO):");
            System.out.print(">>");
            s = input.nextLine();
            array = s.split(" ");
        }
        return array[0];
    }
    
    private static String lerTexto(){
        String nome = new String();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));   
  
        try{   
            nome = br.readLine();     
        }   
        catch (IOException io){   
            System.err.println(io.toString());   
        } 
        return nome;
    }
    
    private static int lerInt(){
        while (!input.hasNextInt()) {
            System.out.println("Escreva um inteiro.");
            System.out.print(">>");
            input.nextLine();
        }
        int num = input.nextInt();
        return num;
    }
    
    private static double lerDouble(){
        while (!input.hasNextDouble()) {
            System.out.println("Escreva um Double.");
            System.out.print(">>");
            input.nextLine();
        }
        double num = input.nextInt();
        return num;
    }
    //acho que est mal
    private static ArrayList<Item> lerItens(){
        int nItens;
        
        System.out.println("Menu Criar Item");
        System.out.println("Diga o numero de Itens que deseja criar");
        System.out.print(">>");
        nItens=lerInt();
        ArrayList<Item> lista = new ArrayList<Item>(nItens);
        for(int i=0; i<nItens;i++){

            System.out.println("Que tipo de item deseja criar:");
            System.out.println("1-Normal;");
            System.out.println("2-CoinItem;");
            System.out.print(">>");
            int tip = lerInt();
            while(tip<1||tip>2){
                System.out.println("ERRO: opção invalida.");
                
                System.out.println("Que tipo de item deseja criar:");
                System.out.println("1-Normal;");
                System.out.println("2-CoinItem;");
                System.out.print(">>");
                tip = lerInt();
            }
          
            switch (tip){
                case 1: lista.add(criarItemnormal().clone()); break;
                case 2: lista.add(crairCoinItem(null).clone()); break;
            }
            
        }
        return lista;
    }
    
    private static  Item criarItemnormal(){
        String nome = new String();
        int valor;
        System.out.println("Diga o nome do item:");
        System.out.print(">>");
        nome=lerTexto();
        System.out.println("Diga o valor do item [1..5]:");
        System.out.print(">>");
        valor=lerInt();
        while(valor<1 || valor>5){
            System.out.println("Diga outra vez valor incorreto");
            System.out.print(">>");
            valor=lerInt();
        }
        return new Item(nome,valor);
    }
    
    private static CoinItem crairCoinItem(GregorianCalendar dt2){
        GregorianCalendar dt;
        String nome;
        System.out.println("Diga o nome do item:");
        System.out.print(">>");
        nome=lerTexto();
        if(dt2==null){
            dt = lerData();
            return new CoinItem(nome,5,dt);//coinitem tem valor maximo
        }
        return new CoinItem(nome,5,(GregorianCalendar)dt2.clone());//coinitem tem valor maximo
    }
    
    
    private static String registaNovoMail(){
        String mail;
        System.out.println("Diga email para registo:");
        System.out.print(">>");
        mail = lerEmail();
            while(app.jaExisteIDuseradmin(mail)){
                System.out.println("ERRO id "+ mail +" já existe, Impossível Criar");
                System.out.println("Diga outro email");
                System.out.print(">>");
                mail = lerEmail();
           
            }
            return mail;
    }
    
    private static String registaNovaPASS(){
        String pass1;
        String pass2;
        System.out.println("Diga a nova password:");
        System.out.print(">>");
        pass1 = lerPalavra();
        System.out.println("Repita a nova password:");
        System.out.print(">>");
        pass2 = lerPalavra();
        while(pass1.equals(pass2)==false){
            System.out.println("ERRO as passwords não são iguais.");
            System.out.println("Diga password:");
            System.out.print(">>");
            pass1 = lerPalavra();
            System.out.println("Repita password:");
            System.out.print(">>");
            pass2 = lerPalavra();  
        }
        return pass1;
    }
    
    private static GregorianCalendar lerData(){
        int dd,mm,yy,i=0;
        try{
            do{
                if(i==0)System.out.println("Data: (dd-mm-yyyy): ");
                else System.out.println("Data invalida: (dd-mm-yyyy): ");
                System.out.print(">>");
                String nascimento=lerPalavra(); 
            
                while (nascimento.length()!=10 || nascimento.charAt(3)!='-' && nascimento.charAt(5)!='-') {
                    System.out.println("Formato Invalido!");
                    System.out.println("Data: (dd-mm-yyyy): ");
                    System.out.print(">>");
                    nascimento=lerPalavra();
                    String[] date = nascimento.split("-");
                }
                String[] date = nascimento.split("-");
                dd = Integer.parseInt(date[0]);
                mm = Integer.parseInt(date[1]);
                yy = Integer.parseInt(date[2]); 
                i++;
            }
            while (dd<1 || dd>31 || mm<1 || mm>12 || yy<1900 || yy>9999);   
            return new GregorianCalendar(yy,mm-1,dd);
        }
        catch(NumberFormatException ex){
            System.out.println("Formato Invalido!");
            return lerData();
        }
    }
    
    private static boolean comparPASS(String mail){
        String pass;
        String verif;
        int i=0;
        try{
            verif = app.getPassDe(mail);
            System.out.println("Diga a sua password:");
            System.out.print(">>");
            pass = lerPalavra();
            while(i<2 && !pass.equals(verif)){
                System.out.println("Password errada diga de novo (restam " + (2-i) +" tentativas):");
                System.out.print(">>");
                pass = lerPalavra();
                i++;
            }
            if(i==2){
                System.out.println("Atingiu limite de tentativas (3).");
                return false;
            }
            return true;
        }
        catch(UtilizadorNaoExisteException e){
            System.out.println("Não existe o utilizador "+ e.getMessage());
            return false;
        }catch(AdminNaoExisteException e){
            System.out.println("Não existe o ADMIN "+ e.getMessage());
            return false;
        }
    }
    private static Pergunta criaPergunta(){
        String perg;
        String op1;
        String op2;
        int correcta;
        String cor;
        int pont;
        
        System.out.println("----------Criar Pergunta----------");
        System.out.println("---------------------------------");
        System.out.println("Diga a Pergunta:");
        System.out.print(">>");
        perg=lerTexto();
        System.out.println("Opção de resposta 1:");
        System.out.print(">>");
        op1=lerTexto();
        System.out.println("Opção de resposta 2:");
        System.out.print(">>");
        op2=lerTexto();
        System.out.println("Qual a opção correta?");
        System.out.print(">>");
        correcta=lerInt();
        while(correcta<1 || correcta>2){
            System.out.println("Opção incorreta:(insira outra vez)");
            System.out.print(">>");
            correcta=lerInt();   
        }
        if(correcta==1) {cor=op1;}
        else {cor=op2;}
        System.out.println("Qual a pontuação da pergunta?");
        System.out.print(">>");
        pont=lerInt();
        Pergunta p = new Pergunta(perg,op1,op2,cor,pont);
        System.out.println("Pergunta criada com êxito");
        return p;
    }
    
    private static Puzzel criaPuzzel(){
        String nome;
        int nPerg;
        
        System.out.println("----------Criar Puzzle----------");
        System.out.println("---------------------------------");
        System.out.println("Diga o nome do Puzzle");
        System.out.print(">>");
        nome=lerTexto();
        Puzzel p = new Puzzel(nome,0);
        System.out.println("Diga quantas perguntas quer criar (1..10):");
        System.out.print(">>");
        nPerg=lerInt();
        while(nPerg<1 || nPerg>10){
            System.out.println("Perguntas limitadas entre 1 e 10, diga de novo");
            System.out.print(">>");
            nPerg=lerInt();   
        }
        for(int i=0; i<nPerg ; i++){
           p.addPergunta(criaPergunta());
        }
        System.out.println("Puzzle criada com êxito");
        return p;
    }
    
    private static Foto criarFoto(){
        String nFoto,fotoUrl,descFoto;
        int vFoto;
        System.out.println("-----------Criar Foto-----------");
        System.out.println("Insira nome Foto");
        System.out.print(">>");
        nFoto = lerTexto();
        System.out.println("Insira valor da Foto");
        System.out.print(">>");
        vFoto = lerInt();
        System.out.println("Insira o URL da Foto");
        System.out.print(">>");
        fotoUrl = lerPalavra();
        System.out.println("Insira a descrição da Foto");
        System.out.print(">>");
        descFoto = lerTexto();
        return new Foto(nFoto,vFoto,fotoUrl,descFoto);
    }
    
    private static int lerSize() {
        int size;
        System.out.println("Diga o tamanho da Cache:");
        System.out.println("1-Muito Pequeno;");
        System.out.println("2-Pequeno;");
        System.out.println("3-Grande;");
        System.out.println("4-Muito Grande");
        System.out.print(">>");
        size=lerInt();
        while(size<1||size>4){
           System.out.print("Tamanho limitado ente 1 e 4 diga de novo:");
           System.out.print(">>");
           size=lerInt(); 
        }
        return size;
    }
    
    private static int lerDif(String tipo){
        int d;
        System.out.println("Diga a dificuldade "+ tipo + " da Cache (1..5) em que 1 mais fácil e 5 mais difícil:");
        System.out.print(">>");
        d=lerInt();
        while(d<1||d>5){
           System.out.print("Dificuldade limitada ente 1 e 5 diga de novo:");
           System.out.print(">>");
           d=lerInt(); 
        }
        return d;
    }
    private static Integer registoCache_Mist(){
        //4misterio
        int dM,dF,size,numItens;
        String nome,desc,veriF;
        Cordenada cord;
        GregorianCalendar nc;
        Puzzel pz;
        Integer id;
        ArrayList<Item> itens= new ArrayList<Item>();//itensNome
        
        System.out.println("---Menu Registo Cache Mistério---");
        
        System.out.println("Qual nome da Cache a adicionar:");
        System.out.print(">>");
        nome = lerTexto();
        
        System.out.println("Qual chave de verificação:");
        System.out.print(">>");
        veriF =lerPalavra();
        
        System.out.println("Qual descrição a dar à Cache:");
        System.out.print(">>");
        desc = lerTexto();
        
        ////////////////
        nc=lerData();
        size=lerSize();
        dM=lerDif("mental");
        dF=lerDif("física");
        cord=criarCoordenada();
        itens=lerItens();
        pz = criaPuzzel();
        id = app.addCacheMist(cord,nc,nome,desc,dM,dF,veriF,itens,size,pz);
        //System.out.println("Registou com sucesso a nova Cache Mistério " + id);
        return id;
    
    }
    
    private static Integer registoCache_Virt(){
        int dM,dF,numItens;
        String nome,desc;
        Cordenada cord;
        GregorianCalendar nc;
        Foto f;
        Integer id;
        ////
        System.out.println("---Menu Registo Cache Virtual---");
        
        System.out.println("Qual nome da Cache a adicionar:");
        System.out.print(">>");
        nome = lerTexto();
        
        System.out.println("Qual descrição a dar à Cache:");
        System.out.print(">>");
        desc = lerTexto();
        ////////////////
        nc=lerData();
        dM=lerDif("mental");
        dF=lerDif("física");
        cord=criarCoordenada();
        f = criarFoto();
        
        id = app.addCacheVirt(cord,nc,nome,desc,dM,dF,f);
        //System.out.println("Registou com sucesso a nova Cache Virtual " + id);
        return id;
    }

    private static Integer registoCache_Micro(){
        int dM,dF,size,numItens;
        String nome,desc,veriF;
        ArrayList<Item> itens= new ArrayList<Item>();//itensNome
        Cordenada cord;
        GregorianCalendar nc;
        Integer id;
        
        System.out.println("---Menu Registo Cache Micro---");
        
        
        System.out.println("Qual nome da Cache a adicionar:");
        System.out.print(">>");
        nome = lerTexto();
        
        System.out.println("Qual chave de verificação:");
        System.out.print(">>");
        veriF =lerPalavra();
        
        System.out.println("Qual descrição a dar à Cache:");
        System.out.print(">>");
        desc = lerTexto();
        ////////////////
        nc=lerData();
        size=lerSize();
        dM=lerDif("mental");
        dF=lerDif("física");
        cord=criarCoordenada();
        itens=lerItens();
        id = app.addCacheMicro(cord,nc,nome,desc,dM,dF,veriF,itens,size);
        //System.out.println("Registou com sucesso a nova Micro Cache " + id);
        return id;
    }
    private static Integer registoCache_Mult(){
        int dM,dF,size,numItens;
        String nome,desc,veriF;
        ArrayList<Item> itens= new ArrayList<Item>();//itensNome
        Cordenada cord;
        GregorianCalendar nc;
        Integer id;
        ArrayList<Integer> caches;
        
        System.out.println("---Menu Registo Cache Multi---");
        System.out.println("Qual nome da Cache a adicionar:");
        System.out.print(">>");
        nome = lerTexto();
        
        System.out.println("Qual chave de verificação:");
        System.out.print(">>");
        veriF =lerPalavra();
        
        System.out.println("Qual descrição a dar à Cache:");
        System.out.print(">>");
        desc = lerTexto();
        ////////////////
        nc=lerData();
        dM=lerDif("mental");
        dF=lerDif("física");
        cord=criarCoordenada();
        itens=lerItens();
        //metodo para criar ou adicionar caches já existentes a multi caches
        caches = adicionarCachesMulti();
        
        id= app.addCacheMulti(cord,nc,nome,desc,dM,dF,veriF,itens,caches);
        //System.out.println("Registou com sucesso a nova Multi Cache " + id + " e as " + caches.size() + " sub-Caches");
        return id;
    }

    private static ArrayList<Integer> adicionarCachesMulti(){
        int nCaches,op;
        ArrayList<Integer> caches = new ArrayList<Integer>();
        
        System.out.println("Quantas Caches deseja criar entre [1..10]");
        System.out.print(">>");
        nCaches=lerInt();
        while(nCaches<1 || nCaches>10){
            System.out.println("Numero limitado entre entre [1..10], diga de novo:");
            System.out.print(">>");
            nCaches=lerInt();
        }
        for(int i=0; i<nCaches;i++){
            System.out.println("-----------Menu Registo das Sub-Caches----------");
            System.out.println("1-Adicionar Nova Cache;");
            System.out.println("2-Adicionar Cache Existente;");
            op=lerInt();
            while(op<1||op>2){
                System.out.println("Numero limitado a 1 e 2, diga de novo:");
                System.out.print(">>");
                op=lerInt();
            }
            switch (op){
                case 1: caches.add(addNovaCache_Multi()); break;
                case 2: caches.add(addExisCache_Multi()); break;
            }

        }
        return caches;
    }

    private static Integer addNovaCache_Multi(){
        Integer id=-1;
        int tip;
        System.out.println("Qual o tipo da Cache?");
        System.out.println("1-Cache Mistério;");
        System.out.println("2-Cache Virtual;");
        System.out.println("3-Micro Cache;");
        System.out.print(">>");
        tip = lerInt();
        while(tip<1||tip>3){
            System.out.println("Numero limitado entre 1 e 3, diga de novo:");
            System.out.print(">>");
            tip=lerInt();
        }
        switch (tip){
            case 1: id=registoCache_Mist(); break;
            case 2: id=registoCache_Virt(); break;
            case 3: id=registoCache_Micro(); break;
        }
        System.out.println("Registou com sucesso a "+ app.dizTipoCache(id)+" com ID " + id );
        return id;
        
    }
    
    private static Integer addExisCache_Multi(){
        Integer id;
        System.out.println("Diga o ID da Cache:");
        System.out.print(">>");
        id=lerInt();
        while(!app.existeCache(id)){
            System.out.println("ID invalido, esta Cache não existe, diga de novo:");
            System.out.print(">>");
            id=lerInt();
        }
        return id;
    }

    private static void registoCache(){
        //private Cache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p){
        int tip;
        int id=0;
        int ninte;
        int npist;
        int i=0;
        System.out.println("---------------GeocachingPOO--------------");
        System.out.println("-----------Menu Registo de Cache----------");
        System.out.println("Qual o tipo da Cache?");
        System.out.println("1-Cache Mistério;");
        System.out.println("2-Cache Virtual;");
        System.out.println("3-Micro Cache;");
        System.out.println("4-Multi Cache;");
        System.out.print(">>");
        tip = lerInt();
        while(tip<1||tip>4){
            System.out.println("Numero limitado entre 1 e 4, diga de novo:");
            System.out.print(">>");
            tip=lerInt();
        }
        switch (tip){
            case 1: id=registoCache_Mist(); break;
            case 2: id=registoCache_Virt(); break;
            case 3: id=registoCache_Micro(); break;
            case 4: id=registoCache_Mult(); break;
        }
        System.out.println("Quantos ponto de interesse deseja adicionar a Cache " + id +"? (0..10)");
        System.out.print(">>");
        ninte=lerInt();
        if(ninte<0) ninte=0;
        if(ninte>10) ninte=10;
        Cordenada cord;
        try{
            while(i<ninte){
                System.out.println("Ciraçao do ponto " + i + " de " + ninte );
                cord=criarCoordenada();
                app.adicionarPontIntCache(id,cord);
                i++;
            }
            System.out.println("Quantas pistas deseja adicionar a Cache " + id +"? (0..10)");
            System.out.print(">>");
            npist=lerInt();
            if(npist<0) npist=0;
            if(npist>10) npist=10;
            i=0;
            String nome;
            while(i<npist){
                System.out.println("Criação da pista " + (i+1) + " de " + npist );
                System.out.println("Diga a pista que deseja adicionar:");
                System.out.print(">>");
                nome=lerTexto();
                app.adicionarPistaCache(id,nome);
                i++; 
            }
            System.out.println("Registou com sucesso a "+ app.dizTipoCache(id)+" com ID " + id );
        }catch(CacheNaoExisteException e){
            System.out.println("Não existe a Cache " + e.getMessage());
        }
    }
    
    private static void userDetail(){
        String mail;
        System.out.println("Qual o utilizador a consultar detalhes:");
        System.out.print(">>");
        mail = lerEmail();
        try{
            if( app.adminMode() ||app.isMyFrind(mail)){
                System.out.println("----------------DETALHES----------------");
                System.out.println(app.userDeatils(mail));
                System.out.println("----------------------------------------");
        
            }
            else  System.out.println("ERRO o utilizador " + mail + " não é seguido por si, não pode consultar os seus detalhes;");
        }catch(UtilizadorNaoExisteException e ){
            System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
        }
    }
    //Admin
    
         private static void removeAdmin(){
        String mail,pass;
        int i=0;
        System.out.println("Qual o ADMIN a remover:");
        System.out.print(">>");
        mail = lerPalavra();
        try{
            if(mail.equals(onLogin)){
               System.out.println("ERRO não se pode remover a si mesmo");
               return;
            }
            if(!comparPASS(mail)) return;
            app.removeAdmin(mail);
            System.out.println("o Admin " + mail + " removido com sucesso da APP");

        }catch(AdminNaoExisteException e){
            System.out.println("Não existe o Admin " + e.getMessage());
            
        }
        
    }
    
    private static void regAdmin(){
        String mail,pass,nome;
        
        System.out.println("\n----------GeocachingPOO----------");
        System.out.println("--------Menu Registo ADMIN-------\n");
        mail= registaNovoMail();
        pass=registaNovaPASS();
        System.out.println("Diga o nome:");
        System.out.print(">>");
        nome = lerTexto();
        try{
            app.registaAdmin(mail,pass,nome);
            System.out.println("Admin "+ mail + " registado com Sucesso.");
        }catch(JaExisteMailException e){
            System.out.println("Já esta registado o Email " + e.getMessage());
        }
    }
    
    //Por mais bonito
    private static void imprimirUsers(){
        try{
            ArrayList<String> novo= app.getUsers();
            System.out.println("Existem na APP " + novo.size() + " utilizadores.");
            if(novo.size()>0) print_formatado(novo,'?');
        }catch(EventoNaoExisteException e){
            return;
        }
        
    }
    private static void removeUser(){
        String mail;
        System.out.println("Qual o utilizador a remover:");
        System.out.print(">>");
        mail = lerEmail();
        try{
            app.removeUser(mail);
            System.out.println("o utilizador " + mail + " removido com sucesso da APP");
        }catch(UtilizadorNaoExisteException e){
            System.out.println("Utilizador "+ e.getMessage()+ " Não se Encontra na APP");
            
        }
        
    }
    
    private static void alteraMPassAdmin(){
        String passN;

        if(!comparPASS(onLogin)) return;
        passN =registaNovaPASS();
        
        try{
            app.setadminPASS(onLogin,passN);
            System.out.println("A sua password foi alterada com sucesso");
        }catch(AdminNaoExisteException e){
            System.out.println("O ADMIN " + e.getMessage() + " Não Existe na APP");
        }
    }   
    
            
   
    
    private static void alteraPassUser(){
        String passN;
        String mail;
        
        System.out.println("Qual o utilizador a altera a password:");
        System.out.print(">>");
        mail = lerEmail();
        try{
            if(!app.jaExisteUser(mail)){
                System.out.println("o utilizador " + mail + " não existe na APP");
            }else{
                passN =registaNovaPASS();
                app.setUserPASS(mail,passN);
                System.out.println("A password do utilizador " + mail + " alterada com sucesso para: " + passN);
                
            }
        }catch(UtilizadorNaoExisteException e){
            System.out.println("o utilizador " + mail + " não existe na APP");
            //esta aqui mas doi resolvido com oi+f acima pois sem o if pedia o utilizador a a pass
            //mas so depois é que dizia que nao existia o user
        }
    }
    
    private static void removeEvento(){
        Integer id;
        System.out.println("Qual o evento a remover?");
        System.out.print(">>");
        id = lerInt();
        try{
            app.removeEvento(id);
            System.out.println("Evento " + id +" removido com sucesso.");
        }catch(EventoNaoExisteException e){
          System.out.println("O evento com o ID " + e.getMessage() + " não se encontra na APP");  
        }
    }
    
    //por mais bonito
    private static void imprimirEventos(){

        try{
            ArrayList<Integer> novo= app.getEventos();
            System.out.println("Existem na APP " + novo.size() + " Eventos:");
            if(novo.size()>0) print_formatado(novo,'?');
        }catch(EventoNaoExisteException e){
             System.out.println("O evento com o ID " + e.getMessage() + " não se encontra na APP");
        }
    }
    
    private static void removeCache(){
        Integer id;
        System.out.println("Qual a Cache a remover?");
        System.out.print(">>");
        id = lerInt();
        try{
            app.forceRemoveCache(id);
            System.out.println("Cache " + id +" removida com sucesso.");
            
        }catch(CacheNaoExisteException e){
          System.out.println("A Cache com o ID " + id + " não se encontra na APP");  
        }catch(PermissionsException e){
            System.out.println("Sem Permissões");
        }
    }
    
    //por mais bonito
    private static void imprimirCaches(){
        ArrayList<Integer> novo= app.getCaches();
        System.out.println("Existem na APP " + novo.size() + " Caches ativas:");
        try{
            if(novo.size()>0) print_formatado(novo,'?');
        }catch(EventoNaoExisteException e){
            return;
        }
    }
    
    private static void reportClean(){
        Integer id;
        System.out.println("Qual a Cache a deseja eliminar?");
        System.out.print(">>");
        id = lerInt();
        try{
            app.limpacacheRp(id);
            System.out.println("Cache " + id +" eliminada com sucesso.");
        }catch(CacheNaoExisteException e){
          System.out.println("A Cache com o ID " + e.getMessage() + " não se encontra no Report Abuse");   
        }
    }
    
    private static void resetReport(){
        Integer id;
        System.out.println("Qual a Cache a fazer reset?");
        System.out.print(">>");
        id = lerInt();
        try{
            app.resetReport(id);
            System.out.println("Reset a Cache " + id +" com sucesso.");
        }catch(CacheNaoExisteException e){
          System.out.println("A Cache com o ID " + e.getMessage() + " não se encontra no Report Abuse");   
        }
        
    }
    
    private static void reportCleanALL(){
        if(app.semReport()){
            System.out.println("Não existem Caches no Report Abuse.");
        }else{
            if(app.existeALimpar()){
                System.out.println("A limpar \"Report Abuse\"...");
                int v = app.limpaReportALL();
                System.out.println("Foram limpas do \"Report Abuse\" " + v + " Caches");
            }
            else{
                System.out.println("Não existem Caches a limpar do Report abuse.");
            }
        }
    }
    
    //Utilizador
    //por mais bonito
    private static void listaMyAmigos(){
        try{
            ArrayList<String> friends = app.getAmigos(onLogin);
            System.out.println("Tem " + friends.size() + " amigos:");
            if(friends.size()>0)print_formatado(friends,'?');
            System.out.println("-------FIM-------");
        }
        catch(UtilizadorNaoExisteException e){
            System.out.println("O utilizador " + e.getMessage() + " Desconhecido.");
        }
        catch(EventoNaoExisteException e){
            return ;
        }
    }
    
    private static void souAmigo(){
        String mail;
        System.out.println("Qual é o Utilizador:");
        System.out.print(">>");
        mail = lerPalavra();
        if(mail.equals(onLogin)){
            System.out.println("Claro que é seu amigo.");
        }
        else{
            if(app.jaExisteUser(mail)){
                if(app.isMyFrind(mail)){
                    System.out.println("É amigo de " + mail);
                }
                else System.out.println("Não é amigo de " + mail);
            }else{
                System.out.println("O utilizador " + mail + " não existe na APP");
            }
        }
    }
    
    private static void comecaSeg(){
        String mail;
        System.out.println("Qual o utilizador a adicionar como Amigo:");
        System.out.print(">>");
        mail = lerPalavra();
        if(mail.equals(onLogin)){
            System.out.println("Não se pode adicionar como Amigo");
        }else{
            try{
                if(!app.isMyFrind(mail)){
                    app.addFriend(mail);
                    System.out.println("Adicionou com sucesso " + mail + " à sua lista de amigos");
                }
                else System.out.println("O utilizador " + mail + " já é seu amigo não pode adicionar novamente");
            }
            catch(UtilizadorNaoExisteException e){
                System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
            }
            
        }
    }
    
    
    private static void deixaSeg(){
        String mail;
        System.out.println("Qual o Amigo a remover:");
        System.out.print(">>");
        mail = lerEmail();
        if(mail.equals(onLogin)){
            System.out.println("Não se pode remover a si mesmo de Amigo");
        }else{
            try{
                if(app.isMyFrind(mail)){
                    app.rmoveFriend(mail);
                    System.out.println("Removeu com sucesso " + mail + " da sua lista de amigos");
                }
                else System.out.println("O utilizador " + mail + " não amigo não remover");
            }catch(UtilizadorNaoExisteException e){
                System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
            }
            catch(NaoAmigoException e){
                System.out.println(e.getMessage());
            }
        }
    }
    
    private static void alteraMPassUser(){
        String passN;
        if(comparPASS(onLogin)){
            passN =registaNovaPASS();
            try{
                app.setUserPASS(onLogin,passN);
                System.out.println("A Sua nova password alterada com sucesso");
            }catch(UtilizadorNaoExisteException e ){
                System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
            }
        }
    }
    
    
    
    private static void autoRemove(){
        String op;
        System.out.println("Tem a certeza que deseja remover a conta?(0-Não, Outro-Sim)");
        System.out.print(">>");
        op = lerPalavra();
        if(op.equals("0") || op.equals("Não")){
            System.out.println("Operação cancelada");
        }
        else{
            try{
                if(comparPASS(onLogin)){
                    app.removeUser(onLogin);
                    System.out.println("Eliminou com sucesso a sua conta");
                }
                else System.out.println("Falhou a password, operação cancelada");
        
            }catch(UtilizadorNaoExisteException e ){
                System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
            }
        }
    }
    private static void last10encByME(){
        try{
            System.out.println(app.last10encCacheBy(onLogin));     
        }catch(UtilizadorNaoExisteException e ){
            System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
        }
    }
    private static void allOrdencCacheByMe(){
        try{
            String novo = app.allOrdencCacheBy(onLogin);
            System.out.println(novo);
        }catch(UtilizadorNaoExisteException e ){
            System.out.println("O utilizador " + e.getMessage() + " não existe na APP");
        }
    }
    
    private static boolean verfCache(Integer id){
        //aatençao ao vc
        String vc;
        String vcl;
        int i=0;
        
        try{
            vc= app.getCacheCode(id);
            System.out.println("Trata-se de uma Cache física, é necessário código de verificação:");
            System.out.println("Introduza esse código:");
            System.out.print(">>");
            vcl=lerPalavra();
            while(i<2 && !vcl.equals(vc)){
                System.out.println("Código errado restam "+ (2-i) + " tentativas.");
                System.out.println("Introduza de novo:");
                System.out.print(">>");
                vcl=lerPalavra();
                i++;
            }
            if(i==2){
                System.out.println("Código errado " + (i+1) + " vezes");
                System.out.println("Registo Abortado...");
                return false;
            }
            System.out.println("Cache validada");
        }catch(CacheNaoExisteException e){
            System.out.println("Cache " + e.getMessage() + " desconhecida");
            return false;
            
        }catch(CacheTipoErradoException e){
            System.out.println(e.getMessage());
            return false;
            
        }
        
            return true;
        }
        
    private static void makeReport(){
        Integer id;
        String vc;
        String vcl;
        System.out.println("Qual o ID da Cache que deseja fazer Report:");
        System.out.print(">>");
        id=lerInt();
        try{
            if(app.existeCache(id) && !app.repoedByMe(id)){
                try{
                    vc= app.getCacheCode(id);
                    if(!verfCache(id))return; 
                }catch(CacheTipoErradoException e){}
                    app.registReportAbuse(id);
                    System.out.println("Report abuse da Cache " + id + " com sucesso.");
            }
            else{
                if(!app.existeCache(id))System.out.println("Erro a Cache " + id +" não se encontra na APP.");
                else System.out.println("Erro já fez Report Abuse da Cache " + id);
            }
        }catch(CacheNaoExisteException e){
            System.out.println("NAO ERA SUPOSTO ACONTECER");
        }
   }
    
    
   private static void ecnconCache(){
        //1-Virtual
        //2multi
        //3micro
        //4misterio
        Integer id;
        String vc;
        int tipo;;
        int pontosBonus=0;
        int bonus=1;
        int trade;
        GregorianCalendar dt;
        System.out.println("Qual o ID da Cache que deseja registar:");
        System.out.print(">>");
        id=lerInt();
        try{
        if(app.existeCache(id) && !app.userJaEnconCache(id) && !app.hidenByMe(id)){
            tipo=app.tipoCache(id);
            dt =lerData();
            System.out.println("Esta a tentar registar uma Cache do tipo: " + app.dizTipoCache(id));
            if(tipo>1){ //tem codigo de verificaçao Fisica
                vc= app.getCacheCode(id);
                if(!verfCache(id))return;
                //ver melhor isto por caisa dos pontos e etc;
                if(tipo==2){//vc.equals("MultiCache")
                    //tipo=2;
                    //falta fazer update a cache misterios ou ate se fazdentro na de baixo
                    System.out.println("A atualizar porgresso na MultiCache " + id);
                    if(!app.multiTodasVisitadas(id)){
                        System.out.println("Ainda não visitou todas as Caches desta Multi Cache, tente novamente quando visitar");
                        return;
                    }
                }else{
                    if(tipo==3){//MicroCache
                        
                    }
                    else{
                        //tipo=4;
                        pontosBonus=jogarMisterio(id);
                    }
                }
                //pontos+=app.getPontFisicCache(id);
                //app.registaEncontroCache(id,pontos,tipo,lerData());
                System.out.println("Deseja trocar algum item ou deixar/levantar CoinItem?(0-Nao outro-Sim)");
                System.out.print(">>");
                trade=lerInt();
                if(trade!=0){
                    trocaMenu(id);
                    
                }
            }
            else{//cache virtual
                System.out.println("Deseja deixar alguma/consultar alguma Foto?");
                System.out.print(">>");
                trade=lerInt();
                if(trade!=0){
                    consultaFoto(id);
                    
                }
                //adicionar a inserçao de fotos
                //app.registFoudVirt(id,data,bonus);
            }
            app.registaEncontroCache(id,pontosBonus,bonus,tipo,dt);
            System.out.println("Cache " + id + " registada com sucesso.");
        }
        else{
            if(!app.existeCache(id))System.out.println("Erro a Cache que tentou registar não se encontra na APP.");
            else {
                
                if(app.hidenByMe(id)) System.out.println("Erro não pode registar uma Cache escondida por si");
                else System.out.println("Erro já possui a Cache " + id + " registada.");
        
            }
        } 
    }catch(Exception e){
        System.out.println("ERRO ao registar a Cache");
        //Naoa é suposto acontecer nao tatei a exepçao porque o fi ja trata disse e era muito confuso altear
    }
    
    }
    
    private static void consultaFoto(Integer id){
        int op;
        try{
            do{
                System.out.println("--MENU da Cache Virtual " + id + "--");
                System.out.println("1-Mostrar Fotos");
                System.out.println("2-Deixar Fotos");
                System.out.println("Outro para cancelar");
                System.out.println("Qual a opção ");
                System.out.print(">>");
                op=lerInt();
                if(op>0 && op<3){
                    switch (op){
                        case (1) :  System.out.println(app.listFotosof(id)); break;
                        case (2) :  leaveFoto(id); break;
                
                    }
                }
                else return;
                System.out.println("Deseja continuar no menu da Cache Virtual? (1-Sim , outro-Não)");
                System.out.print(">>");
                op=lerInt();
            }while(op==1);
        }catch (CacheNaoExisteException e){
            System.out.println("A Cache " + e.getMessage() + "não existe");
        }
        catch (CacheTipoErradoException e){
            System.out.println(e.getMessage());
        }
        
    }
    
    private static void leaveFoto(Integer id) throws CacheNaoExisteException,CacheTipoErradoException{
        Foto ft;
        ft = criarFoto();
        app.addFoto(id,ft.clone());
        System.out.println("Deixou com sucesso uma nova foto na Cache "  +id);
    }
    
    
    private static void trocaMenu(Integer id){
        int op;
        try{
            do{
                System.out.println("--MENU de Caches de " + id + "--");
                System.out.println("1-Listar Itens;");
                System.out.println("2-Trocar Itens;");
                System.out.println("3-Retirar TravelBug;");
                System.out.println("4-Deixar TravelBug;");
                System.out.println("Outro para cancelar.");
                System.out.println("Qual a opção ");
                System.out.print(">>");
                op=lerInt();
                if(op>0 && op<5){
                    switch (op){
                        case (1) :  System.out.println(app.listItensof(id)); break;
                        case (2) :  trade(id); break;
                        case (3) :  retirarCoin(id); break;
                        case (4) :  leaveCoin(id);break;
                
                    }
                }
                else return;
                System.out.println("Deseja continuar no menu de trocas? (1-Sim , outro-Não)");
                System.out.print(">>");
                op=lerInt();
            }while(op==1);
        }catch (CacheNaoExisteException e){
            System.out.println("A Cache " + e.getMessage() + "não existe");
        }
        catch (CacheTipoErradoException e){
            System.out.println(e.getMessage());
        }
        catch(NaoExisteItemException e){
            System.out.println("NAO DEVIA ACONTECER");
        }
    }
    
    private static void retirarCoin(Integer id) throws CacheNaoExisteException,CacheTipoErradoException{
        if(app.meHasCoin()){
            System.out.println("Já possui um CoinItem em posse, não pode ter mais");
        }
        else{
            CoinItem ret;
            try{
                ret= app.retiraCoin(id);
                System.out.println("Levantou com sucesso o CoinItem");
                System.out.println("Detalhes da CoinItem Retirada:");
                System.out.println("------------------------------");
                System.out.println("Nome: " + ret.getNome() + " Valor: "+  ret.getValor());
                System.out.println("Data Criação: "+ ret.getDataFormatada());
                ArrayList<Integer> visit = ret.getHistorico();
                System.out.println("Histórico de Caches visitadas: (" + visit.size()+"):");
                if(visit.size()>0)print_formatado(visit,'?');
                
            }
            catch(NaoExisteCoinException e ){
                System.out.println("A Cache " + id + " não tem nenhum CoinItem");
            }catch(NaoExisteItemException e){
                System.out.println("A Cache " + id + " não tem nenhum Item");
            }catch(EventoNaoExisteException e){
                return;
            }
        }
    }
    
    private static void leaveCoin(Integer id)throws CacheNaoExisteException,CacheTipoErradoException{
        try{
            app.addLeavCoinItem(id);
            System.out.println("Deixou o sei CoinItem com sucesso");
        }
        catch(NaoExisteCoinException e){
            System.out.println("Não tem nenhum CoinItem para deixar");
        }
    }
    
    private static void trade(Integer id)throws CacheNaoExisteException,CacheTipoErradoException,NaoExisteItemException{
        String nome; 
        Item old,novo;
        int num=0;
        if(app.itensDe(id)>0){
            novo= criarItemnormal();
            do{
                System.out.println("Diga no nome do item a trocar (Impossível trocar CoinItens):");
                System.out.print(">>");
                nome  =lerTexto();
                old = app.tradeItems(id,novo,nome);
                if(old==null){
                    if(num==0){
                        System.out.println("O item que referiu não se encontra disponível para troca");
                        System.out.println("Itens Disponíveis:");
                        System.out.println(app.listItensof(id));
                    }
                }
                num++;
            }while(num<=1);
            if(old==null){
                System.out.println("Não foi possível fazer a troca.");
            }
            else{
                System.out.println("Troca com sucesso");
                System.out.println("--Detalhes novo Item--");
                System.out.println("Nome: " + old.getNome() + " Valor: " + old.getValor() +"\n");
            }
        }
    }
    ///_____________________________________________________________
    private static void registo(){
        String mail,pass,nome,pais,cidade,rua,cp;
        char genero;
        GregorianCalendar dtn;
        //ver maioridade
        
        System.out.println("\n----------GeocachingPOO----------");
        System.out.println("-----------Menu Registo----------\n");
        
        mail= registaNovoMail();
        pass=registaNovaPASS();

        System.out.println("Diga o nome:");
        System.out.print(">>");
        nome = lerTexto();
        System.out.println("Diga o Género (M,F,O):");
        System.out.print(">>");
        genero=lerPalavra().charAt(0);
        while(!(genero == 'M' || genero == 'F' || genero == 'O')){
           System.out.println("Género Invalido Diga de novo:");
           System.out.print(">>");
           genero=lerPalavra().charAt(0); 
        }
        dtn= lerData();
        
        System.out.println("Diga o seu Pais:");
        System.out.print(">>");
        pais = lerTexto();
        
        System.out.println("Diga a sua Cidade:");
        System.out.print(">>");
        cidade = lerTexto();
        
        System.out.println("Diga a sua Rua:");
        System.out.print(">>");
        rua = lerTexto();
        
        System.out.println("Diga o seu Código Postal:");
        System.out.print(">>");
        cp = lerTexto();
        try{
            app.registaUser(mail,pass,nome,genero,dtn,pais,cidade,rua,cp);
            System.out.println( mail + " registado com Sucesso.");
        }
        catch(JaExisteMailException e ){
            System.out.println("ERRO Email "+ e.getMessage()+" ja registado");
            System.out.println("Volte a tentar");
            registo();
        }
    }
    
    private static void menuAdmin(){
        int op=1;
        while(op>0 && op<27){
            System.out.println("\n----------GeocachingPOO----------");
            System.out.println("------------Menu ADMIN-----------");
            System.out.println("--ADMINS--");
            System.out.println("1-Criar novo Admin;");
            System.out.println("2-Remover Admin;");

            System.out.println("--USERS--");
            System.out.println("3-Remover Utilizador;");
            System.out.println("4-Consultar Lista de Utilizadores;");
            System.out.println("5-Consultar Utilizador em Detalhe;");
            System.out.println("6-Alterar password de um Utilizador;");

            System.out.println("--CACHES--");
            System.out.println("7-Consultar Lista de Caches;");
            System.out.println("8-Consultar Cache em detalhe;");
            System.out.println("9-Esconder uma nova Cache;");
            System.out.println("10-Remover uma Cache;");
            System.out.println("11-Atualizar um Cache;");

            System.out.println("--REPORT ABUSE--");
            System.out.println("12-Executar Limpeza ao ReporAbuse (elimina todas as Caches da APP que foram \"RepotAbused\" mais que o valor de referencia);");
            System.out.println("13-Executar ReporAbuse a uma Cache especifica;");
            System.out.println("14-Retirar Cache do ReporAbuse(Reset);");

            System.out.println("--EVENTOS--");
            System.out.println("15-Consultar Lista de Eventos;");
            System.out.println("16-Consultar Evento em detalhe;");
            System.out.println("17-Criar novo Evento;");
            System.out.println("18-Eliminar Evento;");
            System.out.println("19-Simular Evento;");

            System.out.println("--GESTÃO APP--");
            System.out.println("20-Ultimo ID de Cache criada (Total de Caches criadas);");
            System.out.println("21-Ultimo ID de Evento criado (Total de Eventos criados);");
            System.out.println("22-Numero de utilizadores na APP;");
            System.out.println("23-Numero total de utilizadores que efetuaram registo (conta com os removidos);");
            System.out.println("24-Numero de Caches ativas na APP;");
            System.out.println("25-Numero de Eventos ativos na APP;");
            System.out.println("26-Fazer Backup;");
            System.out.println("27-Restaurar Backup;");

            System.out.println("--DEFINIÇÕES CONTA--");
            System.out.println("28-Alterar Password;");
            System.out.println("Outro - Sair");
            System.out.print(">>");
            op= lerInt();
            switch (op){
                case 1: regAdmin(); break;
                case 2: removeAdmin(); break;
                case 3: removeUser(); break;
                case 4: imprimirUsers(); break;
                case 5: userDetail(); break;
                case 6: alteraPassUser(); break;
                case 7: imprimirCaches(); break;
                case 8: cacheDetalhe(); break;
                case 9: registoCache(); break;
                case 10: removeCache() ; break;
                case 11: atualizarInfoCaches(); break;
                case 12: reportCleanALL(); break;
                case 13: reportClean(); break;
                case 14: resetReport() ; break;
                case 15: imprimirEventos(); break;
                case 16: detalheEvento(); break;
                case 17: criarEvento(); break;
                case 18: removeEvento(); break;
                case 19: simulaEvent(); break;
                case 20: System.out.println("A ultima Cache criada foi: " + app.lastCacheID()+"."); break;
                case 21: System.out.println("A ultimo Evento criado foi: " + app.lastEventID() +"."); break;
                case 22: System.out.println("Existem " + app.totalUsersActive() + " utilizadores ativos na APP."); break;
                case 23: System.out.println("No total já foram registados " + app.totalRegistedUsers()+ " utilizadores."); break;
                case 24: System.out.println("Existem " + app.totalCacheActive() + " Caches ativas na APP."); break;
                case 25: System.out.println("Existem " + app.totalEventActive() + " Eventos ativos na APP."); break;
                case 26: save(); break;
                case 27: load(); break;
                case 28: alteraMPassAdmin(); break;
            }
            //as outras exige relogin por causa das passes
            if(op>0 && op<27){
               System.out.println("\nInsira algo para continuar"); 
               lixo=lerTexto();
            }
        }
        logout();
    }
    
    private static void save(){
        GregorianCalendar novo = new GregorianCalendar();
        StringBuilder s = new StringBuilder();
        StringBuilder aux = new StringBuilder();
        int min = novo.get(Calendar.MINUTE);
        if(min<10) aux.append("0"+ min);
        else aux.append(min);
         
        File theDir = new File("Backups");

        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            } catch (SecurityException se) {

            }
        }
        s.append("Backups"+File.separator+"geocachingPOO_"+novo.get(Calendar.YEAR) + "_"+ (novo.get(Calendar.MONTH)+1) + "_"+ novo.get(Calendar.DAY_OF_MONTH) +"_"+ novo.get(Calendar.HOUR_OF_DAY) +"_"+ aux.toString() +".obj");
        try{
            app.save(s.toString());
            System.out.println("Estado guardado em : \""+ s.toString()+"\"");
        }catch(ErroSaveException e){
            System.out.println(e.getMessage());
        }
            
    }
    private static void load(){
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        FileNameExtensionFilter objfilter = new FileNameExtensionFilter("obj files (*.obj)", "obj");
        chooser.setFileFilter(objfilter);
        int option = chooser.showOpenDialog(chooser);
        if (option == JFileChooser.APPROVE_OPTION) {
            File sf = chooser.getSelectedFile();
            try{
                GeocachingPOO novo = app.load(sf.getAbsolutePath());
                app=novo;
                System.out.println("Carregou com sucesso o ficheiro: \"" + sf.getAbsolutePath()+"\"");
            }
            catch(ErroLoadException e){
                System.out.println("ERRO ao carregar \"" + sf.getAbsolutePath()+"\"");
                System.out.println("Backup não restaurado, mantem-se o estado antes do Restauro");
            }
        }
    }
    
    
    private static void menuUser(){
        int op=1;
        while(op>0 && op<23){
            System.out.println("\n----------GeocachingPOO----------");
            System.out.println("------------Menu USER------------");
            
            System.out.println("--CACHES--");
            System.out.println("1-Registar encontro de Cache;");
            System.out.println("2-Fazer ReportAbuse de uma Cache;");
            System.out.println("3-Ver todas Caches encontradas;");
            System.out.println("4-Ver ultimas 10 Caches encontradas em formato \"TimeLine\";");
            System.out.println("5-Consultar Cache em detalhe;");
            System.out.println("6-Esconder uma nova Cache;");
            System.out.println("7-Remover uma Cache da APP;");
            System.out.println("8-Atualizar um Cache;");
            System.out.println("9-Listar Caches a uma Distancia;");

            System.out.println("--AMIGOS--");
            System.out.println("10-Adicionar novo Amigo;");
            System.out.println("11-Remover Amigo;");
            System.out.println("12-Verificar se é Amigo;");
            System.out.println("13-Listar todos de Amigos;");
            System.out.println("14-Consultar informação de um Amigo;");
            
            System.out.println("--EVENTOS--");
            System.out.println("15-Registar participação num Evento;");
            System.out.println("16-Ver participantes de um Evento;");
            System.out.println("17-Listar todos Eventos;");
            
            System.out.println("--ESTATISTICAS--");
            System.out.println("18-Verificar estatistica de um Ano;");
            System.out.println("19-Verificar estatistica de um Ano e Mes;");

            System.out.println("--DEFINIÇÕES CONTA--");
            System.out.println("20-Imprimie informação da Conta;"); 
            System.out.println("21-Altear Morada;");
            System.out.println("22-Alterar password;");
            System.out.println("23-Eliminar Conta;");

            

            System.out.println("Outro - Sair");
            System.out.print(">>");
            op= lerInt();
            switch (op){
                case 1: ecnconCache(); break;
                case 2: makeReport();break;
                case 3: allOrdencCacheByMe(); break;
                case 4: last10encByME(); break;
                case 5: cacheDetalhe(); break;
                case 6: registoCache(); break;
                case 7: removeCache(); break;
                case 8: atualizarInfoCaches(); break;
                case 9: listaMaisPerto(); break;
                case 10: comecaSeg(); break;
                case 11: deixaSeg(); break;
                case 12: souAmigo(); break;
                case 13: listaMyAmigos(); break;
                case 14: userDetail(); break;
                case 15: eventRegist(); break;
                case 16: lsitPartcipORD(); break;
                case 17: imprimirEventos(); break;
                case 18: myStatsAno(); break;
                case 19: myStatsAnoMes(); break;
                case 20: System.out.println("DETALHES--------------------------------");
                         try{
                             System.out.println(app.userDeatils(onLogin));
                        }catch(UtilizadorNaoExisteException e){
                            System.out.println("O utilizador " + e.getMessage() + " não existe"); 
                        }
                         System.out.println("----------------------------------------"); break;
                case 21: userChangeMorada(); break;
                case 22: alteraMPassUser(); break;
                case 23: autoRemove(); break;
            }
            if(op>0 && op<23){
               System.out.println("\nInsira algo para continuar"); 
               lixo=lerTexto();
            }
        }
        logout();
    }
    
    
    private static void login(){
        String mail;
        System.out.println("\n----------GeocachingPOO----------");
        System.out.println("--------------Login--------------");
        System.out.println("Diga o Email:");
        System.out.print(">>");
        mail = lerEmail();
        if(app.jaExisteIDuseradmin(mail)==false){
            System.out.println("ERRO não Exite registo do utilizador: " + mail);
            return;
        }
        
        if(!comparPASS(mail)) return;
        System.out.println("\nLogin Com sucesso.");
        onLogin=mail;
        if(app.jaExisteAdmin(mail)){
            System.out.println("Encontrasse em modo ADMIN.");
            loginAdmin(mail);
            menuAdmin();
        }else{
            System.out.println("Encontrasse em modo Utilizador.");
            loginUser(mail);
            menuUser();
        }
    }
    
    public static void main (String [] args){
        int op=1;
        while(op==1 || op== 2){
            if(v!=0){System.out.println("\n");}
            else{v++;}
            if(app.hasAdmin()){
                System.out.println("----------GeocachingPOO----------");
                System.out.println("1 - Login");
                System.out.println("2 - Registar Utilizador");
                System.out.println("Outro - Sair");
                System.out.println("---------------------------------");
                System.out.print(">>");
                op= lerInt();
                if(op==1)login();
                if(op==2)registo();
            }else{
                System.out.println("----------GeocachingPOO----------");
                System.out.println("1 - Registar Admin");
                System.out.println("Outro - Sair");
                System.out.println("---------------------------------");
                System.out.print(">>");
                op= lerInt();
                if(op==1)regAdmin();
                else op=3;
            }
            
        }
        System.out.println("\n\nADEUS!!");
        
    }
    
    private static void logout(){
        app.setModo(0);
        premi=0;
        app.setLogin("");
        onLogin ="";
    }
    private static void loginAdmin(String mail){
        app.setModo(1);
        premi=1;
        app.setLogin(mail);
        onLogin=mail;
    }
    private static void loginUser(String mail){
        app.setModo(0);
        premi=0;
        app.setLogin(mail);
        onLogin=mail;
    }

    private static void print_formatado(ArrayList<? extends Object> lista,char letra) throws EventoNaoExisteException{
        int total=lista.size();
        int topage,linhas=0,j,pos,paginas,avanco;
        int nlinhas,ncolunas;
        System.out.println("Nº Linhas:");
        nlinhas=lerInt();
        if(nlinhas<1){
            System.out.println("Numero minimo de Linhas é 1, vai ser Default(10)");
            nlinhas = 10;
        }
        if(nlinhas>100){
            System.out.println("Numero maximo de Colunas é 1100, vai ser Default");
            nlinhas = 100;
        }
        

        ncolunas=1;
        
        
        avanco=(nlinhas*ncolunas);
        paginas = (total / avanco);
        if((total % (nlinhas*ncolunas))==0) paginas--;
        for (topage=0,pos=topage*avanco; topage!=-1; ){
            if(pos<total){
                if(linhas<9){
                    System.out.printf("%d->  ",linhas+1);
                }else if(linhas<99){
                    System.out.printf("%d-> ",linhas+1);
                }else{
                    System.out.printf("%d->",linhas+1);
                }
                for(j=0;j<ncolunas && pos<total;j++){
                    System.out.printf("%s\t\t",lista.get(pos).toString());
                    pos++;
                }
                System.out.printf("\n");
                linhas++;
            }else{
                linhas=nlinhas;
            }
            if(pos > 0 && linhas % nlinhas == 0 ){
                if(paginas>0){
                    if(letra=='?') System.out.printf("Encontrasse na pagina %d.\nPara que pagina deseja continuar a imprimir? (<1 ou >%d SAIR)\n",topage+1,paginas+1);
                    else System.out.printf("Encontrasse na pagina %d da letra %c.\nPara que pagina deseja continuar a imprimir? (<1 ou >%d parar imprimir letra %c)\n",topage+1,letra,paginas+1,letra);
                    topage=lerInt()-1;
                    pos=topage*avanco;
                    linhas=topage*nlinhas;
                }else topage=1;
            }
            if(topage<0 || topage>paginas){
                topage =-1;
            }
        }
    }

}
