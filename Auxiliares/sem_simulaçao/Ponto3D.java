/**
 * Escreva a descrição da classe Ponto3D aqui.
 * 
 * @author () 
 * @version (número de versão ou data)
 */
import static java.lang.Math.abs;
import java.io.Serializable;
class Ponto3D extends Ponto2D implements Serializable
{
    // Variáveis de instância 
    private double z;
    
    
     public Ponto3D (double x, double y, double z) {
      super(x, y); this.z = z ;
    }
    
    public Ponto3D () {super () ; z = 0.0; }
    
    public Ponto3D(Ponto3D p){
        super(p);
        this.z = p.getZ();
        
    }
     //Geter 
    public double getZ () {return z; }
    //seters
    public void setZ(double z){this.z=z;}
   
    public String toString () {
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append (","); 
        s.append (this.getZ());
        return s.toString();
    }
    
    public Ponto3D clone(){return new Ponto3D(this);}
    
    public int hashCode(){return this.toString().hashCode();}
    
    public boolean equals(Object o){
        if (o==this) return true;
        if (o==null || this==null || o.getClass()!=this.getClass()) return false;
        Ponto3D p=(Ponto3D)o;
        boolean ret = super.equals(p);
        
        return (ret && this.getZ()==p.getZ());
    
    }
    
    //metodos
    public void desloca (double dx, double dy, double dz) {super.desloca (dx, dy); z += dz;}
      
    public double distanciaPlano(Ponto3D p) { return super.distancia(p);}
    
    public double distanciaEspaco(Ponto3D p){
        double d2 = this.distanciaPlano(p);
        double dz = this.varAlt(p);
        return Math.sqrt((d2*d2) + (dz*dz));
    }
    
    public double varAlt(Ponto3D p){return this.z - p.getZ();}
    
    public boolean maisAlto(Ponto3D p){return this.z>p.getZ();}
    
    public boolean maisBaixo(Ponto3D p){return this.z<p.getZ();}
    
        
    public void somaponto (double x, double y, double z) {
        super.somaPonto (x, y); this.z += z;
    }

    public void somaPonto (Ponto3D p) {
        super.somaPonto (p); z += p.getZ();
    }
    
    public Ponto3D somaPonto (double dx, double dy, double dz){
        return new Ponto3D (this.getX()+dx,this.getY()+dy,this.getZ()+dz);
    }
    

}
