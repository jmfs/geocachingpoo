
/**
 * Write a description of class Puzzel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.Iterator;
public class Puzzel extends Item 
{
    // variaveis de instancia
    ArrayList<Pergunta> perguntas;

    /**
     * Construtor vazio para objectos da classe Puzzel
     */
    public Puzzel(){
        super();
        this.perguntas = new ArrayList<Pergunta>();
    }
    
    /**
     * Construtor parameterizado para objectos da classe Puzzel com Nome, Valor e lista de perguntas
     */
    public Puzzel(String n, int valor , ArrayList<Pergunta> perg){
        super(n,valor);
        ArrayList<Pergunta> p = new ArrayList<Pergunta>(perg.size());
        Iterator<Pergunta> it  =  perg.iterator();
        while(it.hasNext()){
           p.add(it.next());
        }
        this.perguntas = p;
    }
    
    /**
     * Construtor parameterizado para objectos da classe Puzzel com Nome e Valor
     */

    public Puzzel(String n, int valor){
        super(n,valor);
        this.perguntas = new ArrayList<Pergunta>();
    }
    
    /**
     * Construtor de copia para objectos da classe Puzzel
     */
    public Puzzel(Puzzel p){
        super(p);
        this.perguntas = p.getPerguntas();
    }
    
    /**
     * Metodo getPerguntas
     * @return      lista de perguntas do puzzel
     */
    public ArrayList<Pergunta> getPerguntas(){
        ArrayList<Pergunta> p = new ArrayList<Pergunta>(this.nPerguntas());
        Iterator<Pergunta> it  =  this.perguntas.iterator(); 
        while(it.hasNext()){
           p.add(it.next());
        }
        return p;
    }
    
    /**
     * Metodo setPerguntas
     * @param       lista de perguntas
     */
    public void setPerguntas(ArrayList<Pergunta> pergs){
        ArrayList<Pergunta> p = new ArrayList<Pergunta>(pergs.size());
        Iterator<Pergunta> it  =  pergs.iterator(); 
        while(it.hasNext()){
           p.add(it.next()); 
        }
        this.perguntas = p;
    }
    
    /**
     * Metodo addPergunta que adiciona uma pergunta ao puzzel
     * @param       Pergunta a adicionar
     * @return      se foi possivel adicionar a pergunta
     */
    public boolean addPergunta(Pergunta p){
        return this.perguntas.add(p);
    }
    
    /**
     * Metodo verifaPerguntaN que verifica uma determinada 
     * @param       o numero da pergunta a responder e a opção de resposta
     * @return      a pontuaçao respetiva a pergunta, caso tenha errado devolve 0
     */
    public int verifaPerguntaN(int nPerg, int opResp){
        return this.perguntas.get(nPerg-1).verificaResp(opResp);
    }
    
    /**
     * Metodo nPerguntas conta quantas perguntas tem um puzzel
     * @return      numero de perguntas do Puzzel
     */
    public int nPerguntas(){return this.perguntas.size();}
    
    /**
     * Metodo toString que passa para String o objecto
     * @return      a informaçao do objecto em forma de string
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append( "Item-> Nome: " + this.getNome() + "; Valor: " + this.getValor() + "\n");
        s.append( "Perguntas:\n");
        for(Pergunta p : this.perguntas){
            int i=1;
            s.append("Pergunta " + i + ":"); i++;
            s.append(p.toString());
        }
        return s.toString();
    }
    
    /**
     * Metodo clone que clona um determinado objecto
     * @return      uma copia do objecto
     */
    public Puzzel clone(){return(new Puzzel(this));}
    
    /**
     * Metodo equals
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        boolean res;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Puzzel v = (Puzzel)obj;
        res = ( super.equals(v) && (this.perguntas.size()==v.perguntas.size()));
        for (int i=0;i<this.perguntas.size() && res ; i++){
            res = ( this.getPerguntas().get(i).equals(v.getPerguntas().get(i)) );
        }
        return res;
    }
    
    /**
     * Metodo hashCode
     * @return      resultado do calculo do hashCode o objecto
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
}
