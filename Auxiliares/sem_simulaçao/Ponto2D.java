
/**
 * Pontos descritos como duas coordenadas reais.
 * 
 * @author F. Mario Martins 
 * @version 2005
 */
import static java.lang.Math.abs;
public class Ponto2D {

    // Vari�veis de Inst�ncia
   private double x, y;
    
    // Construtores usuais
   public Ponto2D(double cx, double cy) { x = cx; y = cy; }
  
   public Ponto2D(){ this(0, 0); }
   
   public Ponto2D(Ponto2D p) { x = p.getX(); y = p.getY(); }


   // M�todos de Inst�ncia
   public double getX() { return x; }
   public double getY() { return y; }
   public void setX(double x) { this.x=x; }
   public void setY(double y) { this.y=y; }
   // incremento das coordenadas 
   public void incCoord(double dx, double dy) {
       x += dx; y += dy;
   }
   // decremento das coordenadas
   public void decCoord(double dx, double dy) {
      x -= dx; y -= dy;
   }
   
   //altera o valor da coordenada em Y para negativo
    public void alterYPonto() {
      y= -y;
   } 
   
    //altera o valor da coordenada em X para negativo
    public void alterXPonto() {
      x= -x;
   } 
   
   // soma coordenadas do ponto par�metro ao ponto receptor
   public void somaPonto(Ponto2D p) {
      x += p.getX(); y += p.getY();
   }
   
   public void desloca (double dx, double dy) {
     x += dx;
     y += dy;
    }
   
   // soma os valores par�metro e devolve um novo ponto
   public Ponto2D somaPonto(double dx, double dy) {
     return new Ponto2D(x += dx, y+= dy);
   }
   // determina se um ponto � sim�trico (dista do eixo dos XX o
   //  mesmo que do eixo dos YY
   public boolean simetrico() {
     return abs(x) == abs(y);
   }
   // verifica se ambas as coordenadas s�o positivas
   public boolean coordPos() {
     return x > 0 && y > 0;
   } 

   // M�todos complementares usuais

   
      public boolean direita(Ponto2D p) {
        return p.getX() > this.getX();
   }
   
      public boolean acima(Ponto2D p) {
        return p.getY() > this.getY();
   }
   
      public boolean acimaedireita(Ponto2D p) {
        return this.acima(p) && this.direita(p);
   }
   
   public boolean acimaoudireita(Ponto2D p) {
        return this.acima(p) || this.direita(p);
   }
    
   public boolean equals(Object po) {
       if (po==this) return true;
       if ((po==null) || (po.getClass()!=this.getClass()))
         return false;
       Ponto2D p=(Ponto2D)po;
       return x == p.getX() && y == p.getY();
   }
   
   public int hashCode() {
       return this.toString().hashCode();
   }
   
   // Calcula distancia entre dois pontos
   public double distancia (Ponto2D p){
       double dx = (this.getX()-p.getX());
       double dy = (this.getY()-p.getY());
       return Math.sqrt((dx*dx) + (dy*dy));
    } 
   
   // Converte para uma representa��o textual - toString()     
   public String toString() {
     return new String(x + ", " + y);
   }

   // Cria uma c�pia do ponto receptor
   public Ponto2D clone() {
      return new Ponto2D(this);
   }
  
}

