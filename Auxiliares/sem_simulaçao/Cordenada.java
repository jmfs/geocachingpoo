
/**
 * Write a description of class Cordenada here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import static java.lang.Math.abs;
import java.io.Serializable;
public class Cordenada implements Serializable
{
    // instance variables
    private Ponto3D pt;
    private static final double RTERRA = 6378.137;
    
    private static double round(double value, int places) {
        if (places < 0) return value;

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    public Cordenada(){
        this.pt= new Ponto3D();
    }
    public Cordenada(Cordenada c){
        this.pt = c.getPonto();
    }
    public Cordenada(int laco, double lamin, char laor, int lco, double lmin, char lor, double alt){
        this.pt = new Ponto3D(converte(lco,lmin,lor),converte(laco,lamin,laor),alt);
    }
    
    public String toString(){return this.pt.toString();}
    
    
    public Cordenada clone(){return new Cordenada(this);}
    
    public boolean equals(Object o){
        if (o==this) return true;
        if (o==null || this==null || o.getClass()!=this.getClass()) return false;
        Cordenada c=(Cordenada)o;
        return (this.pt.equals(c.getPonto()));
    
    }
   
    public int hashCode(){return this.toString().hashCode();}
    
    public void setCoordenada(int laco, double lamin, char laor, int lco, double lmin, char lor, double alt){this.pt = new Ponto3D(converte(lco,lmin,lor),converte(laco,lamin,laor),alt);}
    
    public double getAlt(){return this.pt.getZ();}
    public Ponto3D getPonto(){return this.pt.clone();}

    private double converte(int co, double min, char or){
        double pt = co + min/60;
        if(or=='s' || or=='S' || or=='O' || or =='o'){
            pt = -1*pt;
        }
        return pt;
    }
    
    public double distKM(Cordenada co){
        double dLat  = rad( co.getPonto().getY() - this.getPonto().getY());
        double dLong = rad( co.getPonto().getX() - this.getPonto().getX());
        
        double a = (Math.sin(dLat/2) * Math.sin(dLat/2)) + Math.cos(rad(this.getPonto().getY())) * Math.cos(rad(co.getPonto().getY())) * Math.sin(dLong/2) * Math.sin(dLong/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        
        return c*RTERRA;
    }
 
    private double rad(double x){return x*Math.PI/180;}
    
    public double distM(Cordenada co){return this.distKM(co)*1000;}
    
    public boolean maisPertoM(Cordenada co, double dm){return (this.distM(co)<dm);}
    public boolean maisPertoKM(Cordenada co, double dKm){return (this.distKM(co)<dKm);}
    
    
    public String latitForm(){
        double num = this.getPonto().getY();
        long iPart = (long) Math.abs(num);
        double fPart = (double)Math.round((Math.abs(num) -iPart)*1000)/1000;
        char or='N';
        if(num<0) or='S';
        return ( iPart +"º" + round((fPart *60),3) + "\" " + or);
    } 
    
    public String longitForm(){
        double num = this.getPonto().getX();
        long iPart = (long) Math.abs(num);
        double fPart = (double)Math.round((Math.abs(num) -iPart)*1000)/1000;
        char or='E';
        if(num<0) or='O';
        return ( iPart +"º" + round((fPart *60),3) + "\" " + or );
    }
    
    public boolean maisNorte(Cordenada c){return this.getPonto().acima(c.getPonto());}
    public boolean maisAlto(Cordenada c){return this.getPonto().maisAlto(c.getPonto());}
    public boolean maisEste(Cordenada c){return this.getPonto().direita(c.getPonto());}
    
    public boolean maisSul(Cordenada c){return (!this.maisNorte(c));}
    public boolean maisBaixo(Cordenada c){return (!this.maisAlto(c));}
    public boolean maisOeste(Cordenada c){return (!this.maisEste(c));}
    
    
    
}
