
/**
 * Write a description of class MisteryCache here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.GregorianCalendar;
import java.util.ArrayList;
public class MisteryCache extends FisicCache
{
    // instance variables
    private int size; //1..4
    private Puzzel puzz; 

    /**
     * Construtor vasio para objetos da classe MisteryCache 
     */
    public MisteryCache(){
        super();
        this.size=2;
        this.puzz=new Puzzel();
    }
    
    /**
     * Construtor parametrizado para objetos da classe MisteryCache 
     */
    public MisteryCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code,int size,Puzzel pz){
        super(utl,l,dt,n,desc,dM,dF,p,code);
        this.size|=2;
        this.puzz = pz.clone();
    }
    
    /**
     * Construtor parametrizado(total) para objetos da classe MisteryCache 
     */
    public MisteryCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t,int size,Puzzel pz){
        super(utl,l,dt,n,desc,dM,dF,p,code,t);
        this.size=size;
        this.puzz = pz.clone();
    }
    /**
     * Construtor de copia para objectos da classe MisteryCache
     */
    public MisteryCache(MisteryCache m){
        super(m);
        this.size=m.getSize();
        this.puzz=m.getPuzz();
    }
    
    /**
     * GETTERS
     */
    public int getSize(){return this.size;}
    /**
     * GETTERS
     */
    public Puzzel getPuzz(){return this.puzz.clone();}
    
    /**
     * SETTERS
     */
    public void setSize(int s){this.size=s;}
    /**
     * SETTERS
     */
    public void setSize(Puzzel p){this.puzz=p.clone();}
    
    /**
     * 
     */
    public int verificaPuzzel(int np,int opResp){
        return this.getPuzz().verifaPerguntaN(np,opResp);
       
    }
    
    /**
     * toString
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Tamanho: " + this.getSize() +"\n");
        s.append("Puzzel: \n");
        s.append(this.getPuzz().toString());
        return s.toString();
    }
    
    /**
     * Clone
     */
    public MisteryCache clone(){return new MisteryCache(this);}
    
    /**
     * equals
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        MisteryCache mc = (MisteryCache) obj;
        if (!super.equals(mc)) return false;
        if(!(this.getSize()==mc.getSize() && this.getPuzz().equals(mc.getPuzz()))) return false;
        return res;
    }
}
