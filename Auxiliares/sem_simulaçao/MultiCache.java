/**
 * Write a description of class MultCache here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.GregorianCalendar;
import java.util.Iterator;
public class MultiCache extends FisicCache
{
    // variaveis de instancia
    private ArrayList<Integer> conj;// Id da cahes da multi nao podem estar neste conjunto EventCache nem MultiCache
    private HashMap<String,HashSet<Integer>> userProg;//progresso do user(mail) e conjinto dos id de cachesk

    /**
     * Construtor vazio para objectos da classe MultCache
     */
    public MultiCache(){
        super();
        this.conj = new ArrayList<Integer>();
        this.userProg = new HashMap<String,HashSet<Integer>>();
    }
    
    /**
     * Construtor parameterizado para objectos da classe MultCache Email, Cordenada, data, Nome, Descrição,Dificuldade mental, 
     * Dificuldade Fisica, Pontuação, codigo de verificação e lista de Itens
     */
    public MultiCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t){
        super(utl,l,dt,n,desc,dM,dF,p,code,t);
        this.conj = new ArrayList<Integer>();
        this.userProg = new HashMap<String,HashSet<Integer>>();
    }

    /**
     * Construtor parameterizado para objectos da classe MultCache Email, Cordenada, data, Nome, Descrição,Dificuldade mental, 
     * Dificuldade Fisica, Pontuação, codigo de verificação e lista de Itens
     */
    public MultiCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t, ArrayList<Integer> conjt){
        super(utl,l,dt,n,desc,dM,dF,p,code,t);
        ArrayList<Integer> res = new ArrayList<Integer>();
        for(Integer cache : conjt){
            res.add(cache);
        }
        this.conj = res;
        this.userProg = new HashMap<String,HashSet<Integer>>();
    }
    
    /**
     * Construtor de copia para objectos da classe MultCache
     */
    public MultiCache(MultiCache mc){
        super(mc);
        this.conj = mc.getConj();
        this.userProg = mc.getUserProg();
    }

    /**
     * Metodo getConj
     * @return      lista do conjunto de caches que pertencem a multi-cache
     */
    public ArrayList<Integer> getConj(){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(this.conj);
        return res;
    }
    
    /**
     * Metodo getUserProg
     * @return      progresso de cada utilizador
     */
    public HashMap<String,HashSet<Integer>> getUserProg(){
        HashMap<String,HashSet<Integer>> res = new HashMap<String,HashSet<Integer>>();
        Iterator<String> itKey = this.userProg.keySet().iterator();
        while(itKey.hasNext()){
            String n = itKey.next();
            HashSet<Integer> resSet = new HashSet<Integer>();
            resSet.addAll(this.userProg.get(n));
            res.put(n,resSet);
        }
        return res;
    }
    
    /**
     * Metodo setConj
     * @param       lista do conjunto de caches que pertencem a multi-cache
     */
    public void setConj(ArrayList<Integer> c){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(c);
        this.conj = res;
    }
    
    /**
     * Metodo setUserProg
     * @param       progresso de cada utilizador
     */
    private void setUserProg(HashMap<String,HashSet<Integer>> users){
        HashMap<String,HashSet<Integer>> res = new HashMap<String,HashSet<Integer>>();
        Iterator<String> itKey = users.keySet().iterator();
        while(itKey.hasNext()){
            String n = itKey.next();
            HashSet<Integer> resSet = new HashSet<Integer>();
            resSet.addAll(users.get(n));
            res.put(n,resSet);
        }
        this.userProg = res;
    }
    
    /**
     * Metodo nConj
     * @return      o numero de caches que fazem parte desta multicache 
     */
    public int  nConj(){return this.getConj().size();}
    
    /**
     * Metodo nUtliProg
     * @return      o numero de utilizadores que já emcontraram pelo menos uma cache do conjunto 
     */
    public int nUtliProg(){return this.getUserProg().size();}
    
    /**
     * Metodo addCache
     * @param       id da cache a adicionar
     * @return      se foi possivel adicionar a cache
     */
    public boolean addCache(Integer c){return this.conj.add(c);}
    
    /**
     * Metodo removeCache
     * @param       id da cache a remover
     * @return      se foi possivel remover a cache
     */
    public boolean removeCache(Integer c){return this.conj.remove(c);}
    
    /**
     * Metodo inicializaProgUser
     * @param       email utiliador
     * @param       lista de caches que um utilizador visitou
     * @return      booelan se foi possivel adicionar ao progresso as caches que esse user já tinha visitado
     */
    public boolean inicializaProgUser(String user, ArrayList<Integer> listCache){
        boolean b= true;
        HashSet<Integer> res = this.getUserProg().get(user);
        for(Integer c : this.getConj()){
            if(listCache.contains(c)){
                b=res.add(c);
            } 
        }
        return b;
    }
    
    /**
     * Metodo atualizaProgUser
     * @param       email do utilizador
     * @param       id da cache que o utilizador visitou
     * @return      se foi possivel adicionar a cache ao progresso do utilizador
     */
    public boolean atualizaProgUser(String user, Integer c){
        boolean res = false;
        //so se ele nao contiver é que adivionas
        if(!this.conj.contains(c)) {res = this.userProg.get(user).add(c);}
        return res;
    }
    
    /**
     * Metodo visitouQuant
     * @param       email do utilizador
     * @return      quantas caches do conjunto da multi-cache já visitou
     */
    public int visitouQuant(String utl){return this.getUserProg().get(utl).size();}
    
    /**
     * Metodo visitouTodas
     * @param       email do utilizador
     * @return      se já visitou todas as caches 
     */
    public boolean visitouTodas(String utl){return this.nConj()==visitouQuant(utl);}
    
    /**
     * Metodo toString
     * @return      String
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Conjunto de Caches: n: " + this.getConj() +"\n");
        for(Integer c : this.getConj()){s.append(c.toString());}
        Iterator<String> itKey = this.getUserProg().keySet().iterator();
        while(itKey.hasNext()){
            String u = itKey.next();
            s.append("User: " + u);
            for(Integer c : this.getUserProg().get(u)){s.append("-> " + c.toString()+ "\n");}
        }
        return s.toString();
    }
    
    /**
     * Metodo Clone
     * @retun       copia
     */
    public MultiCache clone(){return new MultiCache(this);}
    
    /**
     * Metodo equals
     * @param       objecto a comparar
     * @return      resultado da igualdade
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        MultiCache mc = (MultiCache) obj;
        if (!super.equals(mc)) return false;
        if(!( this.nUtliProg()==mc.nUtliProg() && this.nConj() == mc.nConj() )) return false;
        for(int i=0; res && i<nConj();i++){
            this.getConj().get(i).equals(mc.getConj().get(i));
        }
        Iterator<String> itKeyT = this.getUserProg().keySet().iterator();
        Iterator<String> itKeyO = mc.getUserProg().keySet().iterator();
        while( res && itKeyT.hasNext() && itKeyO.hasNext()){
            String uT = itKeyT.next();
            String uO = itKeyO.next();
            if(!(uT.equals(uO))) return false;
            Iterator<Integer> itValeusT = this.getUserProg().get(uT).iterator();
            Iterator<Integer> itValeusO = mc.getUserProg().get(uO).iterator();
            while( res && itValeusT.hasNext() && itValeusO.hasNext()){
                itValeusT.next().equals(itValeusO);
            }
        }
        return res;
    }
}
