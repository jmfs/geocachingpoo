
/**
 * Write a description of class VerifCache here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.GregorianCalendar;

public class FisicCache extends Cache
{
    // variaveis de instancia
    private String veriCode;
    private ArrayList<Item> trade; //lista de itens que estao na cache
    
    /**
     * Construtor vazio para objectos da class FisicCache
     */
    public FisicCache(){
        super();
        this.veriCode = new String();
        this.trade = new ArrayList<Item>();
    }
    /**
     * Construtor parametrizado para objectos da class FisicCache com Email 
     */
    public FisicCache(String utl){
        super(utl);
        this.veriCode = new String();
    }
    /**
     * Construtor parametrizado para objectos da class FisicCache com Email, cordenada, data, nome, descrição, dif. mental, dif. fisica, pontuação e codigo de verificação
     */
    public FisicCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code){
        super(utl,l,dt,n,desc,dM,dF,p);
        this.veriCode = code;
        this.trade = new ArrayList<Item>();;
    }
    /**
     * Construtor parametrizado para objectos da class FisicCache com Email, cordenada, data, nome, descrição, dif. mental, dif. fisica, pontuação, codigo de verificação e lista de itens 
     */
    public FisicCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t){
        super(utl,l,dt,n,desc,dM,dF,p);
        this.veriCode = code;
        ArrayList<Item> res = new ArrayList<Item>(t.size());
        for(Item i : t){res.add(i.clone());}
        this.trade =res;
    }
    /**
     * Construtor de copia para objectos da class FisicCache
     */
    public FisicCache(FisicCache fc){
        super(fc);
        this.veriCode=fc.getVeriCode();
        this.trade=fc.getTrade();
    }
    
    /**
     * Metodo getVeriCode
     * @return      codigo de verificação
     */
    public String getVeriCode(){return this.veriCode;}
    
    /**
     * Metodo getTrade
     * @return      lista de itens que a cache possui
     */
    public ArrayList<Item> getTrade(){
        boolean b=true;
        ArrayList<Item> res = new ArrayList<Item>();
        for(Item i : this.trade ){
            res.add(i.clone());
        }
        return res;
    }
    
    /**
     * Metodo setVeriCode
     * @param       codigo de verificaçao
     */
    public void setVeriCode(String code){this.veriCode=code;}
    
    /**
     * Metodo setTrade
     * @param       lista de itens
     */
    public void setTrade(ArrayList<Item> td){
        ArrayList<Item> t = new ArrayList<Item>();
        for(Item i : td){
            t.add(i.clone());
        }
        this.trade = t;
    }
    
    /**
     * Metodo nItens
     * @return      numero de itens que a cache tem
     */
    public int nItens(){return this.getTrade().size();}
    
    /**
     * Metodo que verifica se um determinado codigo esta correcto
     * @param       code
     * @return      se os codigos sao iguais 
     */
    public boolean verificaCodigo(String code){
        return this.getVeriCode().equals(code);
    }
    
    /**
     * Metodo tradeItem que troca itens de uma cache, para retirar um Item é obrigatorio adicionar outro
     * @param       item para ser adicionado
     * @param       item escolhido para ser removido
     * @return      se foi possivel remover e adicionar os itens
     */
    public boolean tradeItem(Item add, Item rm){
        boolean res = false;
        if(!( this.getTrade().size() == 0 || this.trade.contains(rm))) return false;
        if(this.trade.remove(rm.clone()) ){ this.trade.add(add.clone()); res = true;}
        return res;
    }
    
    /**
     * Metodo addItem que adiciona um Item a lista de itens para troca
     * @param       item para ser adicionado
     * @return      se foi possivel  adicionar o item 
     */
    public boolean addItem(Item i){return this.trade.add(i.clone());}
    
    /**
     * Metodo removeItem que remove um Item a lista de itens para troca
     * @param       item para ser removido
     * @return      se foi possivel  removido o item 
     */
    public boolean removeItem(Item i){return this.trade.remove(i.clone());}
    
    /**
     * Metodo clone
     * @return      copia 
     */
    public FisicCache clone(){return new FisicCache(this);}
    
    /**
     * Metodo toString
     * @return      string 
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Codigo de Verificação: " + this.getVeriCode()+ "\n");
        for(Item i : this.getTrade()){s.append(i.toString());}
        return s.toString();
    }
    
    /**
     * Metodo equals
     * @return      resultado da comparaçao da igualdade entre dois objectos 
     */
    public boolean equals(Object obj){
        boolean res=true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        FisicCache f = (FisicCache) obj;
        if (!super.equals(f)) return false;
        if(!(this.getVeriCode().equals(f.getVeriCode()) && this.nItens()==f.nItens())) return false;
        for(int i=0;i<this.nItens() && res;i++){res = this.getTrade().get(i).equals(f.getTrade().get(i));}
        return res;
    }
}
