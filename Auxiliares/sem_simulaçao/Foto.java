/**
 * Write a description of class Foto here.
 * 
 * @author (POOGrupo55) 
 * @version (a version number or a date)
 */

public class Foto extends Item
{
    // variaveis de instancia
    private String url;
    private String desc;

    /**
     * Construtor vazio para objectos da classe Foto
     */
    public Foto(){
       super();
       this.url = new String();
       this.desc = new String();
    }
    
    /**
     * Construtor parameterizado para objectos da classe Foto com Nome, Valor, Url, Descrição
     */
    public Foto(String nome, int valor, String url, String desc){
        super(nome,valor);
        this.url=url;
        this.desc = desc;
    }
    
    /**
     * Construtor de copia para objectos da classe Foto
     */
    public Foto(Foto f){
        super(f);
        this.url = f.getUrl();
        this.desc = f.getDesc();
    }
    
    /**
     * Metodo getUrl
     * @return      url da imagem
     */
    public String getUrl(){return this.url;}
    
    /**
     * Metodo getDesc
     * @return      descrição da imagem
     */
    public String getDesc(){return this.desc;}
    
    /**
     * Metodo setUrl
     * @param       url de uma imagem
     */
    public void setUrl(String url){this.url=url;}
    
    /**
     * Metodo setDesc
     * @param       descrição de uma imagem
     */
    public void setDesc(String desc){ this.desc=desc;}

    /**
     * Metodo toString
     * @return  String   
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append( "URL: " + this.getUrl() + "\n");
        s.append( "Descrição: " + this.getDesc() + "\n");
        return s.toString();
    }
    
    /**
     * Metodo clone
     * @return     uma copia 
     */
    public Foto clone(){return(new Foto(this));}

    /**
     *  Metodo equals
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Foto v = (Foto)obj;
        return ( super.equals(v) && (this.getUrl().equals(v.getUrl()))
                    && (this.getDesc().equals(v.getDesc())));
    }
    
    /**
     * Metodo hashCode
     * @return     valor de hash
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
}
