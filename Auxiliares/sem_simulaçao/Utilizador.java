
/**
 * Write a description of class Utilizador here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeMap;
import java.util.Calendar;
import java.io.Serializable;
public class Utilizador implements Serializable
{
    // instance variables 
    private String email; // id do utilizador diferente para todos
    private String password;
    private String nome;
    private GregorianCalendar datNac;
    private ArrayList<Integer> hidden; //contem as caches que o utilizador escondeu
    
    /**
     * Contrutor vazio da classe Utilizador
     */
    public Utilizador()
    {
        // initialise instance variables
        this.email = "";
        this.password = "";
        this.nome="";
        this.datNac = new GregorianCalendar();
        this.hidden = new ArrayList<Integer>();

    }

    /**
     * Contrutor de coipa da classe Utilizador
     */
    public Utilizador(Utilizador u)
    {
        // initialise instance variables
        this.email = u.getEmail();
        this.password = u.getPassword();
        this.nome=u.getNome();
        this.datNac = u.getDatNac();
        this.hidden = u.getHidden();
       
    }
    /**
     * Contrutor parametrizado da classe Utilizador com GregorianCalendar
     */
    public Utilizador(String email, String password, String nome, GregorianCalendar data )
    {
        // initialise instance variables
        this.email = email;
        this.password = password;
        this.nome=nome;
        this.datNac = (GregorianCalendar)data.clone();
        this.hidden = new ArrayList<Integer>();
       
    }
    
    /**
     * Contrutor parametrizado da classe Utilizador com Ano,Mes,Dia
     */
    public Utilizador(String email, String password, String nome, int ano, int mes, int dia )
    {
        // initialise instance variables
        this.email = email;
        this.password = password;
        this.nome = nome;
        this.datNac = new GregorianCalendar(ano,mes,dia);
        this.hidden = new ArrayList<Integer>();
       
    }
    /**
     * GETTERS
     */
    public String getEmail(){return this.email;}
    public String getId(){return this.getEmail();}
    public String getPassword(){return this.password;}
    public String getNome(){return this.nome;}
    public GregorianCalendar getDatNac(){return (GregorianCalendar)this.datNac.clone();}
    public ArrayList<Integer> getHidden(){
        ArrayList<Integer> novo = new ArrayList<Integer>(this.quantasEscondidas());
        /*for(Integer ca : this.hidden){
            novo.add((Integer)ca.clone());
        }*/
        novo.addAll(this.hidden);
        return novo;
    }
    /**
     * SETTERS
     */
    
    private void setEmail(String mail){this.email=mail;}
    public void setPassword(String pass){this.password=pass;}
    public void setNome(String nome){this.nome=nome;}
    public void setDatNac(GregorianCalendar g){this.datNac=(GregorianCalendar)g.clone();}
    public void setDatNac(int ano,int mes,int dia){
        this.datNac=new GregorianCalendar(ano,mes,dia);
    }
    private void setHidden(ArrayList<Integer> hd){
       ArrayList<Integer> novo = new ArrayList<Integer>(hd.size());
        novo.addAll(hd);
        this.hidden=novo; 
    }
    /**
     * Clone
     * 
     * @return     Utilizador (copia do que recebe a mensagem)
    */
    public Utilizador clone(){return new Utilizador(this);}
    
    /**
     * ToString
     * 
     *@return     String (info do Utilizador)
    */
   public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Nome: " + this.getNome() + "\n");
        s.append("Mail: " + this.getEmail() + "\n");
        s.append("Password: " + this.getPassword() + "\n");
        s.append("Data nascimento: " + this.datNac.get(Calendar.DAY_OF_MONTH)+ "/" + this.datNac.get(Calendar.MONTH) + "/" + this.datNac.get(Calendar.YEAR) +"\n");
        s.append("Caches("+this.quantasEscondidas()+ "): ");
        for(Integer ca : this.hidden){
            s.append("\t"+ ca.toString() + ";\n");
        }
        
        return s.toString();
        
    }
    /**
     * Equals
     * 
     *@param      Object a comprar com o receptor da mensagem
     *@return     boolean (utilizador que recebe a mensagem igual(deep) ao argumeto)
     *
    */
   public boolean equals(Object o){
       if (this==o) return true;
       if(this==null || o==null || this.getClass()!=o.getClass()) return false;
       Utilizador u = (Utilizador)o;
       boolean ret= true;
       ret = ret && this.getDatNac().equals(u.getDatNac()) && this.getEmail().equals(u.getEmail()) && this.getNome().equals(u.getNome()) && this.getPassword().equals(u.getPassword());
       ret = ret && this.quantasEscondidas() == u.quantasEscondidas();
       if (ret==false) return false;
       Iterator<Integer> itu1 = this.getHidden().iterator();
       Iterator<Integer> itu2 = u.getHidden().iterator();
       while(ret && itu1.hasNext() && itu2.hasNext() ){
            ret = itu1.next().equals(itu2.next()); 
        }
       if(itu1.hasNext() != itu2.hasNext() || ret==false) return false;
       return true;
    }
   /**
     * hashCode
     * 
     *@return     int (Codigo hash do utilizador)
     *
    */ 
   public int hashCode(){return this.getEmail().hashCode();}
      
   /**
     * compareTo
     * 
     *@param      Utilizador a comparar
     *@return     int defaut dos comparador
     *
    */
   
   public int compareTo(Utilizador user){
       return this.getEmail().compareTo(user.getEmail());
       
    }
   
   //Metodos
     /**
     * Metodo que dá o numero de caches que o utilizador escondeu
     * 
     * @return     int (Numero das cahes escondidas)
     */
    public int quantasEscondidas(){return this.hidden.size();}
 
    /**
     * donoCache
     * 
     *@param      Integer (id ca cache em analise)
     *@return     boolen se este utilizador ja encontrou a cache com esse id
     *
    */
    public boolean donoCache(Integer id){return this.hidden.contains(id);}
    
    /*
     * donoCache
     * 
     *@param      Cache (cache em analise)
     *@return     boolen se este utilizador ja encontrou a cache com esse id
     *
    */
    //public boolean donoCache(Cache ca){return this.donoCache(ca.getId());}
   
    /**
     * addHidden
     * 
     *@param      Integer (id da cache a adicionar à lista das escondidas)
     *
    */
    public boolean addHidden(Integer id){
        if(this.hidden.contains(id)) return false;
        this.hidden.add(id);
        return true;
    }
    
    /*
     * addHidden
     * 
     *@param      Cache (Cache a adicionar à lista das escondidas)
     *
    */
    //public boolean addHidden(Cache id){return this.addHidden(id.getId());}
    
     /**
     * removeHidden
     * 
     *@param      Cache (id da cache a remover da lista das escondidas)
     *
    */
    private void removeHidden(Integer id){this.hidden.remove(id);}
    
    /*
     * removeHidden
     * 
     *@param      Cache (Cache a remover da lista das escondidas)
     *
    */
    //private void removeHidden(Cache id){this.removeHidden(id.getId());}
    //Pedir ao prof
    
    /**
     * getDataNacFotmatada
     * 
     * 
     * @return  String  data de nacimento no formato DD/MM/AAAA
     */
    public String getDataNacFotmatada(){return (this.datNac.get(Calendar.DAY_OF_MONTH)+1) + "/" + (this.datNac.get(Calendar.MONTH)+1) + "/" + this.datNac.get(Calendar.YEAR);}
    /**
     * howOld
     * 
     * 
     * @return  int  a idade do utilizador tendo a data do sistema com referencia
     */
    public int howOld(){
        
        GregorianCalendar now = new GregorianCalendar();
        long btMili = this.getDatNac().getTimeInMillis();
        long nowMili = now.getTimeInMillis();
        long differenceInSeconds = (now.getTimeInMillis() - this.getDatNac().getTimeInMillis());
        GregorianCalendar age = new GregorianCalendar();
        age.setTimeInMillis(differenceInSeconds);
        
        return age.get(Calendar.YEAR) -1900;
        
    }
    
    /**
     * howOld
     * 
     * @param GregorianCalendar referecia para a decisao
     * @return int  a idade do utilizador tendo a data do GregorianCalendar parametro com referencia
     */
        public int howOld(GregorianCalendar today){
        long btMili = this.getDatNac().getTimeInMillis();
        long nowMili = today.getTimeInMillis();
        long differenceInSeconds = (today.getTimeInMillis() - this.getDatNac().getTimeInMillis());
        GregorianCalendar age = new GregorianCalendar();
        age.setTimeInMillis(differenceInSeconds);
        
        return age.get(Calendar.YEAR) -1900;
        
    }
    
    /**
     * howOld
     * 
     * @param (int ano referencia, int mes referencia, int dia referencia) para a decisao
     * @return int  a idade do utilizador tendo a data parametro com referencia
     */
    public int howOld(int ano,int mes,int dia){
        
        GregorianCalendar now = new GregorianCalendar(ano,mes,dia);
        long btMili = this.getDatNac().getTimeInMillis();
        long nowMili = now.getTimeInMillis();
        long differenceInSeconds = (now.getTimeInMillis() - this.getDatNac().getTimeInMillis());
        GregorianCalendar age = new GregorianCalendar();
        age.setTimeInMillis(differenceInSeconds);
        
        return age.get(Calendar.YEAR) -1900;
        
    }
    
    /**
     * isAdmin
     * 
     *@return     bollean false pois por default nao se cria admin, implementar a true na class admin
     *
    */
    public boolean isAdmin(){return false;}
    
    
    /**
     * maiorIdade
     * 
     * @return  se a idade do utilizador é maior que 18
     */
    public boolean maiorIdade(){return (this.howOld()>=18);}
    /**
     * menorIdade
     * 
     * @return  se a idade do utilizador é menor que 18
     */
    public boolean menorIdade(){return (this.howOld()<18);}
    
    /**
     * maiorIdade
     * 
     * @param GregorianCalendar referecia para a decisao
     * @return  se a idade do utilizador é maior que 18 na data do GregorianCalendar de referencia
     */
    public boolean maiorIdade(GregorianCalendar today){return (this.howOld(today)>=18);}
    
    /**
     * menorIdade
     * 
     * @param GregorianCalendar referecia para a decisao
     * @return  se a idade do utilizador é menor que 18 na data do GregorianCalendar de referencia
     */
    public boolean menorIdade(GregorianCalendar today){return (this.howOld(today)<18);}
    
    /**
     * maiorIdade
     * 
     * @param (int ano referencia, int mes referencia, int dia referencia)
     * @return  se a idade do utilizador é maior que 18 na data de referencia
     */
    public boolean maiorIdade(int ano,int mes,int dia){return (this.howOld(ano,mes,dia)>=18);}
    
    /**
     * menor
     * 
     * @param (int ano referencia, int mes referencia, int dia referencia)
     * @return  se a idade do utilizador é maior que 18 na data de referencia
     */
    public boolean menorIdade(int ano,int mes,int dia){return (this.howOld(ano,mes,dia)<18);}
}
