

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TestePergunta.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class TestePergunta
{
    /**
     * Default constructor for test class TestePergunta
     */
    public TestePergunta()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testepergunta1(){
        Pergunta p1 = new Pergunta();
        Pergunta p2 = p1.clone();
        assertEquals(true,p1.equals(p2));
        Pergunta p3 = new Pergunta("Quem sou eu","Rui","Joao","Rui",5);
        assertEquals(5,p3.verificaResp(1));
        assertEquals(0,p3.verificaResp(2));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
