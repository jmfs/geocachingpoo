

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TestePuzzel.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class TestePuzzel
{
    /**
     * Default constructor for test class TestePuzzel
     */
    public TestePuzzel()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testePuzzel(){
        Puzzel p1 = new Puzzel();
        assertEquals(0,p1.nPerguntas());
        Pergunta pe1 = new Pergunta("Quem sou eu", "Rui","Joao","Rui",5);
        
        Puzzel p2 = new Puzzel();
        assertEquals(true,p2.addPergunta(pe1));
        assertEquals(1,p2.nPerguntas());
        assertEquals(5,p2.verifaPerguntaN(1,1));
    }
}
