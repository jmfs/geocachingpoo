
/**
 * Write a description of class CoinItem here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Iterator;
public class CoinItem extends Item
{
    // instance variables
    private GregorianCalendar fstDat;
    private ArrayList<Integer> historico; //mudar para id
     
    /**
     * Constructors for objects of class CoinItem
     */
    public CoinItem(){
        super();
        this.fstDat = new GregorianCalendar();
        this.historico = new ArrayList<Integer>();
    }
    
    public CoinItem(String nome , int valor, GregorianCalendar fstDat, ArrayList<Integer> h){
        super(nome,valor);
        this.fstDat = fstDat;
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(h); 
        this.historico = res;
    }
    
    public CoinItem(String nome, int valor, GregorianCalendar fstDat){
        super(nome,valor);
        this.fstDat = (GregorianCalendar)fstDat.clone(); //atençao a isto penso que temos de fazer clone
        this.historico = new ArrayList<Integer>();
    }
    
    public CoinItem(CoinItem c){
        super(c);
        this.fstDat = c.getFstDat();
        this.historico = c.getHistorico();
    }
    
    
    /**
     * GETTERS
     */
    public GregorianCalendar getFstDat(){return (GregorianCalendar)fstDat.clone();}
    public ArrayList<Integer> getHistorico(){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(this.historico);
        return res;
    }
    
    /**
     * SETTERS
     */
    public void setFstDat(GregorianCalendar d){this.fstDat = (GregorianCalendar)d.clone();}//atençao a este clone verificar se isto se pode fazer
    public void setHistorico(ArrayList<Integer> h){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res.addAll(h);
        this.historico = res;
    }
    
    /**
     * Metodo que adicciona local a CoinItem por onde passou
     * 
     * @param  c  recebe a cache por onde passou
     * @return     se foi possivel adicionar esse local a CoinItem
     */
    public boolean addLocal(Integer id ){ return this.historico.add(id);}
    
    /**
     * Metodo conta quantos locais este CoinItem já visitou
     * 
     * @return     numero de cahces por onde passou
     */
    public int nVisitou(){
        return this.historico.size();
    }
    
    /**
     * Metodo diz qual foi o ultimo local visitado por uma CoinItem
     * 
     * @return     o ultimo local por onde passou
     */
    public Integer lastLocal(){
        return this.historico.get(this.nVisitou()-1);
    }
    
    /**
     * Metodo hashCode
     *
     * @return      resultado do calculo do hashCode o objecto
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    
    /**
     * Metodo que passa para String o objecto
     *
     * @return      a informaçao do objecto em forma de string
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append( super.toString());
        s.append("Data primeiro local: " );
        s.append("Ano: " +  this.fstDat.get(Calendar.YEAR) + "Mes: " + (this.fstDat.get(Calendar.MONTH) + 1 )+ "Dia: " + (this.fstDat.get(Calendar.DAY_OF_MONTH) + 1) +"\n");
        s.append( "Locais:\n");
        for(Integer p : this.historico){
            s.append(p.toString());
        }
        return s.toString();
    }
    
    /**
     * Metodo que clona um determinado objecto
     *
     * @return     uma copia do objecto
     */
    public CoinItem clone(){return(new CoinItem(this));}
    
    
    /**
     * Metodo equals
     *
     * @param       objecto que vai comprar
     * @return      resultado da comprarçao dos dois objectos
     */
    public boolean equals(Object obj){
        boolean res;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        CoinItem v = (CoinItem)obj;
        res = ( super.equals(v) 
                        && (this.historico.size()==v.historico.size())
                            && this.fstDat.equals(v.getFstDat()));
        for (int i=0;i<this.historico.size() && res ; i++){
            res = ( this.historico.get(i).equals(v.historico.get(i)) );
        }
        return res;
    }
    
}
