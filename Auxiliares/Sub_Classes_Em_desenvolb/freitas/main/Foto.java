
/**
 * Write a description of class Foto here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Foto extends Item
{
    // instance variables
    private String url;
    private String desc; //descrição da imagem

    /**
     * Constructors for objects of class Foto
     */
    public Foto(){
       super();
       this.url = new String();
       this.desc = new String();
    }
    public Foto(String nome, int valor, String url, String desc){
        super(nome,valor);
        this.url=url;
        this.desc = desc;
    }
    public Foto(Foto f){
        super(f);
        this.url = getUrl();
        this.desc = f.getDesc();
    }
    
    /**
     * GETTERS
     */
    public String getUrl(){return this.url;}
    public String getDesc(){return this.desc;}
    
    /**
     * SETTERS
     */
    public void setUrl(String url){this.url=url;}
    public void setDesc(String desc){ this.desc=desc;}

    /**
     * Metodo toString
     *
     * @return  String   
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append( "URL: " + this.getUrl() + "\n");
        s.append( "Descrição: " + this.getDesc() + "\n");
        return s.toString();
    }
    
    /**
     * Metodo clone
     *
     * @return     uma copia 
     */
    public Foto clone(){return(new Foto(this));}

    /**
     *  Metodo equals
     *  
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Foto v = (Foto)obj;
        return ( super.equals(v) && (this.getUrl().equals(v.getUrl()))
                    && (this.getDesc().equals(v.getDesc())));
    }
    
    /**
     * Metodo hashCode
     *
     * @return     valor de hash
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
}
