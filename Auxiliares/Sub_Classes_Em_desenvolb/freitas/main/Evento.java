
/**
 * Write a description of class Evento here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.GregorianCalendar;
import java.util.Calendar;
public class Evento
{
    //classe variables
    private static int idEvent=0;
    private static final int maxD=200;
    private static final int difD=5;
    
    // instance variables - replace the example below with your own
    private int id;
    private String nome;
    private Local local;
    private double raio;
    private ArrayList<Integer> conj;// ID cache nao podem estar neste conjunto(variavel com o raio) EventCache nem MultiCache
    private HashMap<String,HashSet<Integer>> userProg; //mail do user e id das caches que encontrou
    private HashMap<String,Integer> startProg; //estado inicial ni evento
    private GregorianCalendar dCreat;
    private GregorianCalendar endInsc;
    private GregorianCalendar inEvent;
    private GregorianCalendar endEvent;
    private String desc; //descriçao
    private int partic;
    private int maxpartic;
    private int dific; //dificuldade de 1..10
    private ArrayList<String> pistas;
    //private TreeMap<Utilizador,Integer> pont; // duvidas
    
    
    private static int IDEvento(){return idEvent;}
    private static int daIDEvento(){
        int i = idEvent;
        idEvent++;
        return i;
    }
    
    
    /**
     * Construtor vazio para objectos da classe Evento
     */
    public Evento(){
        this.id=daIDEvento();
        this.nome=new String();
        this.local=new Local();
        this.raio=3;
        this.conj = new ArrayList<Integer>();
        this.userProg = new HashMap<String,HashSet<Integer>>();
        this.startProg = new HashMap<String,Integer>();
        this.dCreat = new GregorianCalendar();
        this.endInsc = new GregorianCalendar();
        this.inEvent = new GregorianCalendar();
        this.endEvent = new GregorianCalendar();
        this.desc = new String();
        this.dific = 5;
        this.partic = 0;
        this.maxpartic = maxD;
        this.pistas = new ArrayList<String>();  
    }

    /**
     * Construtor parameterizado para objectos da classe Evento Nome-Local-caches-StartProg-dCreat-endInsc
     */
    public Evento(String n, Local l, double r, ArrayList<Integer> caches, HashMap<String,Integer> sp, GregorianCalendar dCreat, GregorianCalendar endInsc, GregorianCalendar inEvent, GregorianCalendar endEvent, String desc, int dific, ArrayList<String> p    ){
        this.id=daIDEvento();
        this.nome=n;
        this.local=l.clone();
        this.raio=r; //ver melhor isto
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.addAll(caches);
        this.conj = res1;
        this.userProg = new HashMap<String,HashSet<Integer>>();
        HashMap<String,Integer> res2 = new HashMap<String,Integer>();
        Iterator<String> it = sp.keySet().iterator();
        while(it.hasNext()){
            String n2 = it.next();
            res2.put(n2,sp.get(n2));
        }
        this.startProg = res2;
        this.dCreat = (GregorianCalendar)dCreat.clone();
        this.endInsc = (GregorianCalendar)endInsc.clone();
        this.inEvent = (GregorianCalendar)inEvent.clone();
        this.endEvent = (GregorianCalendar)endEvent.clone();
        this.desc = desc;
        this.dific = dific;
        this.partic = partic;
        this.maxpartic = maxD;
        ArrayList<String> res3 = new ArrayList<String>();
        res3.addAll(p);
        this.pistas =  res3;
    }

    /**
     * Construtor copia para objectos da classe Evento
     */
    public Evento(Evento ev){
        this.id=ev.getId();
        this.nome=ev.getNome();
        this.local=ev.getLocal();
        this.raio=ev.getRaio(); 
        this.conj = ev.getConj();
        this.userProg = ev.getUserProg();
        this.startProg = ev.getStartProg();
        this.dCreat = ev.getDCreat();
        this.endInsc = ev.getEndEvent();
        this.inEvent = ev.getInEvent();;
        this.endEvent = ev.getEndEvent();
        this.desc = ev.getDesc();
        this.dific = ev.getDific();
        this.partic = ev.getPartic();
        this.maxpartic = ev.getMaxpartic();
        this.pistas = ev.getPistas();  
    }

    /**
     * getId
     * @return id do evento
     */
    public int getId(){return this.id;}
    /**
     * getNome
     * @return nome do evento
     */
    public String getNome(){return this.nome;}
    /**
     * getLocal
     * @return local onde o evento vai ser realizado
     */    
    public Local getLocal(){return this.local.clone();}
    /**
     * getRaio
     * @return o raio em que vai ser realizado o evento 
     */
    public double getRaio(){return this.raio;}
    /**
     * getConj
     * @return lista de todas as caches que pertencem ao evento
     */
    public ArrayList<Integer> getConj(){ArrayList<Integer> res = new ArrayList<Integer>(); res.addAll(this.conj); return res;}
    /**
     * getUserProg
     * @return o progresso de cada utilizador no evento
     */     
    public HashMap<String,HashSet<Integer>> getUserProg(){
        HashMap<String,HashSet<Integer>> res = new HashMap<String,HashSet<Integer>>();
        Iterator<String> itKey = this.userProg.keySet().iterator();
        while(itKey.hasNext()){
            String n = itKey.next();
            HashSet<Integer> resSet = new HashSet<Integer>();
            resSet.addAll(this.userProg.get(n));
            res.put(n,resSet);
        }
        return res;
    }
    /**
     * GETTERS
     */
    public HashMap<String,Integer> getStartProg(){
        HashMap<String,Integer> res = new HashMap<String,Integer>();
        Iterator<String> it = this.startProg.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            res.put(n,this.startProg.get(n));
        }
        return res;
    }
    /**
     * GETTERS
     */
    public GregorianCalendar getDCreat(){return (GregorianCalendar)this.dCreat.clone(); }
    /**
     * GETTERS
     */
    public GregorianCalendar getEndInsc(){return (GregorianCalendar)this.endInsc.clone();}
    /**
     * GETTERS
     */
    public GregorianCalendar getInEvent(){return (GregorianCalendar)this.inEvent.clone();}
    /**
     * GETTERS
     */
    public GregorianCalendar getEndEvent(){return (GregorianCalendar)this.endEvent.clone();}
    /**
     * GETTERS
     */
    public String getDesc(){return this.desc;}
    /**
     * GETTERS
     */
    public int getPartic(){return this.partic;}
    /**
     * GETTERS
     */
    public int getMaxpartic(){return this.maxpartic;}
    /**
     * GETTERS
     */
    public int getDific(){return this.dific;}
    /**
     * GETTERS
     */
    public ArrayList<String> getPistas(){
        ArrayList<String> res = new ArrayList<String>();
        res.addAll(this.pistas);
        return res;
    }


    /**
     * SETTERS
     */ 
    public void set(int id){this.id=id;}
    /**
     * SETTERS
     */    
    public void set(String nome){this.nome=nome;}
    /**
     * SETTERS
     */
    public void set(Local local){this.local=local.clone();}
    /**
     * SETTERS
     */
    public void set(double raio){this.raio=raio;}
    /**
     * SETTERS
     */
    public void set(ArrayList<Integer> conj){
        ArrayList<Integer> c = new ArrayList<Integer>();
        c.addAll(conj);
        this.conj=c;
    }
    /**
     * SETTERS
     */
    public void set(HashMap<String,HashSet<Integer>> userProg){
        HashMap<String,HashSet<Integer>> res = new HashMap<String,HashSet<Integer>>();
        Iterator<String> itKey = userProg.keySet().iterator();
        while(itKey.hasNext()){
            String n = itKey.next();
            HashSet<Integer> resSet = new HashSet<Integer>();
            resSet.addAll(userProg.get(n));
            res.put(n,resSet);
        }
        this.userProg = res;
    }
    /**
     * SETTERS
     */
    public void setStartProg(HashMap<String,Integer> startProg){
        HashMap<String,Integer> res = new HashMap<String,Integer>();
        Iterator<String> it = startProg.keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            res.put(n,this.startProg.get(n));
        }
        this.startProg = res;
    }
    /**
     * SETTERS
     */
    public void setDCreat(GregorianCalendar dCreat){this.dCreat = (GregorianCalendar)dCreat.clone();}
    /**
     * SETTERS
     */
    public void setEndInsc(GregorianCalendar endInsc){this.endInsc = (GregorianCalendar)endInsc.clone();}
    /**
     * SETTERS
     */
    public void setInEnvent(GregorianCalendar inEnvent){this.inEvent = (GregorianCalendar)inEnvent.clone();}
    /**
     * SETTERS
     */
    public void setEndEvent(GregorianCalendar endEvent){this.endEvent = (GregorianCalendar)endEvent.clone();}
    /**
     * SETTERS
     */
    public void setdesc(String desc){this.desc=desc;}
    /**
     * SETTERS
     */
    public void setPartic(int partic){this.partic=partic;}
    /**
     * SETTERS
     */
    public void setMaxpartic(int maxpartic){this.maxpartic=maxpartic;}
    /**
     * SETTERS
     */
    public void setDific(int dific){this.dific=dific;}
    /**
     * SETTERS
     */
    public void setPistas(ArrayList<String> pistas){
        ArrayList<String> res = new ArrayList<String>();
        res.addAll(pistas);
        this.pistas=res;
    }
    
    public boolean addConj(ArrayList<Integer> list){
        boolean res = true;
        for(int i =0; res && i<list.size();i++){
            Integer c = list.get(i);
            if(!(this.conj.contains(c))){
                res = this.conj.add(c);
            }
        }
        return res;
    }
    public boolean addCacheConj(Integer c){
        boolean res = true;
        if(!(this.conj.contains(c))){
            res = this.conj.add(c);
        }
        return res;
    }
    public boolean removeCacheConj(Integer c){
        boolean res = false;
        if(this.conj.contains(c)){
            res = this.conj.remove(c);
        }
        return res;
    }
    public boolean addCacheUtl(String m, Integer c){
        boolean res = true;
        if(!(this.userProg.get(m).contains(c))){ res = this.userProg.get(m).add(c);}
        return res;
    }
    
    public boolean removeCacheUtl(String m, Integer c){
        boolean res = false;
        if(this.userProg.get(m).contains(c)){ res = this.userProg.get(m).remove(c);}
        return res;
    }
    
    public ArrayList<String> encCache(Integer d){
        ArrayList<String> res = new ArrayList<String>();
        Iterator<String> it = this.getUserProg().keySet().iterator();
        while(it.hasNext()){
            String n = it.next();
            if(this.getUserProg().get(n).contains(d)){ res.add(n);}
        }
        return res;
    }
    
    public void atualizarSU(String utl, Integer pont){
        if(this.startProg.containsKey(utl)){
            if(!(this.startProg.get(utl).equals(pont))){
                this.startProg.remove(utl);
                this.startProg.put(utl,pont);
            }
        }
        else{
            this.startProg.put(utl,pont);
        }  
    }
    public void atualizarLSU(HashMap<String,Integer> lista){
        Iterator<String> it = lista.keySet().iterator();
        while(it.hasNext()){
            String utl = it.next();
            Integer pont = lista.get(utl);
            if(this.startProg.containsKey(utl)){
                if(!(this.startProg.get(utl).equals(pont))){
                    this.startProg.remove(utl);
                    this.startProg.put(utl,pont);
                }
            }
            else{
                this.startProg.put(utl,pont);
            }  
        }
    }
    
    public int nUserProg(){return this.getUserProg().size();}
    public int nConj(){return this.getConj().size();}
    public int nStartUsers(){return this.getStartProg().size();}
    public int nPistas(){return this.getPistas().size();}
   
    /**
     * Metodo toString
     *
     * @return  String   
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Cache: " + this.getId() + " - " + this.getNome() + "\n");
        s.append(this.getLocal().toString());
        s.append("Raio de procura " + this.getRaio() + "\n");
        s.append("Data de creação: < Ano: " +  this.getDCreat().get(Calendar.YEAR) + " Mes: " + (this.getDCreat().get(Calendar.MONTH) + 1 )+ " Dia: " + (this.getDCreat().get(Calendar.DAY_OF_MONTH) + 1) + " >\n" );
        s.append("Data de fim de inscrições: < Ano: " +  this.getEndInsc().get(Calendar.YEAR) + " Mes: " + (this.getEndInsc().get(Calendar.MONTH) + 1 )+ " Dia: " + (this.getEndInsc().get(Calendar.DAY_OF_MONTH) + 1) + " >\n" );
        s.append("Data de inicio do evento: < Ano: " +  this.getInEvent().get(Calendar.YEAR) + " Mes: " + (this.getInEvent().get(Calendar.MONTH) + 1 )+ " Dia: " + (this.getInEvent().get(Calendar.DAY_OF_MONTH) + 1) + " >\n" );
        s.append("Data de fim do evento: < Ano: " +  this.getEndEvent().get(Calendar.YEAR) + " Mes: " + (this.getEndEvent().get(Calendar.MONTH) + 1 )+ " Dia: " + (this.getEndEvent().get(Calendar.DAY_OF_MONTH) + 1) + " >\n" );
        s.append("Nº de participantes: "+ this.getPartic() + " (Max="+ getMaxpartic() +")" +"Dificuldade: " + this.getDific() + "\n");
        s.append("Descrição:\n" + this.getDesc().toString() + "\n");
        s.append("Pistas: \n");
        for(String p : this.getPistas()){
            s.append("-> " + p + ";\n");
        }
        s.append("Lista de caches do Evento:\n");
        for(Integer c : this.getConj()){s.append(c.toString());}
        s.append("Progresso dos Utilizadores:\n");
        Iterator<String> it = this.getUserProg().keySet().iterator();
        while(it.hasNext()){
            String n1 = it.next();
            s.append(n1+"\n");
            for(Integer c : this.getUserProg().get(n1)){s.append(c.toString());}
        }
        s.append("Progresso Inicial:\n");
        Iterator<String> it1 = this.getStartProg().keySet().iterator();
        while(it1.hasNext()){
            String n2 = it.next();
            s.append( n2 + this.getUserProg().get(n2).toString() + "\n");
        }
        return s.toString();
    }
    
    /**
     * Metodo clone
     *
     * @return     uma copia 
     */
    public Evento clone(){return(new Evento(this));}

    /**
     *  Metodo equals
     *  
     * @param       objecto que vai compra
     * @return      resultado da comprarçao dos dois objectos  
     */
    
    public boolean equals(Object obj){
        boolean res = true;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Evento c = (Evento)obj;
        if( !(this.getId()==c.getId()                   && this.getNome().equals(c.getNome())  
         && this.getLocal().equals(c.getLocal())        && this.getPartic()==c.getPartic()
         && this.getRaio()==c.getRaio()                 && this.getDesc().equals(c.getDesc()) 
         && this.getMaxpartic()==c.getMaxpartic()       && this.getDific()==c.getDific()
         && this.nPistas()==c.nPistas()                 && this.nUserProg()==c.nUserProg()
         && this.nConj()==c.nConj()                     && this.nStartUsers()==c.nStartUsers())) return false;
        if(!(this.getConj().containsAll(c.getConj()))) return false;
        if(!(this.getPistas().containsAll(c.getPistas()))) return false;
        Iterator<String> itKeyT = this.getUserProg().keySet().iterator();
        Iterator<String> itKeyO = c.getUserProg().keySet().iterator();
        while( res && itKeyT.hasNext() && itKeyO.hasNext()){
            String uT = itKeyT.next();
            String uO = itKeyO.next();
            if(!(uT.equals(uO))) return false;
            Iterator<Integer> itValeusT = this.getUserProg().get(uT).iterator();
            Iterator<Integer> itValeusO = c.getUserProg().get(uO).iterator();
            while( res && itValeusT.hasNext() && itValeusO.hasNext()){
                res = itValeusT.next().equals(itValeusO);
            }
        }
        Iterator<String> itKey1 = this.getUserProg().keySet().iterator();
        Iterator<String> itKey2 = c.getStartProg().keySet().iterator();
        while( res && itKey1.hasNext() && itKey2.hasNext()){
            res = this.getStartProg().get(itKey1.next()).equals(c.getStartProg().get(itKey2.next()));
        }
        return res;
    }
    
    
    /**
     * Metodo hashCode
     *
     * @return     valor de hash
     */
    public int hashCode(){return id;}
    
    
}