
/**
 * Write a description of class Item here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Item
{
     // instance variables
    private String nome;
    private int valor; // 1..5
    

    /**
     * Constructors for objects of class Item
     */
    
    public Item(){
        this.nome= new String();
        this.valor=0;
    }
    public Item(String nome, int valor){
        this.nome= nome;
        this.valor=valor;
    }
    public Item(Item t){
        this.nome=t.getNome();
        this.valor=t.getValor();
    }
    
    /**
     * GETTERS
     */
    public String getNome(){return this.nome;}
    public int getValor(){return this.valor;}
    
    /**
     * SETTERS
     */
    public void setNome(String nome){this.nome=nome;}
    public void setValor(int valor){this.valor=valor;}
    
    /**
     * Metodo toString
     *
     * @return  String     
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append( "Item-> Nome: " + this.getNome() + "; Valor: " + this.getValor() + "\n");
        return s.toString();
    }
    
    /**
     * Metodo clone
     *
     * @return     uma copia 
     */
    public Item clone(){return(new Item(this));}
    
    /**
     * Metodo hashCode
     *
     * @return     valor de hash
     */
    public int hashCode(){return this.nome.hashCode();}
    
    /**
     *  Metodo equals
     *  
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Item v = (Item)obj;
        return ( (this.getNome().equals(v.getNome()))  &&  (this.getValor()==v.getValor() ));
    
    }
}
