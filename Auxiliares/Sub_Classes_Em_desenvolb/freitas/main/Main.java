
/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Scanner;
import java.util.ArrayList;
public class Main
{
    // instance variables - replace the example below with your own
    private static GeocachingPOO app=new GeocachingPOO() ;
    private static Scanner input = new Scanner(System.in);
    private static String lixo;
    private static String onLogin;
    private static int premi;
    /**
     * MAIN
     */
    
    public static void userDetail(){
        String mail;
        System.out.println("Qual o utilizador a consultar detalhes:");
        System.out.print(">>");
        mail = input.next();
        if(app.jaExisteUser(mail)){
            System.out.println("DETALHES--------------------------------");
            System.out.println(app.userDeatils(mail));
            System.out.println("----------------------------------------");
        }else{
            System.out.println("O utilizador " + mail + " não existe na APP");
        }
    }
    
    public static void removeEvento(){
        Integer id;
        System.out.println("Qual o evento a remover?");
        System.out.print(">>");
        id = input.nextInt();
        if(app.existeEvento(id)){
            app.removeEvento(id);
            System.out.println("Evento " + id +" removido com sucesso.");
        }else{
          System.out.println("Evento NÂO removida:\nAO evento com o ID " + id + " não se encontra na APP");  
        }
    }
    
    public static void imprimirEventos(){
        ArrayList<Integer> novo= app.getEventos();
        System.out.println("Existem na APP " + novo.size() + " Eventos.");
        for(Integer s : novo){
            System.out.println(s);
        }
        
    }
    
    public static void removeClean(){
        Integer id;
        System.out.println("Qual a cache a remover?");
        System.out.print(">>");
        id = input.nextInt();
        if(app.existeCache(id)){
            app.forceRemoveCache(id);
            System.out.println("Cache " + id +" removida com sucesso.");
        }else{
          System.out.println("Cache NÂO removida:\nA cache com o ID " + id + " não se encontra na APP");  
        }
    }
    
    public static void imprimirCaches(){
        ArrayList<Integer> novo= app.getCaches();
        System.out.println("Existem na APP " + novo.size() + " cahes ativas.");
        for(Integer s : novo){
            System.out.println(s);
        }
        
    }
    
            
   
    public static void reportClean(){
        Integer id;
        System.out.println("Qual a cache a fazer reset?");
        System.out.print(">>");
        id = input.nextInt();
        if(app.estanoReport(id)){
            app.limpacacheRp(id);
            System.out.println("Cache " + id +" removida com sucesso.");
        }else{
          System.out.println("Cache NÂO removida:\nA cache com o ID " + id + " não se encontra no report abuse");  
        }
    }
    
    public static void resetReport(){
        Integer id;
        System.out.println("Qual a cache a fazer reset?");
        System.out.print(">>");
        id = input.nextInt();
        if(app.estanoReport(id)){
            app.resetReport(id);
            System.out.println("Reset a cache " + id +" com sucesso.");
        }else{
          System.out.println("Cache NÂO removida:\nA cache com o ID " + id + " não se encontra no report abuse");  
        }
        
    }
    
    public static void reportCleanALL(){
        if(app.semReport()){
            System.out.println("Não existem caches no Report Abuse.");
        }else{
            if(app.existeALimpar()){
                System.out.println("A limpar \"Report Abuse\"...");
                int v = app.limpaReportALL();
                System.out.println("Foram limpas do \"Report Abuse\" " + v + " Caches");
            }
            else{
                System.out.println("Não existem caches a limpar do Report abuse.");
            }
        }
    }
    public static void alteraPassUser(){
        String pass1;
        String pass2;
        String passadmin;
        
        String mail;
        System.out.println("Qual o utilizador a altera a pass:");
        System.out.print(">>");
        mail = input.next();
        if(app.jaExisteUser(mail)==false){
            System.out.println("o utilizador " + mail + " nao existe na APP");
        }else{
            System.out.println("Diga a nova password:");
            System.out.print(">>");
            pass1 = input.next();
            System.out.println("Repita a nova password:");
            System.out.print(">>");
            pass2 = input.next();
            while(pass1.equals(pass2)==false){
                System.out.println("ERRO as passwords não são iguais.");
                System.out.println("Diga a nova password:");
                System.out.print(">>");
                pass1 = input.next();
                System.out.println("Repita a nova password:");
                System.out.print(">>");
                pass2 = input.next();  
            }
            if(app.setUserPASS(mail,pass1)){
                System.out.println("A pass do utilizador" + mail + " alterada com sucesso para " + pass1);
            }
            else{
                System.out.println("Impossivel altear a passe o utilizador " + mail);
            }
        }
    }
    
    public static void alteraMPassUser(){
        String mail=onLogin;
        String pass;
        String pass1;
        String pass2;
        String passadmin;
        int i=0;
        
        passadmin=app.getPassDe(mail);
        System.out.println("Diga a sua password:");
        System.out.print(">>");
        pass = input.next();
        while(i<2 && passadmin.equals(pass)==false){
            System.out.println("Pass errada ( restam "+(2-i) +" tentativas ) diga de novo:");
            System.out.print(">>");
            pass = input.next();
            i++;
        }
        if(i==2){
               System.out.println("ERRO pass incorreta " + (i+1)+ " vezes."); 
               return;
        }
        System.out.println("Diga a nova password:");
        System.out.print(">>");
        pass1 = input.next();
        System.out.println("Repita a nova password:");
        System.out.print(">>");
        pass2 = input.next();
        while(pass1.equals(pass2)==false){
            System.out.println("ERRO as passwords não são iguais.");
            System.out.println("Diga a nova password:");
            System.out.print(">>");
            pass1 = input.next();
            System.out.println("Repita a nova password:");
            System.out.print(">>");
            pass2 = input.next();  
        }
            
        if(app.setUserPASS(mail,pass1)){
            System.out.println("A Sua nova password alterada com sucesso");
        }
        else{
            System.out.println("ERRO não foi possivel alterar a pass");
        }
        
    }
    
    public static void alteraMPassAdmin(){
        String mail=onLogin;
        String pass;
        String pass1;
        String pass2;
        String passadmin;
        int i=0;
        
        passadmin=app.getPassDe(mail);
        System.out.println("Diga a sua password:");
        System.out.print(">>");
        pass = input.next();
        while(i<2 && passadmin.equals(pass)==false){
            System.out.println("Pass errada ( restam "+(2-i) +" tentativas ) diga de novo:");
            System.out.print(">>");
            pass = input.next();
            i++;
        }
        if(i==2){
               System.out.println("ERRO pass incorreta " + (i+1)+ " vezes."); 
               return;
        }
        System.out.println("Diga a nova password:");
        System.out.print(">>");
        pass1 = input.next();
        System.out.println("Repita a nova password:");
        System.out.print(">>");
        pass2 = input.next();
        while(pass1.equals(pass2)==false){
            System.out.println("ERRO as passwords não são iguais.");
            System.out.println("Diga a nova password:");
            System.out.print(">>");
            pass1 = input.next();
            System.out.println("Repita a nova password:");
            System.out.print(">>");
            pass2 = input.next();  
        }
            
        if(app.setadminPASS(mail,pass1)){
            System.out.println("A Sua nova password alterada com sucesso");
        }
        else{
            System.out.println("ERRO não foi possivel alterar a pass");
        }
        
    }
    public static void imprimirUsers(){
        ArrayList<String> novo= app.getUsers();
        System.out.println("Existem na APP " + novo.size() + " utilizadores.");
        for(String s : novo){
            System.out.println(s);
        }
        
    }
    public static void removeUser(){
        String mail;
        System.out.println("Qual o utilizador a remover:");
        System.out.print(">>");
        mail = input.next();
        if(app.jaExisteUser(mail)==false){
            System.out.println("o utilizador " + mail + " nao existe na APP");
        }else{
            if(app.removeUser(mail)){
                System.out.println("o utilizador " + mail + " removdo com sucesso da APP");
            }
            else{
                System.out.println("ERRO o utilizador " + mail + " nao foi removido da APP");
            }
        }
        
    }
    
     public static void removeAdmin(){
        String mail;
        String pass;
        String passadmin;
        int i=0;
        System.out.println("Qual o ADMIN a remover:");
        System.out.print(">>");
        mail = input.next();
        if(app.jaExisteAdmin(mail)==false){
            System.out.println("o Admin " + mail + " nao existe na APP");
        }else{
            if(mail.equals(onLogin)){
               System.out.println("ERRO nao se pode remover a si mesmo");
               return;
            }
            passadmin=app.getPassDe(mail);
            System.out.println("Diga o Pass do " + mail + " :");
            System.out.print(">>");
            pass = input.next();
            while(i<2 && passadmin.equals(pass)==false){
                System.out.println("Pass ( restam "+(2-i) +" tentativas ) errada diga de novo:");
                System.out.print(">>");
                pass = input.next();
                i++;
            }
            if(i==2){
               System.out.println("ERRO pass incorreta " + i +1+ " vezes, Admin não removido"); 
               return;
            }
            if(app.removeAdmin(mail)){
                System.out.println("o Admin " + mail + " removdo com sucesso da APP");
            }
            else{
                System.out.println("ERRO o Admin " + mail + " nao foi removido da APP");
            }
        }
        
    }
    public static void registo(){
        String mail;
        String pass1;
        String pass2;
        String nome;
        char genero;
        int ano;
        int mes;
        int dia;
        String pais;
        String cidade;
        String rua;
        String cp;
        
        //ver maioridade
        
        System.out.println("----------GeocachingPOO----------");
        System.out.println("-----------Menu Registo----------");
        System.out.println("Diga email para registo:");
        System.out.print(">>");
        mail = input.next();
        while(app.jaExisteIDuseradmin(mail)){
           System.out.println("ERRO id do já existe, Impossivel Criar");
           System.out.println("Diga outro email");
           System.out.print(">>");
           mail = input.next();
           
        }
        System.out.println("Diga password:");
        System.out.print(">>");
        pass1 = input.next();
        System.out.println("Repita password:");
        System.out.print(">>");
        pass2 = input.next();
        while(pass1.equals(pass2)==false){
            System.out.println("ERRO as passwords não são iguais.");
            System.out.println("Diga password:");
            System.out.print(">>");
            pass1 = input.next();
            System.out.println("Repita password:");
            System.out.print(">>");
            pass2 = input.next();  
        }
        System.out.println("Diga o nome:");
        System.out.print(">>");
        nome = input.next();
        System.out.println("Diga o Genero (M,F,O):");
        System.out.print(">>");
        genero=input.next().charAt(0);
        while(!(genero == 'M' || genero == 'F' || genero == 'O')){
           System.out.println("Genero Invalido Diga de novo:");
           System.out.print(">>");
           genero=input.next().charAt(0); 
        }
        System.out.println("Diga o ano de nascimento >1900:");
        System.out.print(">>");
        ano=input.nextInt();
        while(ano<=1900){
            System.out.println("Ano invalido Diga ne novo.");
            System.out.print(">>");
            ano=input.nextInt();   
        }
        System.out.println("Diga o mes de nascimento (1..12):");
        System.out.print(">>");
        mes=input.nextInt();
        while(mes<1 || mes>12){
            System.out.println("Mes Invalido Diga ne novo.");
            System.out.print(">>");
            mes=input.nextInt();   
        }
        
        System.out.println("Diga o dia de nascimento (1..31):");
        System.out.print(">>");
        dia=input.nextInt();
        while(dia<1 || dia>31){
            System.out.println("Dia Invalido Diga ne novo.");
            System.out.print(">>");
            mes=input.nextInt();   
        }
        if(dia == 31 && (mes==4 ||mes==6 ||mes==9 ||mes==11 )) dia =30;
        if(dia>28 && mes == 2) dia =28; 
        
        System.out.println("Diga o seu Pais:");
        System.out.print(">>");
        pais = input.next();
        
        System.out.println("Diga a sua Cidade:");
        System.out.print(">>");
        cidade = input.next();
        
        System.out.println("Diga a sua Rua:");
        System.out.print(">>");
        rua = input.next();
        
        System.out.println("Diga o seu Codigo Postal:");
        System.out.print(">>");
        cp = input.next();
        
        app.registaUser(mail,pass1,nome,genero,ano,mes,dia,pais,cidade,rua,cp);

        
        System.out.println( mail + "registado com Sucesso.");
    }
    
    
    
    private static void menuAdmin(){
        int op=1;
        while(op>0 && op<19){
            System.out.println("----------GeocachingPOO----------");
            System.out.println("-----------Menu ADMIN----------");
            System.out.println("1-Criar novo admin; (Done)");
            System.out.println("2-Remover Admin;(Done)");
            System.out.println("3-Remover Utilizador;(Done)");
            System.out.println("4-Consultar Utilizadores(Done);");
            System.out.println("21-Consultar Utilizador detalhe (Done);");
            System.out.println("5-Alterar password Utilizador(Done);");
            System.out.println("6-Consultar Caches;(done)");
            System.out.println("X-Consultar Caches detalhe(FALTA);");
            System.out.println("7-Esconder Cache;");
            System.out.println("8-Remover Cahe(Done);");
            System.out.println("9-Atualizar Cache;");
            System.out.println("10-Executar Limpeza ao ReporAbuse;(Done)");
            System.out.println("11-Limpar Cache expecifica do ReporAbuse(Done);");
            System.out.println("12-Retirar cache do ReporAbuse;(done)");
            System.out.println("13-Consultar Eventos(Done);");
            System.out.println("X-Consultar Eventos detalhe;");
            System.out.println("14-Criar Evento;");
            System.out.println("15-Eliminar Evento(DONE) so remove nao elemina ifo dos users");
            System.out.println("16-Alterar Evento;");
            System.out.println("17-Simular Evento");
            System.out.println("18-Guardar Estado;");
            System.out.println("19-Carregar Estado");
            System.out.println("20-Alterar Password(Done)");
            System.out.println("Outro - Sair");
            System.out.print(">>");
            op= input.nextInt();
            switch (op){
                case 1: regAdmin(); break;
                case 2: removeAdmin(); break;
                case 3: removeUser(); break;
                case 4: imprimirUsers(); break;
                case 5: alteraPassUser(); break;
                case 6: imprimirCaches(); break;
                case 7: System.out.println("op7"); break;
                case 8: removeClean(); break;
                case 9: System.out.println("op9"); break;
                case 10: reportCleanALL(); break;
                case 11: reportClean(); break;
                case 12: resetReport(); break;
                case 13: imprimirEventos(); break;
                case 14: System.out.println("op14"); break;
                case 15: removeEvento(); break;
                case 16: System.out.println("op16"); break;
                case 17: System.out.println("op17"); break;
                case 18: System.out.println("op18"); break;
                case 19: System.out.println("op19"); break;
                case 20: alteraMPassAdmin(); break;
                case 21: userDetail(); break;
            }
            if(op>0 && op<21){
               System.out.println("Insira algo para continuar"); 
               lixo=input.next();
            }
        }
        app.setModo(0);
        premi=0;
        onLogin ="";
    }
    
    private static void menuUser(){
        System.out.println("----------GeocachingPOO----------");
        System.out.println("-----------Menu USER----------");
    }
    public static void login(){
        String mail;
        String pass;
        String verif;
        int i=0;
        System.out.println("----------GeocachingPOO----------");
        System.out.println("-----------Login------------");
        System.out.println("Diga o Email:");
        System.out.print(">>");
        mail = input.next();
        if(app.jaExisteIDuseradmin(mail)==false){
            System.out.println("ERRO não Exite registo do utilizador: " + mail);
            return;
        }
        verif = app.getPassDe(mail);
        System.out.println("Diga o Pass:");
        System.out.print(">>");
        pass = input.next();
        while(i<2 && pass.equals(verif)==false){
           System.out.println("Pass errada diga de novo:");
           System.out.print(">>");
           pass = input.next();
           i++;
        }
        if(i==2){
            System.out.println("Limite de tentativas de login (3).");
            return;
        }
        System.out.println("Login Com sucesso.");
        onLogin=mail;
        if(app.jaExisteAdmin(mail)){
            System.out.println("Encontrasse em modo ADMIN.");
            app.setModo(1);
            premi=1;
            menuAdmin();
        }else{
            System.out.println("Encontrasse em modo Utilizador.");
            app.setModo(0);
            premi=0;
            menuUser();
        }
        
        
    }
    
    public static void regAdmin(){
        String mail;
        String pass1;
        String pass2;
        String nome;
        System.out.println("----------GeocachingPOO----------");
        System.out.println("-------Menu Registo ADMIN-------");
        System.out.println("Diga email para registo:");
        System.out.print(">>");
        mail = input.next();
        while(app.jaExisteIDuseradmin(mail)){
           System.out.println("ERRO id do já existe, Impossivel Criar");
           System.out.println("Diga outro email");
           System.out.print(">>");
           mail = input.next();
           
        }
        System.out.println("Diga password:");
        System.out.print(">>");
        pass1 = input.next();
        System.out.println("Repita password:");
        System.out.print(">>");
        pass2 = input.next();
        while(pass1.equals(pass2)==false){
            System.out.println("ERRO as passwords não são iguais.");
            System.out.println("Diga password:");
            System.out.print(">>");
            pass1 = input.next();
            System.out.println("Repita password:");
            System.out.print(">>");
            pass2 = input.next();  
        }
        System.out.println("Diga o nome:");
        System.out.print(">>");
        nome = input.next();
        app.registaAdmin(mail,pass1,nome);
        System.out.println("Admin registado com Sucesso.");
    }
    public static void main (String [] args){
        int op=1;
        while(op==1 || op== 2){
            if(app.hasAdmin()){
                System.out.println("----------GeocachingPOO----------");
                System.out.println("1 - Registar Utilizador");
                System.out.println("2 - Login");
                System.out.println("Outro - Sair");
                System.out.println("---------------------------------");
                System.out.print(">>");
                op= input.nextInt();
                if(op==1)registo();
                if(op==2)login();
            }else{
                System.out.println("----------GeocachingPOO----------");
                System.out.println("1 - Registar Admin");
                System.out.println("Outro - Sair");
                System.out.println("---------------------------------");
                System.out.print(">>");
                op= input.nextInt();
                if(op==1)regAdmin();
                else op=3;
            }
            
        }
        System.out.println("ADEUS!!");
        // put your code here
        
    }

    public void registoCache_Multi(){
        int laco; double lamin; char laor;int lco;double lmin; char lor; double alt;
        int ano;int mes;int dia;
        String nome;
        String desc;
        int dM;int dF;int pont;
        String codeV;
        ArrayList<String> nItens = new ArrayList<String>();
        ArrayList<Integer> valorItem = new ArrayList<Integer>();
        ArrayList<Integer> idCaches = new ArrayList<Integer>();
        
        
        System.out.println("----------Multi-Cache----------");
        System.out.println("---------------------------------");
        System.out.println("Diga as Cordenadas:");
        System.out.print(">>");
        laco= input.nextInt();
        System.out.println("Diga o ano de nascimento >1900:");
        System.out.print(">>");
        ano=input.nextInt();
        while(ano<=1900){
            System.out.println("Ano invalido Diga ne novo.");
            System.out.print(">>");
            ano=input.nextInt();   
        }
        System.out.println("Diga o mes de nascimento (1..12):");
        System.out.print(">>");
        mes=input.nextInt();
        while(mes<1 || mes>12){
            System.out.println("Mes Invalido Diga ne novo.");
            System.out.print(">>");
            mes=input.nextInt();   
        }
        System.out.println("Diga o dia de nascimento (1..31):");
        System.out.print(">>");
        dia=input.nextInt();
        while(dia<1 || dia>31){
            System.out.println("Dia Invalido Diga ne novo.");
            System.out.print(">>");
            mes=input.nextInt();   
        }
        System.out.println("Diga o nome da Cache:");
        System.out.print(">>");
        nome=input.next();
        System.out.println("Diga o nome da Cache:");
        System.out.print(">>");
        nome=input.next();
        
        
        //registoCache_MultiPOO(laco,lamin,laor,lco,lmin,lor,alt,ano,mes,dia,nome,);
         
    
    }
    
    public static Pergunta criaPergunta(){
        String perg = "";
        String op1= "";
        String op2="";
        int correcta;
        String cor;
        int pont;
        
        System.out.println("----------Criar Pergunta----------");
        System.out.println("---------------------------------");
        System.out.println("Diga a Pergunta:");
        System.out.print(">>");
        perg=input.next();
        System.out.println("Opção de resposta 1:");
        System.out.print(">>");
        op1=input.next();
        System.out.println("Opção de resposta 2:");
        System.out.print(">>");
        op2=input.next();
        System.out.println("Qual a opção correcta?");
        System.out.print(">>");
        correcta=input.nextInt();
        while(correcta<1 || correcta>2){
            System.out.println("Opçao incorrecta:(insira outra vez)");
            System.out.print(">>");
            correcta=input.nextInt();   
        }
        if(correcta==1) {cor=op1;}
        else {cor=op2;}
        System.out.println("Qual a pontuação da pergunta?");
        System.out.print(">>");
        pont=input.nextInt();
        Pergunta p = new Pergunta(perg,op1,op2,cor,pont);
        
        System.out.println(p.toString() + "\n" + p.getCorrecta() + "\n");
        
        
        
        return p;
    }
    
    public static Puzzel criaPuzzel(){
        String nome;
        int valor;
        int nPerg;
        
        System.out.println("----------Criar Puzzel----------");
        System.out.println("---------------------------------");
        System.out.println("Diga o nome do Puzzel");
        System.out.print(">>");
        nome=input.next();
        System.out.println("Diga o nome o valor do Puzzel");
        System.out.print(">>");
        valor=input.nextInt();
        while(valor<1 || valor>100){
            System.out.println("Opçao incorrecta:(insira outra vez)");
            System.out.print(">>");
            valor=input.nextInt();   
        }
        Puzzel p = new Puzzel(nome,valor);
        System.out.println("Diga quantas perguntas quer criar:");
        System.out.print(">>");
        nPerg=input.nextInt();
        while(nPerg<1 || nPerg>10){
            System.out.println("Opçao incorrecta:(insira outra vez)");
            System.out.print(">>");
            nPerg=input.nextInt();   
        }
        for(int i=0; i<nPerg ; i++){
            p.addPergunta(criaPergunta());
        }
        
        return p;
    }
    
    public static Cordenada criarCoordenada(){
        String cord = "";
        System.out.println("Diga o nome do Puzzel");
        System.out.print(">>");
        cord=input.next();
        return new Cordenada();
    }
}
