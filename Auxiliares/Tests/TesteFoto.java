

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TesteFoto.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class TesteFoto
{
    /**
     * Default constructor for test class TesteFoto
     */
    public TesteFoto()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void Foto1(){
        Foto f1 = new Foto();
        Foto f2 = new Foto("Rui",5,"ola.txt","o rui é o maior");
        assertEquals("Rui",f2.getNome());
        assertEquals(5,f2.getValor());
        assertEquals("ola.txt",f2.getUrl());
        assertEquals("o rui é o maior",f2.getDesc());
        
        f2.setNome("rui");
        f2.setValor(2);
        f2.setUrl("txt");
        f2.setDesc("tudo bem");
        
        assertEquals("rui",f2.getNome());
        assertEquals(2,f2.getValor());
        assertEquals("txt",f2.getUrl());
        assertEquals("tudo bem",f2.getDesc());
    }
    
    
    
    
    
    
    
    
    
    
    
}
