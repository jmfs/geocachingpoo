
/**
 * Write a description of class GocachingPOO here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.GregorianCalendar;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Calendar;
import static java.lang.Math.abs;
public class GeocachingPOO
{
    private GregorianCalendar relogProg;
    private TreeMap<String,DefaultUser> users; // lista de todos os utlizadores da aplicaçao
    private TreeMap<Integer,Evento> events; // tem os eventos(atividades) que o utilizador cria
    private TreeMap<Integer,Integer> reportAbuse; //idcache -> numero de reportabuse
    private TreeMap<Integer,Cache> caches; //contem todas as caches exixtentes no momento
    private TreeMap<String,Admin> admins;
    private int modo; //0 user 1 admin
    private int usersRegist=0;
    private String onLogin;
    // instance variables - replace the example below with your own
    private static final int rp=5;
    
    //So esta aqui para teste


    /**
    * @param String mail
    *adiciona Friend, atraves da String mail
    */
    
    public void addFriend(String mail){
        this.users.get(onLogin).addFriends(mail);
        this.users.get(mail).addFriends(onLogin);
    }
    
    /**
    * @param String mail
    * remove Friend, atraves da String mail
    */

    public void rmoveFriend(String mail){
        this.users.get(onLogin).removeFriends(mail);
        this.users.get(mail).removeFriends(onLogin);
    }
    
    /**
    * @param String mail
    * confirma existencia de Friend numa lista de amigos, atraves da String mail
    */
    
    public boolean isMyFrind(String mail){return this.users.get(onLogin).getFriends().contains(mail);}
    
    /**
    * @param int dM, nivel de dificuldade mental
    * @param int dF, nivel de dificuldade fisica
    * faz a avaliação da dificuldade geral de determinada cache para lhe atribuir depois os pontos 
    * @return int total de pontos
    * Ex: dM=3
    *     dF=2
    *     total= dM + dF = 5
    */

    private int calcPont(int dM,int dF){return dM+dF;}
    
    /**
    * criaçao de nova cache
    */

    public void addCache(){
        Cache a = new MicroCache();
        this.caches.put(a.getId(), a);
    }
    
    /**
    * @param Cordenada cord, localização da cache
    * @param GregorianCalendar nc, data da criaçao da cache
    * @param String nome, nome da cache
    * @param String desc, descriçao da cache
    * @param int dF, dificuldade fisica
    * @param int dM, dificuldade mental
    * @param String veriF, codigo de verificaçao da cache
    * @param ArrayList<Item> itens, lista com itens presentes nessa cache
    * @parma int size, tamanho da cache
    * @param Puzzel pz, perguntas
    * adiciona criação de Cache Misterio e os seus dados.
    */

    public Integer addCacheMist(Cordenada cord, GregorianCalendar nc, String nome,String desc,int dF, int dM, String veriF,ArrayList<Item> itens, int size,Puzzel pz){
        MisteryCache creat = new MisteryCache(onLogin,cord,nc,nome,desc,dM,dF,calcPont(dM,dF),veriF,itens,size,pz);
        Integer id = creat.getId();
        this.caches.put(id,creat);
        if(modo==1){
           this.admins.get(onLogin).addHidden(id); 
        }
        else{
            this.users.get(onLogin).addHidden(id,nc.get(Calendar.YEAR), nc.get(Calendar.MONTH),4);
        }
        return id;
    }
    

    /**
    * @param Integer id, numero de identificaçao de cache
    *confirmaçao que chace adicionada ainda nao tinha sido encontrada por utilizador
    * @return boolean (foi ou nao encontrada)
    */

    public boolean userJaEnconCache( Integer id){
        if(this.users.containsKey(onLogin) && this.caches.containsKey(id)){
            return this.users.get(onLogin).getFounded().contains(id);
        }
        return false;
    }


    /**
    * @param Integer id
    * confirmacao que cache existe, e caso a sua existencia seja confirmada atribui-lhe os seus pontos especificos.
    * @return int, pontos fisicos da cache
    */

    public int getPontFisicCache(Integer id){
        if(this.caches.containsKey(id)==false) return 0;
        Cache ca = this.caches.get(id).clone();
        FisicCache f = (FisicCache)ca;
        return f.getPontuation();
    }
    

    /**
    * @param Integer id
    * confirma se todas as caches presentes numa multicache foram visitadas
    */

    public boolean multiTodasVisitadas(Integer id){
        //ir as visitadas do onlogin e ver se tem todas as da multi
        //adicionar a multi as comums
        return false;
    }
    
    /**
    * @param Integer id, numero caracteristico daquela cache
    * @param int pontos, pontos especifico daquela cache 
    * @parma int tipo, tipo da cache
    * @param GregorianCalendar data, data em que cache foi encontrada
    * adiciona Cache as Caches ja encontradas
    */

    public void registaEncontroCache(Integer id,int pontos,int tipo,GregorianCalendar data){
        this.caches.get(id).addViseted(onLogin);
        this.users.get(onLogin).addFound(id,data.get(Calendar.YEAR),data.get(Calendar.MONTH),tipo);
        this.users.get(onLogin).addPontos(data.get(Calendar.YEAR),data.get(Calendar.MONTH),pontos);
        
    }
    
    /**
    * @param Integer id
    * contagem de vezes que surgio algum problema com aquela cache em especifico
    */

    private void updateReport(Integer id){
        int n=1;
        if(this.reportAbuse.containsKey(id)){
            n = this.reportAbuse.get(id)+1;
            this.reportAbuse.remove(id);
            n++;
        }
        this.reportAbuse.put(id,n);
    }
    

    /**
    * @param Integer id
    * faz report se surgio algum problema com aquela cache em especifico, reportado por terceiros
    */

    public void registReportAbuse(Integer id){
        if(!this.repoedByMe(id)){
            this.users.get(onLogin).addMyReportAbuse(id);
            this.updateReport(id);
        }
    }

    /**
    * @param Integer id
    * faz report se surgio algum problema com aquela cache em especifico, reportado pelo utilizador 
    */

    public boolean repoedByMe(Integer id){
        if(this.users.containsKey(onLogin) && this.caches.containsKey(id)){
            return this.users.get(onLogin).getMyReportAbuse().contains(id);
        }
        return false;
    }
    
    /**
    *
    */

    public int getUsersRegist(){return this.usersRegist;}
    
    /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador faz o seu historial das 10 ultimas caches encontradas
    * @return este historico de forma a ser "human readable"
    */

    
    public String last10encCacheBy(String mail){
        ArrayList<Integer> anlfoung = getEncontradas(mail);
        StringBuilder s = new StringBuilder();
        int i=0;
          s.append("Caches encontradas ( 10 de " + Math.min(10,anlfoung.size()) + "):\n");
          for(i=0;i<Math.min(10,anlfoung.size());i++){
               Integer c = anlfoung.get(anlfoung.size()-i);
               s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }
        return s.toString();
    }
    

    /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador faz o historial de todas as caches encontradas
    * @return este historico de forma a ser "human readable"
    */

    public String allOrdencCacheBy(String mail){
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> anlfoung = new ArrayList<Integer>();
        TreeSet<Integer> novo = new TreeSet<Integer>();
        novo.addAll(this.getEncontradas(mail));
        anlfoung.addAll(novo);
        s.append("Caches encontradas (" + anlfoung.size() + "):\n");
        for(Integer c: anlfoung){
            s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
        }
        return s.toString();
    }
    

    /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador faz o historial de todas as caches encontradas
    * @return este historico de forma a ser "human readable"
    */
    
    public String last10HideCacheBy(String mail){
        ArrayList<Integer> anlhide = getHiddenPor(mail);
        StringBuilder s = new StringBuilder();
        int i=0;
        s.append("Caches sscondidas ( 10 de " + Math.min(10,anlhide.size()) + "):\n");
          for(i=0;i<Math.min(10,anlhide.size()) ;i++){
               Integer c = anlhide.get(anlhide.size()-i);
               s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
        }
        return s.toString();
    }
    


    /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador faz o historial de todas as caches criadas
    * @return este historico de forma a ser "human readable"
    */
    public String allOrdHideCacheBy(String mail){
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> anlhide = getHiddenPor(mail);
        s.append("Caches escondidas (" + anlhide.size() + "):\n");
            for(Integer c: anlhide){
                s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }
        return s.toString();
    }
    

     /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador faz o seu historial dos 10 ultimos eventos encontrados
    * @return este historico de forma a ser "human readable"
    */    
    public String last10EventsBy(String mail){
        ArrayList<Integer> analEvents = getEventos(mail);
        StringBuilder s = new StringBuilder();
        int i=0;
        s.append("Eventos Participados (10 de " +  Math.min(10,analEvents.size()) + "):\n");
            for(i=0;i<Math.min(10,analEvents.size());i++){
               Integer ev = analEvents.get(analEvents.size()-i);
               s.append("\t"+ ev.toString() +"\n");
            }
        return s.toString();
    }
    
    /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador faz o historial de todos os eventos em que participou
    * @return este historico de forma a ser "human readable"
    */

    public String allOrdEventsBy(String mail){
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> analEvents = getEventos(mail);
        s.append("Eventos Participados (" + analEvents.size() + "):\n");
            for(Integer ev: analEvents){
                s.append("\t"+ ev.toString() +"\n");
            }
        return s.toString();
    }
    


     /**
    * @param String mail, caracteristica de cada utilizador
    * atraves do mail do utilizador sao apresentadas todas as informaçoes daquele utilizador
    * @return esta informçao e apresentada de forma a ser "human readable"
    */

    //falta report abuse e os gerais de encontradas
    public String userDeatils(String mail){
        StringBuilder s = new StringBuilder();
        DefaultUser anal = this.users.get(mail).clone();
        s.append("ID: " + mail + ", Nome: " + anal.getNome());
        if(adminMode()){
            s.append(" Pass: "+ anal.getPassword());
        }
        
        s.append("\nNac: " + anal.getDataNacFotmatada() + " Genero: "+  anal.getGenero() +"\n");
        Local analoc = anal.getMorada();
        s.append("Pais: " + analoc.getPais() +" Cidade: " +  analoc.getCidade() +" Rua: " + analoc.getRua() +" Codigo Postal: " + analoc.getCodP() +"\n");
        ArrayList<String> anlamigos = getAmigos(mail);
        s.append("Amigos (" + anlamigos.size() + "):\n");
        for(String f: anlamigos){
            s.append("\t"+ f.toString() +"\n");
        }
        //ArrayList<Integer> anlfoung = getEncontradas(mail);
        if(this.onLogin.equals(mail) || this.adminMode()){
            /*s.append("Caches encontradas (" + anlfoung.size() + "):\n");
            for(Integer c: anlfoung){
                s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }*/
            s.append(this.allOrdencCacheBy(mail));
        }else{//amigo so ve 10 cache por ordem
         /* int i=0;
          s.append("Caches encontradas ( 10 de " + anlfoung.size() + "):\n");
          for(i=0;i<10;i++){
               Integer c = anlfoung.get(anlfoung.size()-i);
               s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }*/
            s.append(this.last10encCacheBy(mail));
        }
        
        //ArrayList<Integer> anlhide = getHiddenPor(mail);
        if(this.onLogin.equals(mail) || this.adminMode()){
           /* s.append("Caches escondidas (" + anlhide.size() + "):\n");
            for(Integer c: anlhide){
                s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }*/
            allOrdHideCacheBy(mail);
        }else{//amigo so ve 10 cache por ordem
            last10HideCacheBy(mail);
         /* int i=0;
          s.append("Caches encontradas ( 10 de " + anlhide.size() + "):\n");
          for(i=0;i<10;i++){
               Integer c = anlhide.get(anlfoung.size()-i);
               s.append("\t"+ c.toString() +" (" + dizTipoCache(c) +")\n");
            }*/
        }
        
        //ArrayList<Integer> analEvents = getEventos(mail);
        if(this.onLogin.equals(mail) || this.adminMode()){
            /*s.append("Eventos Participados (" + analEvents.size() + "):\n");
            for(Integer ev: analEvents){
                s.append("\t"+ ev.toString() +"\n");
            }*/
            allOrdEventsBy(mail);
        }else{
            last10EventsBy(mail);
            /*s.append("Eventos Participados (10 de " + analEvents.size() + "):\n");
            int i;
            for(i=0;i<10;i++){
               Integer ev = analEvents.get(analEvents.size()-i);
               s.append("\t"+ ev.toString() +"\n");
            }*/
        
        }
        
        return s.toString(); 
    }
    


    /**
    * 
    */

    public boolean adminMode(){return this.modo==1;}
    
    /**
    * 
    */

    public void setLogin(String mail){this.onLogin=mail;}
    
    /**
    * 
    */

    private ArrayList<Integer> getEventos(String mail){
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getPratEvents());
        return novo;
    }
    

    public String dizTipoCache(Integer id){
        if(this.caches.containsKey(id)==false) return "Removida";
        return this.caches.get(id).tipoCache();
    }
    
    //a ananilsar esta
    public String getCacheCode(Integer id){
        Cache ca = this.caches.get(id).clone();
        //String tipo =ca.tipoCache();
        if(ca instanceof FisicCache){
            return ((FisicCache)ca).getVeriCode();
        }
        return null;
    }
    public ArrayList<Integer> getEncontradasOrd(String mail){
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getFounded());
        ret.addAll(novo);
        return ret;
    }
    
    public ArrayList<Integer> getEncontradas(String mail){
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getFounded());
        return novo;
    }
    
    
    private ArrayList<Integer> getHiddenPor(String mail){
        ArrayList<Integer> novo = new ArrayList<Integer>();
        novo.addAll(this.users.get(mail).getHidden());
        return novo;
    }
    
    private ArrayList<String> getAmigos(String mail){
        TreeSet<String> novo = new TreeSet<String>();
        ArrayList<String> ret = new ArrayList<String>();
        ArrayList<String> retL = new ArrayList<String>();
        novo.addAll(this.users.get(mail).getFriends());
        ret.addAll(novo);
        for(String a : ret){
            if(jaExisteUser(a)){
                retL.add(a);
            }
        }
        return retL;
    }
    
    public boolean existeEvento(Integer id){return this.events.containsKey(id);}
    
    public void removeEvento(Integer id){this.events.remove(id);}
    
    public ArrayList<Integer> getEventos(){
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        novo.addAll(this.events.keySet());
        ret.addAll(novo);
        return ret;
    }
    

     /**
    *@param Integer id, caracteristico de cada cache 
    *remocao de uma determinada cache das caches existentes
    */
    public void forceRemoveCache(Integer id){
        this.caches.remove(id);
        if(this.estanoReport(id)){
            resetReport(id);
        }
        
    }

    /**
    *@param Integer id, caracteristico de cada cache 
    *confirma se aquela cache existe ou nao
    *@return boolean (existe ou nao existe)
    */
    public boolean existeCache(Integer id){return this.caches.containsKey(id);}
    
    
    /** 
    *
    */
    public int limpaReportALL(){
        ArrayList<Integer> prec = new ArrayList<Integer>();
        prec.addAll(this.reportAbuse.keySet());
        int ret=0;
        for(Integer analise : prec){
            if(this.reportAbuse.get(analise) >= rp){
                this.reportAbuse.remove(analise);
                this.caches.remove(analise);
                ret++;
            }
        }
        return ret;
    }
    
    public boolean existeALimpar(){
        ArrayList<Integer> prec = new ArrayList<Integer>();
        prec.addAll(this.reportAbuse.keySet());
        int ret=0;
        for(Integer analise : prec){
            if(this.reportAbuse.get(analise) >= rp){
                ret++;
            }
        }
        return ret>0;
    
    }
    
    public boolean semReport(){return this.reportAbuse.isEmpty();}
    
    public boolean estanoReport(Integer id){return this.reportAbuse.containsKey(id);}
    public void limpacacheRp(Integer id){this.reportAbuse.remove(id); this.caches.remove(id);}
    public void resetReport(Integer id){this.reportAbuse.remove(id);}
    /**
     * Constructor for objects of class GocachingPOO
     */
    public GeocachingPOO()
    {
        // initialise instance variables
        this.relogProg = new GregorianCalendar();
        this.users = new TreeMap<String,DefaultUser>();
        this.events = new TreeMap<Integer,Evento>();
        this.reportAbuse = new TreeMap<Integer,Integer>();
        this.caches = new  TreeMap<Integer,Cache>();
        this.admins = new TreeMap<String,Admin>();
        this.modo=0;
        this.onLogin="";
        //criar com o admin default
        Admin novo= new Admin();
        this.admins.put(novo.getEmail(),novo);
    }



    /**
    *confirma se ja existe Admin.
    *@return 
    */
    public boolean hasAdmin(){return !(this.admins.isEmpty());}
    

    /**
    *@param String mail, caracteristico de cada utilizador
    *@param String pass, caracteristico dessa conta
    *@param String nome, caracteristico dessa conta
    *coriação de um administrador.
    */

    public void registaAdmin(String mail, String pass, String nome){
        Admin novo = new Admin(mail,pass,nome);
        this.admins.put(novo.getEmail(),novo);
    }
    

     /**
    * @param String mail, e-mail de utilizador
    * @param String pass, password para a conta desse utilizador
    * @param String nome, nome do utilizadro
    * @param char genero, genero do utilizador (M/F)
    * @param GregorianCalendar dtn, data da criaçao de conta
    * @param String pais, pais onde utilizador vive
    * @param String cidade, cidade onde utilizador vive
    * @param String rua, rua onde utilizador vive
    * @parma String cp, codigo postal de onde o utilizador vive
    * criaçao de conta de um utilizador.
    */

    public void registaUser(String mail, String pass, String nome,char genero,GregorianCalendar dtn,String pais,String cidade,String rua,String cp){
        DefaultUser novo = new DefaultUser(mail,pass,nome,dtn.get(Calendar.YEAR),dtn.get(Calendar.MONTH),dtn.get(Calendar.DAY_OF_MONTH),genero,new Local(pais,cidade,rua,cp,new Cordenada()));
        this.users.put(novo.getEmail(),novo);
        this.usersRegist++;
    }
    

    /**
    *@param String mail, caracteristico de cada utilizador
    *confirma se aquela administrador ja existe.
    *@return boolean (existe ou nao existe)
    */
    public boolean jaExisteAdmin(String mail){return this.admins.containsKey(mail);}
    
    /**
    *@param String mail, caracteristico de cada utilizador
    *confirma se aquela utilizador ja existe.
    *@return boolean (existe ou nao existe)
    */
    public boolean jaExisteUser(String mail){return this.users.containsKey(mail);}
    
    /**
    *@param String mail, caracteristico de cada utilizador
    *confirma se aquela mail ja existe.
    *@return boolean (existe ou nao existe)
    */
    public boolean jaExisteIDuseradmin(String mail){return (this.jaExisteAdmin(mail) || this.jaExisteUser(mail));}
    



    public String getPassDe(String mail){
        String ret;
        if(jaExisteAdmin(mail)){
            ret = this.admins.get(mail).getPassword();
        }else{
           ret = this.users.get(mail).getPassword(); 
        }
        return ret;
    }
    

    /**
    *passa os utilizadores para uma nova estrutura
    *@return ArrayList<String> 
    */
    public ArrayList<String> getUsers(){
        TreeSet<String> novo = new TreeSet<String>();
        ArrayList<String> ret = new ArrayList<String>();
        novo.addAll(this.users.keySet());
        ret.addAll(novo);
        return ret;
    }
    
    /**
    *passa os caches para uma nova estrutura
    *@return ArrayList<Integer> 
    */
    
    public ArrayList<Integer> getCaches(){
        TreeSet<Integer> novo = new TreeSet<Integer>();
        ArrayList<Integer> ret = new ArrayList<Integer>();
        novo.addAll(this.caches.keySet());
        ret.addAll(novo);
        return ret;
    }
    
    
    
    /**
    *@param String mail, caracteristico de cada utilizador
    *confirma se aquela mail ja existe.
    *@return boolean (conseguio ou nao consegui remover a cache)
    */
    public boolean removeUser(String mail){
        if (this.users.containsKey(mail)){
            this.users.remove(mail);
            return true;
        }
        return false;
    }
    

    /**
    *@param String mail, caracteristico de cada administrador
    *confirma se aquela mail ja existe.
    *@return boolean (conseguio ou nao consegui remover a cache)
    */
    public boolean removeAdmin(String mail){
        if (this.admins.containsKey(mail)){
            this.admins.remove(mail);
            return true;
        }
        return false;
    }
    public boolean setUserPASS(String id ,String pass){
        if(this.users.containsKey(id)==false) return false;
        this.users.get(id).setPassword(pass);
        return true;
    }
    
    public boolean setadminPASS(String id ,String pass){
        if(this.admins.containsKey(id)==false) return false;
        this.admins.get(id).setPassword(pass);
        return true;
    }
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public int sampleMethod(int y)
    {
        // put your code here
        return 1;
    }
    public void setModo(int m){this.modo=m;}
}
