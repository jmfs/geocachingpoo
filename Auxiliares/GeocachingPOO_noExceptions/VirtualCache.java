/**
 * Write a description of class VirtualCache here.
 * 
 * @author (POOGrupo55) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.util.GregorianCalendar;
public class VirtualCache extends Cache
{
    // variaveis de instancia
    private ArrayList<Foto> album;
    
    /**
     * Construtor vazio para objectos da classe VirtualCache
     */
    public VirtualCache(){
        super();
        this.album=new ArrayList<Foto>();
    }
    
    /**
     * Construtor parametrizado para objectos da classe VirtualCache com Email, Coordenada, Data, nome, descrição, dif. mental, dif. fisica e pontuação
     */
    public VirtualCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p){
        super(utl,l,dt,n,desc,dM,dF,p);
        this.album=new ArrayList<Foto>();
    }
    
    /**
     * Construtor de copia para objectos da classe VirtualCache
     */
    public VirtualCache (VirtualCache vc){
        super(vc);
        this.album=vc.getAlbum();
    }

    /**
     * Metodo getAlbum
     * @return      lista de fotos
     */
    public ArrayList<Foto> getAlbum(){
        ArrayList<Foto> af = new ArrayList<Foto>();
        for(Foto f : this.album){af.add(f.clone());}
        return af;
    }
    
    /**
     * Metodo setAlbum
     * @param       lista de fotos
     */
    private void setAlbum(ArrayList<Foto> al){
        ArrayList<Foto> af = new ArrayList<Foto>();
        for(Foto f : al){af.add(f.clone());}
        this.album= af;
    }
    
    /**
     * Metodo nFotos
     * @return      numero de fotos do album
     */
    public int nFotos(){return this.getAlbum().size();}
    
    /**
     * Metodo addFoto
     * @param       foto para ser adicionada a lista de fotos da cache
     * @return      se foi possivel adicionar
     */
    public boolean addFoto(Foto f){return this.album.add(f.clone());}
    
    /**
     * Metodo removeFoto
     * @param       foto para ser removida da lista de fotos da cache
     * @return      se foi possivel remover
     */
    public boolean removeFoto(Foto f){return this.album.remove(f.clone());}
    
    /**
     * Metodo toString
     * @return      string
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Fotos:\n");
        for(Foto f : this.getAlbum()){s.append(f.toString());}
        return s.toString();
    }
    
    /**
     * Metodo Clone
     * @return      copia do objecto
     */
    public VirtualCache clone(){return new VirtualCache(this);}
    
    /**
     * Metodo equals
     * @param       objeto a comparar
     * @return      resultado da igualdade
     */
    public boolean equals(Object obj){
       boolean res = true;
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        VirtualCache vc = (VirtualCache) obj;
        if(!(super.equals(vc) && this.nFotos()==vc.nFotos())) return false;
        for(int i=0; res && i<this.nFotos(); i++){res = this.getAlbum().get(i).equals(vc.getAlbum().get(i));}
        return res;
    }
}
