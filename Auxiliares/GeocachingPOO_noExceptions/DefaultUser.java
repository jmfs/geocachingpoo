
/**
 * Write a description of class DefaultUser here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Iterator;
public class DefaultUser extends Utilizador
{
    // instance variables 
    
    private char genero;
    private Local morada;
    private EstaUtil stats;
    private ArrayList<Integer> founded; //contem as ID caches que o utilizador encontrou
    private ArrayList<Integer> pratEvents; // tem os ID eventos(atividades) que o utilizador participa
    private TreeSet<String> friends; // amigos do user
    private ArrayList<Integer> myReportAbuse; //contem as ID caches que o utilizador abusou
    private CoinItem coin;
    /**
     * Contrutor vazio da classe DefaultUser
     */
    public DefaultUser()
    {
        // initialise instance variables
        super();
        this.genero='O';
        this.morada = new Local();
        this.stats = new EstaUtil();
        this.founded = new ArrayList<Integer>();
        this.pratEvents = new ArrayList<Integer>();
        this.friends = new TreeSet<String>();
        this.myReportAbuse = new ArrayList<Integer>();
        this.coin=null;
    }
    
     /**
     * Contrutor de coipa da classe DefaultUser
     */
    public DefaultUser(DefaultUser u)
    {
        // initialise instance variables
        super(u);
        this.genero=u.getGenero();
        this.morada = u.getMorada();
        this.stats = u.getStats();
        this.founded = u.getFounded();
        this.pratEvents = u.getPratEvents();
        this.friends = u.getFriends();
        this.myReportAbuse = u.getMyReportAbuse();
        this.coin=u.getCoin();
    }
    
    /**
     * Contrutor parametrizado da classe DefaultUser com Email, Password, Nome, Ano, Mes, Dia, Genero, Morada
     */
    public DefaultUser(String email, String password, String nome, int ano, int mes, int dia, char genero,Local morada)
    {
        // initialise instance variables
        super(email,password,nome,ano,mes,dia);
        this.genero=genero;
        this.morada = morada.clone();
        this.stats = new EstaUtil();
        this.founded = new ArrayList<Integer>();
        this.pratEvents = new ArrayList<Integer>();
        this.friends = new TreeSet<String>();
        this.myReportAbuse = new ArrayList<Integer>();
        this.coin=null;
    }
    
    /**
     * GETTER
     */
    public CoinItem getCoin(){
        if(this.coin==null) return null;
        return this.coin.clone();
    }
    /**
     * GETTER
     */
    public char getGenero(){return this.genero;}
    /**
     * GETTER
     */
    public Local getMorada(){return this.morada.clone();}
    /**
     * GETTER
     */
    public EstaUtil getStats(){return this.stats.clone();}
    
    /**
     * GETTER
     */
    public ArrayList<Integer> getFounded(){
        ArrayList<Integer>  novo = new ArrayList<Integer>();
        novo.addAll(this.founded);
        return novo;
    }
    /**
     * GETTER
     */
    public ArrayList<Integer> getPratEvents(){
        ArrayList<Integer>  novo = new ArrayList<Integer>();
        novo.addAll(this.pratEvents);
        return novo;
    }
    /**
     * GETTER
     */
    public TreeSet<String> getFriends(){
        TreeSet<String>  novo = new TreeSet<String>();
        novo.addAll(this.friends);
        return novo;
    }
    /**
     * GETTER
     */
    public ArrayList<Integer> getMyReportAbuse(){
        ArrayList<Integer>  novo = new ArrayList<Integer>();
        novo.addAll(this.myReportAbuse);
        return novo;
    }
    
    /**
     * SETTER
     */
    public void setGenero(char g){this.genero=g;}
    /**
     * SETTER
     */
    public void setCoin(CoinItem ci){this.coin=ci.clone();}
    /**
     * SETTER
     */
    public void setMorada(Local l){this.morada = l.clone();}
    /**
     * SETTER
     */
    public  void setStats(EstaUtil es){this.stats=es.clone();}
    /**
     * SETTER
     */
    public void setFounded(ArrayList<Integer> fd){
        ArrayList<Integer>  novo = new ArrayList<Integer>();
        novo.addAll(fd);
        this.founded=novo;
    }
    /**
     * SETTER
     */
    public void setPratEvents(ArrayList<Integer> ev){
        ArrayList<Integer>  novo = new ArrayList<Integer>();
        novo.addAll(ev);
        this.pratEvents =novo;
    }
    /**
     * SETTER
     */
    public void setFriends(TreeSet<String> fr){
        TreeSet<String>  novo = new TreeSet<String>();
        novo.addAll(fr);
        this.friends=novo;
    }
    /**
     * SETTER
     */
    public void setMyReportAbuse( ArrayList<Integer> rb){
        ArrayList<Integer>  novo = new ArrayList<Integer>();
        novo.addAll(rb);
        this.myReportAbuse = novo;
    }
    /**
     * Clone
     * 
     * @return     DefaultUser (copia do que recebe a mensagem)
    */
    public DefaultUser clone(){return new DefaultUser(this);}
    
    /**
     * ToString
     * 
     *@return     String (info do DefaultUser)
    */
   public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        
        s.append("Genero: " + this.getGenero() + "\n");
        s.append("Morada: " + this.getMorada() + "\n");
        s.append("Stats: " + this.getStats() + "\n");
        s.append("Caches Encontradas:\n");
        for(Integer ca : this.getFounded()){
            s.append("\t"+ ca.toString() + ";\n");
        }
        s.append("Eventos:\n");
        for(Integer ev : this.getPratEvents()){
            s.append("\t"+ ev.toString() + ";\n");
        }
        s.append("Amigos:\n");
        for(String frind : this.getFriends()){
            s.append("\t"+ frind + ";\n");
        }
        s.append("Reported:\n");
        for(Integer rp : this.getMyReportAbuse()){
            s.append("\t"+ rp.toString() + ";\n");
        }
        return s.toString();
        
    }
    /**
     * Metodo default Equals
     * 
     * @param  o   Objeto a comparar igualdade com o recetor
     * @return     true se os objetos sao iguais e false caso nao sejam 
     */
   public boolean equals(Object o){
        if (this==o) return true;
       if(this==null || o==null || this.getClass()!=o.getClass()) return false;
       DefaultUser du = (DefaultUser)o;
       boolean ret= super.equals(du);
       ret = ret && this.getGenero()==(du.getGenero());
       ret= ret && this.getMorada().equals(du.getMorada());
       ret= ret && this.getStats().equals(du.getStats());
       ret = ret && (this.nFound()==du.nFound()) && (this.nPratEvents()==du.nPratEvents()) && (this.nFriends()==du.nFriends()) && (this.nMyReportAbuse()==du.nMyReportAbuse());
       if(ret==false) return false;
       //igualded das encontradas
       Iterator<Integer> u1I = this.getFounded().iterator();
       Iterator<Integer> u2I = du.getFounded().iterator();
       while(ret && u1I.hasNext() && u2I.hasNext() ){
            ret = u1I.next().equals(u2I.next()); 
        }
       if(ret==false || (u1I.hasNext() != u2I.hasNext())) return false;
       //igualdade eventos
       u1I = this.getPratEvents().iterator();
       u2I = du.getPratEvents().iterator();
       while(ret && u1I.hasNext() && u2I.hasNext() ){
            ret = u1I.next().equals(u2I.next()); 
        }
       if(ret==false || (u1I.hasNext() != u2I.hasNext())) return false;
        //igualdade reports
       u1I = this.getMyReportAbuse().iterator();
       u2I = du.getMyReportAbuse().iterator();
       while(ret && u1I.hasNext() && u2I.hasNext() ){
            ret = u1I.next().equals(u2I.next()); 
        }
       if(ret==false || (u1I.hasNext() != u2I.hasNext())) return false;
       //igualdade de amigos
       Iterator<String> u1S = this.getFriends().iterator();
       Iterator<String> u2S = du.getFriends().iterator();
       while(ret && u1S.hasNext() && u2S.hasNext() ){
            ret = u1S.next().equals(u2S.next()); 
        }
       if(ret==false || (u1S.hasNext() != u2S.hasNext())) return false;
    return ret;
       
    }
    /**
     * Metodo que retorna o numero de caches encontradas pelo utilizador que recebe a mensagem
     * 
     * @return     int (nuemro de cahes encontradas do utilizador)
     */
    public int nFound(){return this.founded.size();}
        /**
     * Metodo que retorna o numero de eventos participados pelo utilizador que recebe a mensagem
     * 
     * @return     int (nuemro de eventos participados do utilizador)
     */
    public int nPratEvents(){return this.pratEvents.size();}
        /**
     * Metodo que retorna o numero de amigos do utilizador que recebe a mensagem
     * 
     * @return     int (nuemro de amigos do  utilizador)
     */
    public int nFriends(){return this.friends.size();}
        /**
     * Metodo que retorna o numero de caches utilizador que recebe a mensagem fez repor abuse
     * 
     * @return     int (nuemro de cahes que o utilizador fez report abuse)
     */
    public int nMyReportAbuse(){return this.myReportAbuse.size();}
 
    /**
     * Metodo para adicionar uma nova cache a lista de encontradas(por id)
     * 
     * @param  id   Integer (id da cache a adicionar)
     * @return     true caso a cache seja adicionada com sucesso (ainda nao existia), false caso ja exista na lista de encontradas
     */
    public boolean addFound(Integer id,int ano, int mes, int tipo){
        //1-Virtual
        //2multi
        //3micro
        //4misterio
        if( this.founded.contains(id)) return false;
        this.founded.add(id);
        switch(tipo){
                case 1: this.stats.addVirtFAnoMes(ano,mes); break;
                case 2: this.stats.addMultFAnoMes(ano,mes); break;
                case 4: this.stats.addMisteFAnoMes(ano,mes); break;
                default: this.stats.addMicroFAnoMes(ano,mes); break;
            }
        return true;
    }
    
    /**
     * addHidden
     * 
     *@param      Integer (id da cache a adicionar à lista das escondidas)
     *
    */
    public boolean addHidden(Integer id, int ano, int mes, int tipo){ //1-Virtual
        //2multi
        //3micro
        //4misterio
        if(super.addHidden(id)){
            switch(tipo){
                case 1: this.stats.addVirtHAnoMes(ano,mes); break;
                case 2: this.stats.addMultHAnoMes(ano,mes); break;
                case 4: this.stats.addMisteHAnoMes(ano,mes);break;
                case 3: this.stats.addMicroHAnoMes(ano,mes);break;
            }
            return true;
        }
        return false;
    }
    /*
    /**
     * Metodo para adicionar uma nova cache a lista de encontradas
     * 
     * @param      Cache a adicionar
     * @return     true caso a cache seja adicionada com sucesso (ainda nao existia), false caso ja exista na lista de encontradas
     */
    /*
    public boolean addFound(Cache ca){return this.addFound(ca.getId());}    
    */
    /**
     * Metodo para adicionar um novo evento a lista de eventos paricipados(por id)
     * 
     * @param  id   Integer (id do evento a adicionar)
     * @return     true caso o evento seja adicionada com sucesso (ainda nao existia), false caso ja exista na lista de eventos
     */
    public boolean addPratEvents(Integer id, int ano ,int mes){
        if( this.pratEvents.contains(id)) return false;
        this.stats.addEventPAnoMes(ano,mes);
        this.pratEvents.add(id);
        return true;
    }
    
    /*
    /**
     * Metodo para adicionar um novo evento a lista de eventos paricipados
     * 
     * @param      Evento a adicionar
     * @return     true caso o evento seja adicionada com sucesso (ainda nao existia), false caso ja exista na lista de eventos
     */
    /*
    public boolean addPratEvents(Evento ev){return this.addPratEvents(ev.getId());}
    */
    /**
     * Metodo para adicionar novo DefaultUser aos amigos (por id)
     * 
     * @param  id   String (id do utilizador a adicionar)
     * @return     true caso o utilizador seja adicionado com sucesso (ainda nao existia), false caso ja exista na lista de amigod
     */
    
    public boolean addFriends(String id){
        if( this.friends.contains(id)) return false;
        this.friends.add(id);
        return true;
    }
    
    /**
     * Metodo para adicionar novo DefaultUser aos amigos
     * 
     * @param      DefaultUser amigo a adicionar
     * @return     true caso o utilizador seja adicionado com sucesso (ainda nao existia), false caso ja exista na lista de amigod
     */
    public boolean addFriends(DefaultUser u){ return this.addFriends(u.getId());}
    
    /**
     * Metodo para adicionar uma nova cache a lista de reportabuse(por id)
     * 
     * @param  id   Integer (id da cache a adicionar)
     * @return     true caso a cache seja adicionada com sucesso (ainda nao existia), false caso ja exista na lista de report abuse
     */
    
    
    public boolean addMyReportAbuse(Integer id){
        if( this.myReportAbuse.contains(id)) return false;
        this.myReportAbuse.add(id);
        this.stats.addRepotaduse();
        return true;
    }
    /*
    /**
     * Metodo para adicionar uma nova cache a lista de reportabuse
     * 
     * @param      Cache a adicionar ao report abuse
     * @return     true caso a cache seja adicionada com sucesso (ainda nao existia), false caso ja exista na lista de report abuse
     */
    /*
    public boolean addMyReportAbuse(Cache ca){ return this.addMyReportAbuse(ca.getId());}
    */
    //REMOVES
    /**
     * Metodo para remover uma cache a lista de encotradas (por id)
     * 
     * @param  id   Integer (id da cache a remover)
     * @return     true caso a cache seja removida com sucesso (existia na lista), false caso nao exista na lista de encontradas
     */
    private boolean removeFound(Integer id){return this.founded.remove(id);}
    
    /*public boolean removeFound(Cache ca){return this.removeFound(ca.getId());}*/
    
    /**
     * Metodo para remover um evento dos participados (por id)
     * 
     * @param  id   Integer (id do Evento a remover)
     * @return     true caso a cache seja removida com sucesso (existia na lista), false caso nao exista na lista de encontradas
     */
    private boolean removePratEvents(Integer id){return this.pratEvents.remove(id);}
    
    /*public boolean removePratEvents(Evento ev){return this.removePratEvents(ev.getId());}*/
    
    /**
     * Metodo para remover uma cache a lista de encotradas (por id)
     * 
     * @param  id   Integer (id da cache a remover)
     * @return     true caso a cache seja removida com sucesso (existia na lista), false caso nao exista na lista de encontradas
     */
    public boolean removeFriends(String id){return this.friends.remove(id);}

    public boolean removeFriends(DefaultUser u){ return this.removeFriends(u.getId());}
    
    /**
     * Metodo para remover uma cache a lista de encotradas (por id)
     * 
     * @param  id   Integer (id da cache a remover)
     * @return     true caso a cache seja removida com sucesso (existia na lista), false caso nao exista na lista de encontradas
     */
    private boolean removeMyReportAbuse(Integer id){return this.myReportAbuse.remove(id);}
    //public boolean removeMyReportAbuse(Cache ca){return this.removeMyReportAbuse(ca.getId());}
    
    /**
     * Metodo para verificar se o utilizador possui alguma CoinItem em posse
     * 
     * @return     true caso possua uma coin item false caso contrario
     */
    
    public boolean hasCoin(){return this.coin!=null;}
    
    /**
     * Metodo para remover a CoinItem que o utilizador Possui (get com remoçao da coin do utilizador)
     * 
     * )
     * @return   CoinItem  a coinItem que o utilizador possuia
     */
    
    public CoinItem retirarCoin(){
        CoinItem ret = this.getCoin();
        this.coin=null;
        return ret;
    }
    
    /**
     * Mecher nas pontuaçoes
     */
    public void addPontos(int ano, int mes, int pont){this.stats.addPontosAnoMes(ano,mes,pont);}
    /**
     * Mecher nas pontuaçoes
     */
    public int quantPontos(int ano){return this.stats.getPontAno(ano);}
    /**
     * Mecher nas pontuaçoes
     */
    public int quantPontos(int ano,int mes){return this.stats.getPontAnoMes(ano,mes);}
    /**
     * @return em quantos anos o utilizador tem estatisticas (tempo registo)
     */
    public int idadeGeo(){return this.stats.quantosAnos();}
    
    /**
     * Consultar estatisticas (razoes)
     */
    public float razHidenFound(int ano){return this.stats.razHideFound(ano,0,11);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razFoundHiden(int ano){return this.stats.razFoundHide(ano,0,11);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razHidenFound(int ano, int mes){return this.stats.razHideFound(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razFoundHiden(int ano, int mes){return this.stats.razFoundHide(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    
    public float razMistFoundTotl(int ano){return this.stats.razMisteTotalFAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMultFoundTotl(int ano){return this.stats.razMultTotalFAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razVirtFoundTotl(int ano){return this.stats.razVirtTotalFAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMicroFoundTotl(int ano){return this.stats.razMicroTotalFAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    
    public float razMistFoundTotl(int ano, int mes){return this.stats.razMisteTotalFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMultFoundTotl(int ano, int mes){return this.stats.razMultTotalFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razVirtFoundTotl(int ano, int mes){return this.stats.razVirtTotalFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMicroFoundTotl(int ano, int mes){return this.stats.razMicroTotalFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    
    
    public float razMistHidenTotl(int ano){return this.stats.razMisteTotalHAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMultHidenTotl(int ano){return this.stats.razMultTotalHAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razVirtHidenTotl(int ano){return this.stats.razVirtTotalHAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMicroHidenTotl(int ano){return this.stats.razMicroTotalHAno(ano);}
    /**
     * Consultar estatisticas (razoes)
     */
    
    public float razMistHidenTotl(int ano, int mes){return this.stats.razMisteTotalHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMultHidenTotl(int ano, int mes){return this.stats.razMultTotalHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razVirtHidenTotl(int ano, int mes){return this.stats.razVirtTotalHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (razoes)
     */
    public float razMicroHidenTotl(int ano, int mes){return this.stats.razMicroTotalHAnoMes(ano,mes);}
    
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMistF(int ano, int mes){return this.stats.getMisteFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMistF(int ano){return this.stats.getMisteFAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMistH(int ano, int mes){return this.stats.getMisteHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMistH(int ano){return this.stats.getMisteHAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    
    public int quantMicroF(int ano, int mes){return this.stats.getMicroFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMicroF(int ano){return this.stats.getMicroFAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMicroH(int ano, int mes){return this.stats.getMicroHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMicroH(int ano){return this.stats.getMicroHAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    
    public int quantVirtF(int ano, int mes){return this.stats.getVirtFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantVirtF(int ano){return this.stats.getVirtFAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantVirtH(int ano, int mes){return this.stats.getVirtHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantVirtH(int ano){return this.stats.getVirtHAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    
    public int quantMultF(int ano, int mes){return this.stats.getMultFAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMultF(int ano){return this.stats.getMultFAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMultH(int ano, int mes){return this.stats.getMultHAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantMultH(int ano){return this.stats.getMultHAno(ano);}
    /**
     * Consultar estatisticas (quantidades)
     */
    
    public int quantEventP(int ano, int mes){return this.stats.getEventPAnoMes(ano,mes);}
    /**
     * Consultar estatisticas (quantidades)
     */
    public int quantEventP(int ano){return this.stats.getEventPAno(ano);}
    
    /**
     * Consultar se o utilizadot tem estatisticas de uma ano argumento
     */
    public boolean hasYear(int ano){return this.stats.containsAno(ano);}
}
