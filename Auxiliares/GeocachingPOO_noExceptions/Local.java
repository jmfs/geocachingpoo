
/**
 * Write a description of class Morada here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.Serializable;
public class Local implements Serializable
{
    // instance variables - replace the example below with your own
    private String pais;
    private String cidade;
    private String rua;
    private String codP;
    private Cordenada gps;
    

    /**
     * Construtores de objectos da class Morada
     */
    public Local(){
        this.pais = new String();
        this.cidade = new String();
        this.rua = new String();
        this.codP = new String();
        this.gps = new Cordenada();
    }
    
    public Local(String p, String cd, String r, String codP, Cordenada coord){
        this.pais = p;
        this.cidade = cd;
        this.rua = r;
        this.codP = codP;
        this.gps = coord.clone();
    }
    
    public Local(Local m){
        this.pais = m.getPais();
        this.cidade = m.getCidade();
        this.rua = m.getRua();
        this.codP = m.getCodP();
        this.gps = m.getGPS();
    }

    /**
     * Getters
     */
    
    public String getPais(){
        return this.pais;
    }
    public String getCidade(){
        return this.cidade;
    }
    public String getRua(){
        return this.rua;
    }
    public String getCodP(){
        return this.codP;
    }
    public Cordenada getGPS(){
        return this.gps.clone();
    }
    
    /**
     * Setters
     */
    public void setPais(String p){
        this.pais=p;
    }
    public void setCidade(String c){
        this.cidade=c;
    }
    public void setRua(String r){
        this.rua=r;
    }
    public void setCodP(String codP){
        this.codP=codP;
    }
    public void setGPS(Cordenada gps){
        this.gps=gps.clone();
    }
    public void alteraGPS(int laco, double lamin, char laor, int lco, double lmin, char lor, double alt){
        this.gps.setCoordenada(laco, lamin, laor, lco, lmin, lor, alt);
    }
    
    public Local clone() {return new Local(this);}
    
    public int hashCode(){return this.toString().hashCode();}
    
    public boolean equals(Object o){
        if (o==this) return true;
        if (o==null || this==null || o.getClass()!=this.getClass()) return false;
        Local l=(Local)o;
        return (this.getCidade().equals(l.getCidade()) && this.getCodP().equals(l.getCodP()) && this.getGPS().equals(l.getGPS()) && 
                this.getPais().equals(l.getPais()) && this.getRua().equals(l.getRua()));
    
    }
    public String toString () {
       StringBuilder s = new StringBuilder();
       s.append("Pais:" + pais + ' '+ "Cidade:" + cidade + ' '+ "Rua:" + rua + ' '+ "Código postal:" + codP + ' ' + "Coord:" + gps.toString());
       
       return s.toString();
    }
    
    public String getGPSFormatado(){
        return this.getGPS().latitForm() + " "+this.getGPS().longitForm();
    }
    
    public double distM(Local l){ return this.getGPS().distM(l.getGPS());}
    public double distKM(Local l){ return this.getGPS().distKM(l.getGPS());}
    
    public boolean maisPertoM(Local c, double dm){return (this.getGPS().maisPertoM(c.getGPS(),dm));}
    public boolean maisPertoKM(Local c, double dKm){ return (this.getGPS().maisPertoKM(c.getGPS(),dKm));}

    public boolean maisNorte(Local c){return (this.getGPS().maisNorte(c.getGPS()));}
    public boolean maisAlto(Local c){return (this.getGPS().maisAlto(c.getGPS()));}
    public boolean maisEste(Local c){return (this.getGPS().maisEste(c.getGPS()));}
    
    public boolean maisSul(Local c){return (this.getGPS().maisSul(c.getGPS()));}
    public boolean maisBaixo(Local c){return (this.getGPS().maisBaixo(c.getGPS()));}
    public boolean maisOeste(Local c){return (this.getGPS().maisOeste(c.getGPS()));}




}