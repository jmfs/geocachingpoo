
/**
 * Write a description of class MicroCache here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.GregorianCalendar;
import java.util.ArrayList;
public class MicroCache extends FisicCache
{
    // instance variables 
    private int size; //1..4

    /**
     * Construtor vasio para objetos da classe MicroCache
     */
    public MicroCache(){
        super();
        this.size=2;
    }
    
    /**
     * Construtor parametrizado pra objetos da classe MicroCache com Email, cordenada, data , nome , descrição, dif. mental, dif, fisica, pontuação , codigo de verificaçao, lista de itens e tamanho
     */
    public MicroCache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p, String code, ArrayList<Item> t,int size){
        super(utl,l,(GregorianCalendar)dt.clone(),n,desc,dM,dF,p,code,t);
        this.size=size;
    }
    
    /**
     * Construtor de copia para objetos da classe MicroCache
     */
    public MicroCache(MicroCache mc){
        super(mc);
        this.size=mc.getSize();
    }
    
    /**
     * Metodo getSize
     * @return      tamanho da cache 
     */
    public int getSize(){return this.size;}
    
    /**
     * Metodo setSize
     * @param       tamanho da cache 
     */
    private void setSize(int s){this.size=s;}
    
    /**
     * Metodo toString
     * @return      string do objecto     
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        s.append("Tamanho: " + this.getSize() +"\n");
        return s.toString();
    }
    
    /**
     * Metodo clone
     * @return     uma copia de um objecto
     */
    public MicroCache clone(){return new MicroCache(this);}
    
    /**
     * Metodo equals
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        if(this==obj) return true;
        if(obj==null || this.getClass()!=obj.getClass())return false;
        MicroCache mc = (MicroCache) obj;
        return (super.equals(mc) && this.getSize()==mc.getSize());
    }
}
