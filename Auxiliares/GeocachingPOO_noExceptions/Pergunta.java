
/**
 * Write a description of class Pergunta here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.ArrayList;
import java.io.Serializable;
public class Pergunta implements Serializable
{
    // instance variables
    private String pergunta;
    private ArrayList<String> opResp;//No maximo 2 opçoes
    private String correcta;
    private int pont;
    
    /**
     * Constructor for objects of class Pergunta
     */
    public Pergunta(){
        this.pergunta = new String();
        this.opResp = new ArrayList<String>(2);
        this.correcta = new String();
        this.pont = 1;
    }
    
    /**
     * Construtor parametrizado para objectos da classe Pergunta com Pergunta,Lista de opçoes, Resposta e Pontuação
     */
    public Pergunta(String p, ArrayList<String> r, String c, int pont){
        this.pergunta = p;
        ArrayList<String> resp = new ArrayList<String>();
        resp.addAll(r);
        this.opResp = resp;
        this.correcta = c;
        this.pont = pont;
    } 
    
    /**
     * Construtor parametrizado para objectos da classe Pergunta com Pergunta,Opção 1,Opção 2, Resposta e Pontuação
     */
    public Pergunta(String p,String op1, String op2, String c, int pont){
        this.pergunta = p;
        ArrayList<String> resp = new ArrayList<String>();
        resp.add(op1);
        resp.add(op2);
        this.opResp = resp;
        this.correcta = c;
        this.pont = pont;
    }
    
    /**
     * Construtor parametrizado para objectos da classe Pergunta com Pergunta, Resposta e Pontuação
     */
    public Pergunta(String p, String c, int pont){
        this.pergunta = p;
        this.opResp = new ArrayList<String>(2);
        this.correcta = c;
        this.pont = pont;
    }
    
     /**
      * Construtor de copia para objectos da classe Pergunta
      */
    public Pergunta(Pergunta p){
        this.pergunta = p.getPergunta();
        this.opResp = p.getOpResp();
        this.correcta = p.getCorrecta();
        this.pont = p.getPont();
    }
    
    /**
     * Metodo getPergunta
     * @return      string pergunta
     */
    public String getPergunta(){return this.pergunta;}
    
    /**
     * Metodo getCorrecta
     * @return      opçao correcta
     */
    public String getCorrecta(){return this.correcta;}
    
    /**
     * Metodo getPont
     * @return      pontuação para a resposta certa
     */
    public int getPont(){return this.pont;}
    
    /**
     * Metodo getOpResp
     * @return      lista de todas as opções de resposta
     */
    public ArrayList<String> getOpResp(){
        ArrayList<String> resp = new ArrayList<String>();
        resp.addAll(this.opResp);
        return resp;
    }
    
    /**
     * Metodo setPergunta
     * @param       pergunta
     */
    public void setPergunta(String p){this.pergunta=p;}
    
    /**
     * Metodo setCorrecta
     * @param       resposta correcta
     */
    public void setCorrecta(String c){this.correcta=c;}
    
    /**
     * Metodo setPont
     * @param       pontuaçao da pergunta
     */
    public void setPont(int pont){this.pont=pont;}
    
    /**
     * Metodo setOpResp
     * @param       lista de opções de resposta
     */
    public void setOpResp(ArrayList<String> r){
        ArrayList<String> resp = new ArrayList<String>();
        resp.addAll(r);
        this.opResp = resp;
    }
    
    /**
     * Metodo verificaResp que verifa se uma resposta é a correcta
     * @param       numero da resposta
     * @return      pontuaçao obtida de acordo com a resposta
     */
    public int verificaResp(int pos){
        int res = 0;
        if(this.correcta.equals(this.opResp.get(pos-1))){
            res = this.getPont();
        }
        return res;
    }
    
    /**
     * Metodo addResposta que adiciona uma opçao de resposta a pergunta
     * @param       resposta
     * @return      se foi possivel adicionar essa resposta(nº resposta maximo = 2)  
     */
    public boolean addResposta(String op){
        boolean res = false;
        if(this.opResp.size()<2){
            this.opResp.add(op);
            res = true;
        }
        return res;
    }
    
    /**
     * Metodo toString
     * @return      string do objecto     
     */
    public String toString(){
        int i=1;
        StringBuilder s = new StringBuilder();
        s.append( "Pergunta: " + this.getPergunta() +"?\n");
        for(String a : this.getOpResp()){
            s.append("\t(Opção-"+i+"): " + a + ";\n");
            i++;
        }
        return s.toString();
    }
    
    /**
     * Metodo clone
     * @return     uma copia de um objecto
     */
    public Pergunta clone(){return(new Pergunta(this));}

    /**
     * Metodo equals
     * @param       objecto que vai comprar 
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        boolean res;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Pergunta v = (Pergunta)obj;
        res = ( (this.getPergunta().equals(v.getPergunta()))  &&  (this.getCorrecta()==v.getCorrecta()) && (this.getPont()==v.getPont()) && (this.getOpResp().size()==v.getOpResp().size()));
        for (int i=0;i<this.opResp.size() && res ; i++){
            res = ( this.opResp.get(i).equals(v.getOpResp().get(i)) );
        }
        return res;
    }
    
    /**
     * Metodo hashCode
     * @return      resultado do calculo do hashCode o objecto
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
}
