
/**
 * Write a description of class Admin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.GregorianCalendar;
import java.util.TreeSet;
import java.util.Iterator;
public class Admin extends Utilizador
{
 
    private TreeSet<Integer> creatEvents; // tem os eventos(atividades) que o utilizador cria
    
    /**
     * Contrutor vazio classe admin
     * 
     * cria com o mail "admin", pass "geocachingPOOAdmin" e nome "Admin"
     */
    public Admin()
    {
        // initialise instance variables
        super("admin@geo.poo","geocachingPOOAdmin","Admin",new GregorianCalendar());
        this.creatEvents = new TreeSet<Integer>();
    }

    /**
     * Contrutor de copia classe admin
     *
     */
    public Admin(Admin a)
    {
        // initialise instance variables
        super(a.getEmail(),a.getPassword(),a.getNome(),a.getDatNac());
        this.creatEvents = a.getCreatedEvents();
    }
    
    /**
     * Contrutor parametrizado classe admin
     *
     */
    public Admin(String mail, String pass, String nome){
        super(mail,pass,nome,new GregorianCalendar());
        this.creatEvents = new TreeSet<Integer>();
    }
    
    /**
     * Getter os events criados pelo admin
     * 
     * @return  TreeSet<Integer> dos eventos criados
     */
    public TreeSet<Integer>  getCreatedEvents(){
        TreeSet<Integer> novo = new TreeSet<Integer>();
        novo.addAll(this.creatEvents);
        return novo;
    }
    
    /**
     * Adiciona um novo evento criado pelo utilizador
     * 
     * @param id do evento criado
     */
    public void addEvent(Integer id){this.creatEvents.add(id);}
    
    /**
     * ToString
     * 
     * toSting do Admin
     * 
     * @return String 
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(super.toString());
        Iterator<Integer> it = this.getCreatedEvents().iterator();
        s.append("Eventos:\n");
        while(it.hasNext()){
            s.append(it.next().toString() +"\n");
        }
        return s.toString();
        
    }
    /**
     * clone
     * 
     * @return  copia do objeto que recebe a mensagem
     * 
     */
    public Admin clone(){return new Admin(this);}
    
    /**
     * equals
     * 
     * @param Object a comparar
     * 
     * @return boolean true se os objetos forem iguals falso caso contrario
     */
    public boolean equals(Object o){
       if (this==o) return true;
       if(this==null || o==null || this.getClass()!=o.getClass()) return false;
       Admin a = (Admin)o;
       if(super.equals(a)==false) return false;
       boolean ret= true;
       ret = ret && this.quantosCreados() == a.quantosCreados();
       if (ret==false) return false;
       Iterator<Integer> ita1 = this.getCreatedEvents().iterator();
       Iterator<Integer> ita2 = a.getCreatedEvents().iterator();
       while(ret && ita1.hasNext() && ita2.hasNext() ){
            ret = ita1.next().equals(ita2.next()); 
        }
        if(ita1.hasNext() != ita2.hasNext() || ret==false) return false;
       return true;
    }
    
    /**
     * quantosCreados
     * 
     * @return o numero de eventos que o admin criou  
     */
    public int quantosCreados(){return this.getCreatedEvents().size();}
    
    /**
     * removeEvent
     *
     * @param id do evento a remover
     * 
     * @return boolean se conseguiu ou nao remover o evento 
     */
    public boolean removeEvent(Integer id){return this.creatEvents.remove(id);}
    
    /**
     * isAdmin
     * 
     * @return true pois é a classe admin
     */
    
    public boolean isAdmin(){return true;}
   
    /**
     * removeEvent
     *
     * @param id do evento a consultar
     * 
     * @return boolean se a criaçao desse evento foi feita por este admin 
     */
    public boolean createdByME(Integer id){ return this.getCreatedEvents().contains(id);}
}
