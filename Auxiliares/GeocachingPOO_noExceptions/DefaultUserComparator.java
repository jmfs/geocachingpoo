
/**
 * Class para compara Pelo total de pontos dois DefaulUser
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Comparator;
public class DefaultUserComparator implements Comparator<DefaultUser>
{
    /**
     * novo comparador pelo total de poontos dos utilizadotes
     * @param  u1 DefaultUser1
     * @param  u2 DefaultUser1
     * @return    <0 se o DefaultUser1 tem menos pontos que DefaultUser2
     *            >0 se o DefaultUser1 tem mais pontos que DefaultUser2 
     *            =0 se o DefaultUser1 tem o mesmo pontos que DefaultUser2
     */
    public int compare(DefaultUser u1, DefaultUser u2){
        return (u1.getStats().getTotalPontos()-u1.getStats().getTotalPontos());
    }

}
