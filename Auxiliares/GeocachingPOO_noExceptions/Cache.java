
/**
 * Abstract class Cache - write a description of the class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Calendar;
import java.io.Serializable;
public class Cache implements Serializable
{
    //variaveis de class
    private static int cacheID=0; //contador de caches
    
    // variavel de instancia
    private int id; //id unico de cada cache
    private String owner; //email do criador da cache
    private String nome; //nome do criador da cache
    private GregorianCalendar creatDate; //data em que a cache foi criada
    private Cordenada loc; //Cordenada onde a cache esta colocada
    private String descri; //descriçao da cache
    private int difMent; //dificuldade mental de 1..5
    private int difFisic; //dificuldade fisica de 1..5
    private int pontuation; //pontuação que cada utilizador ira receber quando encontrar a cache
    private TreeSet<String> visited; //Lista de todos os utilizdores que visitaram a cache
    private ArrayList<String> pistas; //Lista de pistas para encontrar a cache
    private ArrayList<Cordenada> pontInt; //Locais de interesse perto do Cordenada da cache  


    public static int IDCacheG(){return cacheID;}
    public static void setIDCache(int id){cacheID=id;}
    private static int daIDcache(){
        int i = cacheID;
        cacheID++;
        return i;
    }


    public String getDataFormatada(){
        return this.creatDate.get(Calendar.DAY_OF_MONTH)+1+ "/" + (this.creatDate.get(Calendar.MONTH)+1) + "/" + this.creatDate.get(Calendar.YEAR);
    }
    
    /**
     * Construtor vazio para objectos da classe Cache
     */
    public Cache(){
        this.id = daIDcache(); 
        this.loc = new Cordenada();
        this.creatDate = new GregorianCalendar();
        this.nome = new String();
        this.descri = new String();
        this.owner = new String();
        this.difMent=0;
        this.difFisic=0;
        this.pontuation=0;
        this.visited = new TreeSet<String>();
        this.pistas = new ArrayList<String>();
        this.pontInt = new ArrayList<Cordenada>();
    }
    
    /**
     * Construtor parametrizado para objectos da classe Cache com Email
     */
    public Cache(String utl){
        this.id= daIDcache(); 
        this.loc = new Cordenada();
        this.creatDate = new GregorianCalendar();
        this.nome = new String();
        this.descri = new String();
        this.owner = utl;
        this.difMent=0;
        this.difFisic=0;
        this.pontuation=0;
        this.visited = new TreeSet<String>();
        this.pistas = new ArrayList<String>();
        this.pontInt = new ArrayList<Cordenada>();
    }
    
    /**
     * Construtor parametrizado para objectos da classe Cache com Email, cordenada, data, nome, descrição, dif. mental, dif. fisica, pontuação
     */
    public Cache(String utl, Cordenada l, GregorianCalendar dt, String n, String desc, int dM, int dF, int p){
        this.id= daIDcache();
        this.loc = l.clone();
        this.creatDate = (GregorianCalendar)dt.clone();
        this.nome = n;
        this.descri = desc;
        this.owner = utl;
        this.difMent=dM;
        this.difFisic=dF;
        this.pontuation=p;
        this.visited = new TreeSet<String>();
        this.pistas = new ArrayList<String>();
        this.pontInt = new ArrayList<Cordenada>();
    }
    
    /**
     * Construtor de copia para objectos da classe Cache
     */
    public Cache(Cache c){
        this.id=c.getId();
        this.loc = c.getLoc();
        this.creatDate = c.getCreatDate();
        this.nome = c.getNome();
        this.descri = c.getDescri();
        this.owner = c.getOwner();
        this.difMent=c.getDifMent();
        this.difFisic=c.getDifFisic();
        this.pontuation=c.getPontuation();
        this.visited = c.getVisited();
        this.pistas = c.getPistas();
        this.pontInt = c.getPontInt();
    }
    
    
    /**
     * Metodo getId
     * @return      id da cache   
     */
    public int getId(){return this.id;}
    
    /**
     * Metodo getLoc
     * @return      localização da cache    
     */
    public Cordenada getLoc(){return this.loc.clone();}
    
    /**
     * Metodo getCreatDate
     * @return      data de criação da cache  
     */
    public GregorianCalendar getCreatDate(){return (GregorianCalendar)this.creatDate.clone();}
    
    /**
     * Metodo getNome
     * @return      nome da cache    
     */
    public String getNome(){return this.nome;}
    
    /**
     * Metodo getDescri
     * @return      descrição da cache  
     */ 
    public String getDescri(){return this.descri;}
    
    /**
     * Metodo getOwner
     * @return      email do criador da cache 
     */
    public String getOwner(){return this.owner;}
    
    /**
     * Metodo getDifMent
     * @return      dificuldade mental da cache
     */
    public int getDifMent(){return this.difMent;}
    
    /**
     * Metodo getDifFisic
     * @return      dificuldade fisica da cache 
     */
    public int getDifFisic(){return this.difFisic;}
    
    /**
     * Metodo getPontuation
     * @return      pontuação da cache  
     */
    public int getPontuation(){return this.pontuation;}
    
    /**
     * Metodo getVisited
     * @return      lista de todos os utilizadores que visitaram a cache 
     */
    public TreeSet<String> getVisited(){
        TreeSet<String> v = new TreeSet<String>();
        v.addAll(this.visited);
        return v;
    }
    
    /**
     * Metodo getPistas
     * @return      lista de pistas da cache   
     */
    public ArrayList<String> getPistas(){
        ArrayList<String> p = new ArrayList<String>();
        p.addAll(this.pistas);
        return p;
    }
    
    /**
     * Metodo getPontInt
     * @return      lista de locais de interesse perto da cache 
     */
    public ArrayList<Cordenada> getPontInt(){
        ArrayList<Cordenada> l = new ArrayList<Cordenada>();
        Iterator<Cordenada> it = this.pontInt.iterator();
        while(it.hasNext()){
            l.add(it.next());
        }
        return l;
    }
    
    /**
     * Metodo setLoc
     * @param       local   
     */
    public void setLoc(Cordenada l){this.loc = l.clone();}
    
    /**
     * Metodo setCreatDate
     * @param       data
     */
    public void setCreatDate(GregorianCalendar dt){this.creatDate = (GregorianCalendar)dt.clone();}
    
    /**
     * Metodo 
     * @param       nome
     */
    public void setNome(String n){this.nome = n;}
    
    /**
     * Metodo setNome
     * @param       descrição
     */
    public void setDescri(String d){this.descri = d;}
    
    /**
     * Metodo setOwner
     * @param       email 
     */
    private void setOwner(String u){this.owner = u;}
    
    /**
     * Metodo setDifMent
     * @param       dif. mental (inteiro de 1..5)
     */
    public void setDifMent(int dm){this.difMent = dm;}
    
    /**
     * Metodo setDifFisic
     * @param       dif. fisica (inteiro de 1..5)
     */
    public void setDifFisic(int df){this.difFisic = df;}
    
    /**
     * Metodo setPontuation
     * @param       pontuação 
     */
    public void setPontuation(int p){this.pontuation = p;}
    
    /**
     * Metodo setVisited
     * @param       lista de utilizadores
     */
    public void setVisited(TreeSet<String> v){
        TreeSet<String> res = new TreeSet<String>();
        res.addAll(v);
        this.visited = res;
    }
    
    /**
     * Metodo setPistas
     * @param       lista de pistas 
     */
    public void setPistas(ArrayList<String> p){
        ArrayList<String> res = new ArrayList<String>();
        res.addAll(p);
        this.pistas = res;
    }
    
    /**
     * Metodo setPontInt
     * @param       lista de cordenadas
     */
    public void setPontInt(ArrayList<Cordenada> pi){
        ArrayList<Cordenada> l = new ArrayList<Cordenada>();
        Iterator<Cordenada> it = pi.iterator();
        while(it.hasNext()){
            l.add(it.next());
        }
        this.pontInt = l;
    }
    
    /**
     * Metodo nVisited
     * @return      numero de Utilizadores que visitaram a cache
     */
    public int nVisited(){return this.getVisited().size();}
    
    /**
     * Metodo nPistas
     * @return      numero de pistas que a cache possui
     */
    public int nPistas(){return this.getPistas().size();}
    
    /**
     * Metodo nPontInt
     * @return      numero de pontos de interesse que estao na cache
     */
    public int nPontInt(){return this.getPontInt().size();}
    
    /**
     * Metodo addViseted
     * @param       email de utilizador a adicionar
     * @return      se foi possivel adicionar o utilizador
     */
    public boolean addViseted(String utl){return this.visited.add(utl);}
    
    /**
     * Metodo addPistas
     * @param       pista   
     * @return      se foi possivel adicionar a pista
     */
    public boolean addPistas(String p){
        boolean res = true;
        if(!(this.pistas.contains(p))){res = this.pistas.add(p);}
        return res;    
    }
    
    /**
     * Metodo addPontInt
     * @param       local
     * @return      se foi possivel adicionar o local de interesse
     */
    public boolean addPontInt(Cordenada c){
        boolean res = true;
        if(!(this.pontInt.contains(c.clone()))){res = this.pontInt.add(c.clone());}
        return res;
    }
    
    /**
     * Metodo removeVisited
     * @param       email de utilizador a remover
     * @return      se foi possivel remover o utilizador   
     */
    public boolean removeVisited(String utl){return this.visited.remove(utl);}
    
    /**
     * Metodo removePistas
     * @param       pista
     * @return      se foi possivel adicionar a pista
     */
    public boolean removePistas(String p){ return this.pistas.remove(p);}
    
    /**
     * Metodo removePontInt
     * @param       cordenada
     * @return      se foi possivel adionar a cordenada o aos pontos de interesse
     */
    public boolean removePontInt(Cordenada c){return this.pontInt.remove(c);}
    
    /**
     * Metodo vistou
     * @param       email de utilizador
     * @return      se o utilizador visitou a cache
     */
    public boolean vistou(String utl){return this.visited.contains(utl);}
   
    /**
     * Metodo tipoCache
     * @return      o tipo do objecto
     */
    public String tipoCache(){
        return this.getClass().getSimpleName();
    }
    
    /**
     * Metodo toString
     * @return  String   
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Cache: " + this.getId() + " - " + this.getNome() + " - " + this.getOwner());
        s.append(" < Ano: " +  this.getCreatDate().get(Calendar.YEAR) + " Mes: " + (this.getCreatDate().get(Calendar.MONTH) + 1 )+ " Dia: " + (this.getCreatDate().get(Calendar.DAY_OF_MONTH) + 1) + " >\n" );
        s.append(this.getLoc().toString());
        s.append("Dificuldade Mental: " + this.getDifMent() + "\n" + "Dificuldade Fisica: " + this.getDifFisic() + "\n");
        s.append("Pontuação: " + this.getPontuation() + "\n");
        s.append("Descrição:\n" + this.getDescri().toString() + "\n");
        s.append("Pistas: \n");
        for(String p : this.getPistas()){
            s.append("-> " + p + ";\n");
        }
        s.append("Locais de Interesse: \n");
        for(Cordenada l : this.getPontInt()){
            s.append("-> " + l.toString() + ";\n"); // atençao so devia de imprimir as coordenadas enao toda a informaçao do Cordenada
        }
        s.append("Visitada por: \n");
        for(String utl : this.getVisited()){
            s.append("-> " + utl + ";\n");
        }
        return s.toString();
    }
    
    /**
     * Metodo clone
     * @return     uma copia 
     */
    public Cache clone(){return(new Cache(this));}

    /**
     *  Metodo equals
     * @param       objecto que vai compra
     * @return      resultado da comprarçao dos dois objectos  
     */
    public boolean equals(Object obj){
        boolean res = true;
        if(this==obj) return true;
        if(obj==null || this.getClass() != obj.getClass()) return false;
        Cache c = (Cache)obj;
        if( !(this.getId()==c.getId()           && this.getOwner().equals(c.getOwner()) 
         && this.getNome().equals(c.getNome())  && this.getCreatDate().equals(c.getCreatDate()) 
         && this.getLoc().equals(c.getLoc())    && this.getDescri().equals(c.getDescri()) 
         && this.getDifMent()==c.getDifMent()   && this.getDifFisic()==c.getDifFisic() 
         && this.getPontuation()==c.getPontuation()) && this.nVisited()==c.nVisited()
         && this.nPistas()==c.nPistas()         && this.nPontInt()==c.nPontInt()) return false;
        if(!(this.getVisited().containsAll(c.getVisited()))) return false;
        if(!(this.getPistas().containsAll(c.getPistas()))) return false;
        for(int i=0;i<nPontInt() && res;i++){
            res = this.getPontInt().get(i).equals(c.getPontInt().get(i));
        }
        return res;
    }
    
    /**
     * Metodo hashCode
     * @return     valor de hash
     */
    public int hashCode(){return this.id;}
    //por JMS
    public double distMetro(Cordenada c) {return this.getLoc().distM(c.clone());}
    public double distKm(Cordenada c) {return this.getLoc().distKM(c.clone());}
    public boolean maisPertoM(Cordenada co, double dm){return this.getLoc().maisPertoM(co,dm);}
    public boolean maisPertoKM(Cordenada co, double dm){return this.getLoc().maisPertoKM(co,dm);}
}
